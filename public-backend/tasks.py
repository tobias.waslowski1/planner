import shlex
import sys

import watchfiles
from invoke import task  # pyright: reportMissingModuleSource=false


@task
def install(c):
    "(Re-)install the python dependencies"
    c.run("poetry install")


@task
def test(c):
    "Run the tests"
    run_with_passthrough(c, "pytest", pty=True)


@task
def watch(c):
    "Continuously re-run all tests whenever files are changed"
    watchfiles.run_process("./src", target=f"pytest {passthrough_args()}")


@task
def tdd_reset(c):
    """
    Clear caches and everything.

    Shouldn't usually be necessary, try this if tdd is weird
    """
    c.run("rm -rf .pytest_cache .testmondata", echo="both")


@task(tdd_reset)
def tdd(c):
    "Continuousy re-run affected tests whenever files are changed"
    watchfiles.run_process("./src", target=f"pytest --testmon {passthrough_args()}")


#####################################################################
## Helper functions


def run_with_passthrough(c, cmd, *args, **kwargs):
    return c.run(" ".join([cmd, passthrough_args()]), *args, **kwargs)


def passthrough_args() -> str:
    try:
        dashdash_position = sys.argv.index("--")
    except ValueError:
        return ""
    return " ".join(map(shlex.quote, sys.argv[dashdash_position + 1 :]))
