# Public Backend

## Setting up the project

- Ensure you have python installed in a version >= 3.9 :

`python3 --version`

- To work locally it is recommended to configure a python virtual environment. To achieve this do the following steps:

```
python3 -m venv .env
source .env/bin/activate
pip install --upgrade pip
pip install -r requirements.txt -r requirements-dev.txt
```

## Running the project for development

We now use [invoke](https://www.pyinvoke.org/) as a convenient way to run common
commands.
It is included in requirements-dev.txt, so to get started:

1. Install python, version 3.9 strongly preferred and setup a virtualenv
2. `pip install --upgrade pip`
3. `pip install -r requirements.txt -r requirements-dev.txt`
4. `invoke setup-repository`
5. `invoke --list` or `inv -l` for short, or look into tasks.py to see what the
   commands do.

## Configure the app to use dev settings

- If you're using direnv, it will happen automatically
- If not, you can create an empty file ".use-dev-settings" in the repo-root (the
  one that contains the folders "backoffice", "public-backend" and
  "public-client")

## Prepare the Database

- Prepare the local database by running `./manage.py migrate`
- This will create a local db.sqlite

## Start the server

`./manage.py runserver`
