from django.urls import path
from django.views.generic import TemplateView

from . import views

urlpatterns = [
    path("create/", views.application_create, name="application_create"),
    path("oldest/", views.application_detail_oldest, name="application_detail_oldest"),
    path("applications/", views.applications, name="applications"),
    path(
        "application_success/",
        TemplateView.as_view(template_name="applications/application_success.html"),
    ),
    path("inquiries/", views.inquiries, name="inquiries"),
    path("inquiry_form/<uuid:uid>/", views.inquiry_form, name="inquiry_form"),
    path(
        "inquiry_success/",
        TemplateView.as_view(template_name="applications/inquiry_success.html"),
    ),
    path("inquiry/<uuid:uid>/", views.inquiry_delete, name="inquiry_delete"),
    path("<uuid:uid>/", views.application_delete, name="application_delete"),
    path(
        "request_availabilities/",
        views.application_request_availabilities,
        name="application_request_availabilities",
    ),
    path("update/", views.application_update, name="application_update"),
]
