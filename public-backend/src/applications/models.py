from datetime import date, datetime, timezone
from uuid import uuid4

from django.core.exceptions import ValidationError
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from planner_common import dtos


def uuid4_str():
    return str(uuid4())


class CommonFormModel(models.Model):
    id = models.AutoField(primary_key=True)
    uid = models.UUIDField(default=uuid4, editable=False)
    created = models.DateTimeField(editable=False)
    data = models.JSONField()

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        """On save, create timestamp"""
        if not self.id and self.created is None:
            self.created = datetime.now(timezone.utc)
        self.on_save()
        return super().save(*args, **kwargs)

    def on_save(self):
        pass


class Application(CommonFormModel):
    def on_save(self):
        if f"{self.data['date_of_birth']}" > f"{date.today()}":
            raise ValidationError("Date of birth has to be in the past.")

    def to_dict(self):
        return {
            "uid": self.uid,
            "created": self.created,
            "data": self.data,
        }


class Language(models.Model):
    code = models.CharField(max_length=3)
    points = models.IntegerField()

    applicant = models.ForeignKey(Application, on_delete=models.CASCADE)


class Inquiry(CommonFormModel):
    class State(models.TextChoices):
        NEW = "NEW", _("new")
        ANSWERED = "ANSWERED", _("answered")

    state = models.CharField(max_length=255, choices=State.choices, default=State.NEW)

    class Type(models.TextChoices):
        AVAILABILITY = "AVAILABILITY", _("availability")

    type = models.CharField(max_length=255, choices=Type.choices)

    def to_dto(self) -> dtos.Inquiry:
        return dtos.Inquiry(
            uid=self.uid,
            type=dtos.Inquiry.Type(self.type.lower()),
            created=self.created,
            data=dtos.AvailabilityInquiryData.from_dict(self.data),
        )

    def form_url(self):
        return reverse("inquiry_form", kwargs={"uid": self.uid})

    ##########
    # Managers

    class UnansweredManager(models.Manager):
        def get_queryset(self):
            return super().get_queryset().filter(state=Inquiry.State.NEW)

    objects = models.Manager()
    unanswered = UnansweredManager()
