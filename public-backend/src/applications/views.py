import json
import logging
import os
from http import HTTPStatus
from uuid import UUID

from django.conf import settings
from django.core import mail
from django.core.exceptions import ValidationError as DjangoValidationError
from django.http import HttpResponse, JsonResponse
from django.http.response import HttpResponseNotFound
from django.shortcuts import get_object_or_404, render
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods, require_POST
from django.views.generic import TemplateView
from jsonschema import validate
from jsonschema.exceptions import ValidationError as SchemaValidationError
from planner_common import dtos, schema

from .models import Application, Inquiry
from .token_authentication import requires_token_authentication

logger = logging.getLogger(__name__)


@require_POST
@csrf_exempt
def application_create(request):
    data = json.loads(request.body)

    try:
        validate(instance=data, schema=schema.initial_application)
    except SchemaValidationError as e:
        return JsonResponse(status=422, data={"ok": False, "message": e.args[0]})

    try:
        Application.objects.create(
            data=data,
        )

    except KeyError as e:
        return JsonResponse(status=422, data={"message": f"Missing key: {e.args[0]}"})
    except DjangoValidationError as e:
        return JsonResponse(status=422, data={"message": e.args[0]})

    try:
        mail.send_mail(
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[data["email"]],
            subject="Thanks for signing up",
            message=render_to_string(
                "applications/email/application_submitted.txt",
                {"first_name": data["first_name"]},
            ),
            fail_silently=False,
        )
    except:
        logger.exception(
            "Error sending application confirmation mail to applicant",
            extra={"applicant_email": data.get("email", "<unknown>")},
        )

    return HttpResponse(status=HTTPStatus.NO_CONTENT)


@requires_token_authentication
def application_detail_oldest(request):
    try:
        application = Application.objects.order_by("created")[0:1].get()
    except Application.DoesNotExist:
        return HttpResponse(status=HTTPStatus.NO_CONTENT)
    else:
        return JsonResponse(application.to_dict())


@csrf_exempt
@requires_token_authentication
@require_http_methods(["DELETE"])
def application_delete(request, uid):
    try:
        application = Application.objects.get(uid=uid)
        application.delete()
    except Application.DoesNotExist:
        return HttpResponse(status=HTTPStatus.NOT_FOUND)
    else:
        return JsonResponse({"ok": True})


class HomeView(TemplateView):
    template_name = "applications/index.html"


@csrf_exempt
@requires_token_authentication
@require_POST
def application_request_availabilities(request):
    # Not sure why we're parsing the DTO here. Maybe to make sure that it has roughly the right
    # shape? I want to keep refactoring this, so I'm not quite ready to delete it yet.
    dto = dtos.AvailabilityInquiryData.from_dict(json.loads(request.body))
    inquiry = Inquiry.objects.create(
        # This needs to be kept in sync with the AvailabilityInquiryFactory
        data=dto.to_dict(),
        type=Inquiry.Type.AVAILABILITY,
    )

    mail.send_mail(
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[dto.email],
        subject="Mission availability request",
        message=render_to_string(
            "applications/email/availability_requested.txt",
            {
                "inquiry": inquiry,
                "form_url": f"https://{settings.EMAIL_URL_HOST}{inquiry.form_url()}",
            },
        ),
        fail_silently=False,
    )
    return HttpResponse(status=204)


@require_POST
@csrf_exempt
def application_update(request):
    data = {}
    token = ""  # nosec hardcoded_password_string
    inquiry = None
    try:
        data = json.loads(request.body)
        token = UUID(data["token"])
    except (json.JSONDecodeError, KeyError, ValueError):
        return JsonResponse(
            status=422, data={"ok": False, "message": "Token is missing or invalid"}
        )
    try:
        inquiry = Inquiry.objects.get(uid=token)
    except Inquiry.DoesNotExist:
        return JsonResponse(
            status=422, data={"ok": False, "message": "Token is missing or invalid"}
        )
    if inquiry.type == Inquiry.Type.AVAILABILITY:
        try:
            validate(instance=data, schema=schema.availability)
        except SchemaValidationError as e:
            return JsonResponse(status=422, data={"ok": False, "message": e.args[0]})

        # Ensure that missions were not forged by client
        missions_from_client = dict()
        missions_from_inquiry = dict()
        for mission in data["missions"]:
            missions_from_client[mission["mission"]] = mission["availability"]
        for mission in inquiry.data["missions"]:
            missions_from_inquiry[mission["mission"]] = mission

        if missions_from_inquiry.keys() != missions_from_client.keys():
            return JsonResponse(
                status=422, data={"ok": False, "message": "Invalid request"}
            )

        # Update mission availabilities
        for mission in inquiry.data["missions"]:
            mission["availability"] = missions_from_client[mission["mission"]]
        inquiry.state = Inquiry.State.ANSWERED
        inquiry.save()
        mail.send_mail(
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[inquiry.data["email"]],
            subject="Mission availability confirmed",
            message=render_to_string(
                "applications/email/availability_confirmed.txt", {"inquiry": inquiry}
            ),
            fail_silently=False,
        )
    return HttpResponse(status=204)


@requires_token_authentication
def applications(request):
    applications = Application.objects.all()
    if applications:
        return JsonResponse(
            {"applications": [application.to_dict() for application in applications]}
        )
    else:
        return HttpResponse(status=204)


@requires_token_authentication
def inquiries(request):
    inquiries = Inquiry.objects.filter(state=Inquiry.State.ANSWERED)
    return JsonResponse(
        {"inquiries": [inquiry.to_dto().to_dict() for inquiry in inquiries]}
    )


def inquiry_form(request, uid):
    inquiry = get_object_or_404(Inquiry.unanswered, uid=uid)
    return render(
        request,
        "applications/inquiries/availability.html",
        context={"inquiry": inquiry},
    )


@csrf_exempt
@requires_token_authentication
@require_http_methods(["DELETE"])
def inquiry_delete(request, uid):
    deleted_count, _ = Inquiry.objects.filter(uid=uid).delete()
    return JsonResponse({"ok": True}) if deleted_count != 0 else HttpResponseNotFound()
