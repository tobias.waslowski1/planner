from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseForbidden


def raise_on_invalid_auth_token(request):
    try:
        token_header = request.META["HTTP_AUTHORIZATION"]
    except KeyError:
        raise PermissionDenied()

    if token_header != f"Token {settings.PLANNER_PUBLIC_BACKEND_AUTH_TOKEN}":
        raise PermissionDenied()


def requires_token_authentication(view):
    def wrapped(request, *args, **kwargs):
        raise_on_invalid_auth_token(request)
        return view(request, *args, **kwargs)

    return wrapped
