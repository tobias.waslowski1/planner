Dear {{ inquiry.data.name }},

thank you for letting us know when you are available.

This is what you have selected:

{% for mission in inquiry.data.missions %}{{ mission.start_date }} to {{ mission.end_date }}: {{ mission.availability }}
{% endfor %}

Please contact us via crewing@sea-watch.org if you want to provide us any additional information.

Cheers
Sea-Watch Crewing
