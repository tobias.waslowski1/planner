import dataclasses
import json
from datetime import date, datetime, timezone
from http import HTTPStatus
from typing import cast
from urllib.parse import unquote
from uuid import UUID, uuid4

import dateutil.parser
import pytest
from bs4 import BeautifulSoup
from bs4.element import Tag
from django.conf import settings
from django.contrib.staticfiles import finders
from django.core import mail
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseNotAllowed
from django.http.response import HttpResponse
from django.template.response import TemplateResponse
from django.test import Client, RequestFactory, override_settings
from django.urls import reverse
from planner_common import dtos, schema
from planner_common.test.factories import (
    ApplicationDataFactory,
    AvailabilityFactory,
    AvailabilityInquiryDataFactory,
)

from .. import views
from ..models import Application, Inquiry
from .factories import ApplicationFactory, AvailabilityInquiryFactory


def test_schema_defines_denial_of_all_possible_addition_properties():
    assert schema.initial_application["additionalProperties"] == False

    # same check recursively
    def recursively_check(node):
        if node["type"] == "array" and "items" in node:
            recursively_check(node["items"])
        elif node["type"] == "object":
            assert node["additionalProperties"] == False
            for prop in node["properties"].values():
                recursively_check(prop)

    recursively_check(schema.initial_application)


###########################
# application_detail_oldest


@pytest.mark.django_db
@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
def test_application_detail_oldest_is_reachable(client: Client):
    response = client.get(
        reverse("application_detail_oldest"),
        HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN",
    )
    assert isinstance(response, HttpResponse)
    assert response.status_code == 204


@pytest.mark.django_db
@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
def test_application_detail_oldest_returns_oldest_application(rf: RequestFactory):
    first_name = "Old Application"
    uid = UUID("a3888211-31cc-4708-91f5-82bb9b23dd14")
    phone_number = "+491739948582"
    test_datetime = datetime(2020, 1, 12, 3, 0, 0, tzinfo=timezone.utc)
    ApplicationFactory(
        first_name=first_name,
        phone_number=phone_number,
        uid=uid,
        created=test_datetime,
    )
    ApplicationFactory()
    request = rf.get(
        "application_detail_oldest", HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN"
    )
    response = views.application_detail_oldest(request)
    application = json.loads(response.content)
    application_created_datetime = dateutil.parser.isoparse(application["created"])

    # We only test fields that are already imported by the backoffice
    assert application["data"]["first_name"] == first_name
    assert application["data"]["phone_number"] == phone_number
    assert application["uid"] == str(uid)
    assert application_created_datetime == test_datetime


@pytest.mark.django_db
def test_application_detail_oldest_requires_token(rf: RequestFactory):
    request = rf.delete("application_detail_oldest")
    with pytest.raises(PermissionDenied):
        views.application_detail_oldest(request)


####################
# application_delete


@pytest.mark.django_db
@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
def test_application_delete_is_reachable(client: Client):
    response = client.delete(
        reverse("application_delete", kwargs={"uid": uuid4()}),
        HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN",
    )
    assert isinstance(response, HttpResponse)
    assert response.status_code == 404


@pytest.mark.django_db
@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
def test_application_delete_deletes_application_in_db(rf: RequestFactory):
    uid = uuid4()
    ApplicationFactory(uid=uid)
    request = rf.delete(
        "application_delete", HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN"
    )
    response = views.application_delete(request, uid=uid)
    assert response.status_code == 200
    assert json.loads(response.content)["ok"]
    assert Application.objects.filter(uid=uid).count() == 0


def test_application_delete_requires_token(rf: RequestFactory):
    request = rf.delete("application_delete")
    with pytest.raises(PermissionDenied):
        views.application_delete(request, uid="asldalks")


@pytest.mark.parametrize("method", ["get", "post", "put"])
@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="VALID")
def test_application_delete_requires_delete(rf: RequestFactory, method: str):
    request = getattr(rf, method)(
        "application_delete", HTTP_AUTHORIZATION="Token VALID"
    )
    response = views.application_delete(request, uid=uuid4())
    assert HTTPStatus(response.status_code) == HTTPStatus.METHOD_NOT_ALLOWED


####################################
# application_request_availabilities


@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
@pytest.mark.django_db
def test_application_request_availabilities_is_reachable(client: Client):
    data: dtos.AvailabilityInquiryData = AvailabilityInquiryDataFactory.create()
    response = client.post(
        reverse("application_request_availabilities"),
        json.dumps(data.to_dict()),
        content_type="application/json",
        HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN",
    )
    assert isinstance(response, HttpResponse)
    assert response.status_code == 204


@pytest.mark.django_db
def test_application_request_availabilities_requires_token(rf: RequestFactory):
    request = rf.get("application_request_availabilities")
    with pytest.raises(PermissionDenied):
        views.application_request_availabilities(request)


@pytest.mark.django_db
@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
def test_application_request_availabilities_creates_form_data(rf: RequestFactory):
    uid = UUID("e605cc82-7c06-4d43-9571-a5ba7f4001ca")
    data: dtos.AvailabilityInquiryData = AvailabilityInquiryDataFactory.create(
        name="Jane",
        email="jane@test.com",
        missions=[
            AvailabilityFactory(
                mission=uid,
                start_date=date(2023, 1, 1),
                end_date=date(2024, 1, 1),
                availability=dtos.Availability.Answer.Unknown,
            )
        ],
    )
    request = rf.post(
        "application_request_availabilities",
        json.dumps(data.to_dict()),
        content_type="application/json",
        HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN",
    )
    views.application_request_availabilities(request)
    inquiry = Inquiry.objects.last()
    assert inquiry
    assert inquiry.type == Inquiry.Type.AVAILABILITY
    assert inquiry.data == {
        "name": "Jane",
        "email": "jane@test.com",
        "missions": [
            {
                "availability": "unknown",
                "mission": str(uid),
                "start_date": "2023-01-01",
                "end_date": "2024-01-01",
            }
        ],
    }


@pytest.mark.django_db
@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
def test_application_request_availabilities_sends_email(rf: RequestFactory):
    data: dtos.AvailabilityInquiryData = AvailabilityInquiryDataFactory.create()
    request = rf.post(
        "application_request_availabilities",
        json.dumps(data.to_dict()),
        content_type="application/json",
        HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN",
    )
    response = views.application_request_availabilities(request)
    assert response.status_code == 204

    assert len(mail.outbox) == 1, "No email has been sent"
    app_request_email = mail.outbox[0]
    assert (
        "Please use the following link to update your availabilities:"
        in app_request_email.body
    )


@pytest.mark.django_db
@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
def test_application_request_availabilities_email_contains_mission_dates(
    rf: RequestFactory,
):
    data: dtos.AvailabilityInquiryData = AvailabilityInquiryDataFactory.create(
        name="Marlon",
        email="marlon@example.net",
        missions=[
            AvailabilityFactory(availability=dtos.Availability.Answer.Yes),
            AvailabilityFactory(availability=dtos.Availability.Answer.Unknown),
        ],
    )
    request = rf.post(
        "application_request_availabilities",
        json.dumps(data.to_dict()),
        content_type="application/json",
        HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN",
    )
    views.application_request_availabilities(request)

    inquiry = Inquiry.objects.last()
    assert inquiry
    missions = inquiry.data["missions"]
    email = mail.outbox[0]
    assert (
        missions[0]["start_date"] in email.body
    ), "Mission starting date for mission 1 is missing"
    assert (
        missions[0]["end_date"] in email.body
    ), "Mission end date for mission 1 is missing"
    assert (
        missions[1]["start_date"] in email.body
    ), "Mission starting date for mission 2 is missing"
    assert (
        missions[1]["end_date"] in email.body
    ), "Mission end date for mission 2 is missing"
    assert "yes" in email.body, "Does not contain pre-selected availability"
    assert (
        "unknown" not in email.body
    ), "Email contains pre-selected avail even though it is set to unknown"


@pytest.mark.django_db
@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
@override_settings(EMAIL_URL_HOST="public.test")
def test_application_request_availabilities_email_contains_correct_link(
    rf: RequestFactory,
):
    data: dtos.AvailabilityInquiryData = AvailabilityInquiryDataFactory.create()
    request = rf.post(
        "application_request_availabilities",
        json.dumps(data.to_dict()),
        content_type="application/json",
        HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN",
    )
    views.application_request_availabilities(request)
    inquiry = Inquiry.objects.last()
    assert inquiry
    app_request_email = mail.outbox[0]
    assert "http://" not in app_request_email.body
    form_url = "https://public.test" + reverse(
        "inquiry_form", kwargs={"uid": inquiry.uid}
    )
    assert form_url in app_request_email.body, "No url found in email"


####################
# application_update


def test_application_update_is_reachable(client: Client):
    response = client.post(reverse("application_update"))
    assert response.status_code in [200, 422]


def test_application_update_get_not_allowed(rf: RequestFactory):
    request = rf.get("application_update")
    response = views.application_update(request)
    assert isinstance(response, HttpResponseNotAllowed)


def test_application_update_raises_when_vars_missing(rf: RequestFactory):
    request = rf.post(
        "application_update",
        json.dumps({}),
        content_type="application/json",
    )
    response = views.application_update(request)
    assert response.status_code == 422


@pytest.mark.django_db
def test_application_update_raises_when_token_is_invalid(rf: RequestFactory):
    request = rf.post(
        "application_update",
        json.dumps({"token": "fake_token"}),
        content_type="application/json",
    )
    response = views.application_update(request)
    assert response.status_code == 422


@pytest.mark.django_db
def test_application_update_raises_when_inquiry_vars_are_missing(rf: RequestFactory):
    inquiry = AvailabilityInquiryFactory()
    request = rf.post(
        "application_update",
        json.dumps({"token": str(inquiry.uid)}),
        content_type="application/json",
    )
    response = views.application_update(request)
    assert response.status_code == 422


@pytest.mark.django_db
def test_application_update_updates_availabilities(rf: RequestFactory):
    mission_uid = uuid4()
    inquiry = AvailabilityInquiryFactory(
        data__missions=[AvailabilityFactory(mission=mission_uid)]
    )
    availability = [{"mission": str(mission_uid), "availability": "yes"}]
    request = rf.post(
        "application_update",
        json.dumps({"token": str(inquiry.uid), "missions": availability}),
        content_type="application/json",
    )
    response = views.application_update(request)
    assert response.status_code == 204
    inquiry = Inquiry.objects.filter(uid=inquiry.uid).first()
    assert inquiry
    assert inquiry.data["missions"][0]["availability"] == "yes"
    assert inquiry.state == Inquiry.State.ANSWERED


@pytest.mark.django_db
def test_application_update_rejects_forged_availabilites(rf: RequestFactory):
    uid = uuid4()
    inquiry = AvailabilityInquiryFactory(data__missions__1__mission=uid)
    forged = [{"mission": str(uid), "availability": "elections2020!!!1one"}]
    request = rf.post(
        "application_update",
        json.dumps({"token": str(inquiry.uid), "missions": forged}),
        content_type="application/json",
    )
    response = views.application_update(request)
    assert response.status_code == 422

    forged = [{"mission": "impossible", "availability": "yes"}]
    request = rf.post(
        "application_update",
        json.dumps({"token": str(inquiry.uid), "missions": forged}),
        content_type="application/json",
    )
    response = views.application_update(request)
    assert response.status_code == 422


@pytest.mark.django_db
def test_application_update_sends_mail_after_submitting_availabilities(
    rf: RequestFactory,
):
    uids = [str(uuid4()) for _ in range(3)]
    missions = [
        AvailabilityFactory(
            mission=uids[0],
            start_date=date(2020, 1, 1),
            end_date=date(2020, 2, 2),
        ),
        AvailabilityFactory(
            mission=uids[1],
            start_date=date(2020, 3, 1),
            end_date=date(2020, 4, 2),
        ),
        AvailabilityFactory(
            mission=uids[2],
            start_date=date(2020, 5, 1),
            end_date=date(2020, 6, 2),
        ),
    ]
    inquiry = AvailabilityInquiryFactory(
        data__missions=missions,
        data__email="volunteer@example.com",
        data__name="Brandon",
    )
    availability = [
        {"mission": uids[0], "availability": "yes"},
        {"mission": uids[1], "availability": "no"},
        {"mission": uids[2], "availability": "maybe"},
    ]
    request = rf.post(
        "application_update",
        json.dumps({"token": str(inquiry.uid), "missions": availability}),
        content_type="application/json",
    )
    response = views.application_update(request)

    assert response.status_code == 204

    assert len(mail.outbox) == 1
    email = mail.outbox[0]
    assert email.to == ["volunteer@example.com"]
    assert email.subject == "Mission availability confirmed"
    assert "Brandon" in email.body
    assert "thank you" in email.body
    assert "2020-01-01 to 2020-02-02: yes" in email.body
    assert "2020-03-01 to 2020-04-02: no" in email.body
    assert "2020-05-01 to 2020-06-02: maybe" in email.body
    assert "http://" not in email.body


@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
def request_availabilities_returns_400_on_missing_email(rf: RequestFactory):
    request = rf.get(
        "application_request_availabilities",
        data={},
        HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN",
    )
    response = views.application_request_availabilities(request)
    assert response.status_code == 400


@pytest.mark.django_db
@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
def test_applications_is_reachable(client: Client):
    response = client.get(
        reverse("applications"),
        HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN",
    )
    assert isinstance(response, HttpResponse)
    assert response.status_code == 204


@pytest.mark.django_db
def test_applications_requires_token(rf: RequestFactory):
    request = rf.get("applications/ignored")
    with pytest.raises(PermissionDenied):
        views.applications(request)


@pytest.mark.django_db
@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
def test_applications_returns_application(rf: RequestFactory):
    # The data isn't evaluated by what we care about here.
    data = dataclasses.asdict(ApplicationDataFactory())
    uid = UUID("b6a33a79-78d2-42a4-ad5f-c3ea28f99753")
    test_datetime = datetime(2020, 2, 10, 4, 1, 6, tzinfo=timezone.utc)
    Application.objects.create(uid=uid, created=test_datetime, data=data)

    request = rf.get(
        "applications/ignored",
        HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN",
    )
    response = views.applications(request)
    assert response.status_code == 200

    applications_data = json.loads(response.content)
    assert "applications" in applications_data

    applications = applications_data["applications"]
    assert isinstance(applications, list)
    assert len(applications) == 1

    application = applications[0]
    assert application["uid"] == str(uid)
    application_created_datetime = dateutil.parser.isoparse(application["created"])
    assert application_created_datetime == test_datetime
    assert application["data"] == data


@pytest.mark.django_db
@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
def test_applications_returns_multiple_applications(rf: RequestFactory):
    ApplicationFactory()
    ApplicationFactory()
    request = rf.get(
        "applications/ignored",
        HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN",
    )
    response = views.applications(request)
    applications = json.loads(response.content)["applications"]
    assert len(applications) == 2


###########
# inquiries


@pytest.mark.django_db
@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
def test_inquiries_is_reachable(client: Client):
    response = client.get(
        reverse("inquiries"),
        HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN",
    )
    assert isinstance(response, HttpResponse)
    assert response.status_code == 200


@pytest.mark.django_db
def test_inquiries_requires_token(rf: RequestFactory):
    request = rf.get("inquiries/ignored")
    with pytest.raises(PermissionDenied):
        views.inquiries(request)


@pytest.mark.django_db
@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
def test_inquiries_returns_inquiry(rf: RequestFactory):
    # Given there is one Inquiry present in the databse
    inquiry: Inquiry = AvailabilityInquiryFactory.create(state=Inquiry.State.ANSWERED)
    # When I call the inquiries endpoint
    request = rf.get(
        "inquiries/ignored",
        HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN",
    )
    response = views.inquiries(request)
    # I expect that the response contains the inquiry
    assert response.status_code == 200
    inquiries_data = json.loads(response.content)
    assert "inquiries" in inquiries_data
    remote_inquiry = inquiries_data["inquiries"][0]
    # and I expect the uid and type to match to the object that was in the db
    assert str(inquiry.uid) == remote_inquiry["uid"]
    assert inquiry.type.lower() == remote_inquiry["type"]
    # and I expect the data payload of the inquiry to be as expected
    expected_payload = inquiry.data
    assert remote_inquiry["data"] == expected_payload


@pytest.mark.django_db
@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
def test_inquiries_returns_multiple_inquiries(rf: RequestFactory):
    # given that there are 2 answered inquiries present in the database
    AvailabilityInquiryFactory(state=Inquiry.State.ANSWERED)
    AvailabilityInquiryFactory(state=Inquiry.State.ANSWERED)
    # When I call the inquiries endpoint
    request = rf.get(
        "inquiries/ignored",
        HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN",
    )
    response = views.inquiries(request)
    # I expect that the response contains both inquiries
    inquiries_data = json.loads(response.content)
    assert 2 == len(inquiries_data["inquiries"])


@pytest.mark.django_db
@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
def test_inquiries_returns_only_returns_answered_inquiries(rf: RequestFactory):
    # given that there is a answered and a new inquiry present in the database
    answered = AvailabilityInquiryFactory(state=Inquiry.State.ANSWERED)
    AvailabilityInquiryFactory(state=Inquiry.State.NEW)
    # When I call the inquiries endpoint
    request = rf.get(
        "inquiries/ignored",
        HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN",
    )
    response = views.inquiries(request)
    # I expect that the response contains only the answered inquiry
    inquiries_data = json.loads(response.content)
    assert 1 == len(inquiries_data["inquiries"])
    assert str(answered.uid) == inquiries_data["inquiries"][0]["uid"]


@pytest.mark.django_db
@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="VALID")
def test_inquiries_returns_empty_json_if_no_inquiries_exist(rf: RequestFactory):
    response = views.inquiries(rf.get("...", HTTP_AUTHORIZATION="Token VALID"))
    assert response.status_code == 200
    assert json.loads(response.content)["inquiries"] == []


####################
# application_update


@pytest.mark.django_db
def test_application_update_rejects_availabilities_when_missing_selection(
    rf: RequestFactory,
):
    mission1_uid = uuid4()
    inquiry = AvailabilityInquiryFactory(
        data__missions=[
            AvailabilityFactory(mission=mission1_uid),
            AvailabilityFactory(),
        ]
    )
    availability = [{"mission": str(mission1_uid), "availability": "yes"}]
    request = rf.post(
        "application_update",
        json.dumps({"token": str(inquiry.uid), "missions": availability}),
        content_type="application/json",
    )
    response = views.application_update(request)
    assert response.status_code == 422


@pytest.mark.django_db
def test_application_update_rejects_availabilities_when_selection_has_invalid_value(
    rf: RequestFactory,
):
    uid = uuid4()
    inquiry = AvailabilityInquiryFactory(data__missions__1__mission=uid)
    availability = [{"mission": str(uid), "availability": "wtf"}]
    request = rf.post(
        "application_update",
        json.dumps({"token": str(inquiry.uid), "missions": availability}),
        content_type="application/json",
    )
    response = views.application_update(request)
    assert response.status_code == 422


@pytest.mark.django_db
def test_application_update_rejects_availabilities_when_selection_is_None(
    rf: RequestFactory,
):
    uid = uuid4()
    inquiry = AvailabilityInquiryFactory(data__missions__1__mission=uid)
    availability = [{"mission": str(uid), "availability": None}]
    request = rf.post(
        "application_update",
        json.dumps({"token": str(inquiry.uid), "missions": availability}),
        content_type="application/json",
    )
    response = views.application_update(request)
    assert response.status_code == 422


##########
# HomeView


def test_HomeView_contains_application_form(rf: RequestFactory):
    response = views.HomeView.as_view()(rf.get(".."))
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")
    assert len(html.find_all("application-form")) == 1


def test_HomeView_data_protection_document_links_works(client: Client):
    home = client.get("/")
    html = BeautifulSoup(
        cast(TemplateResponse, home).rendered_content, features="html.parser"
    )
    data_protection_info = html.find(id="data-protection-info")
    assert isinstance(data_protection_info, Tag)
    links = [
        l
        for l in data_protection_info.find_all("a")
        if l["href"].startswith(settings.STATIC_URL)
    ]
    assert (
        links
    ), "no staticfiles link found in data protection, either bug or useless test"

    for link in links:
        file = unquote(link["href"].removeprefix(settings.STATIC_URL))
        assert finders.find(file)
