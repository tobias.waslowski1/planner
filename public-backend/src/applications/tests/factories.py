import dataclasses
from datetime import timezone
from typing import Callable, ClassVar

from factory import Faker, LazyAttribute, SubFactory
from factory.django import DjangoModelFactory
from planner_common import schema
from planner_common.models import Gender
from planner_common.test.factories import (
    ApplicationData,
    AvailabilityInquiryDataFactory,
    CertificatesListFactory,
    TypedFactoryMetaClass,
)

from ..models import Application, Inquiry


class ApplicationFactory(
    DjangoModelFactory, metaclass=TypedFactoryMetaClass[Application]
):
    class Meta:
        model = Application

    create: ClassVar[Callable[..., Application]]
    build: ClassVar[Callable[..., Application]]

    class Params:
        first_name = Faker("first_name")
        last_name = Faker("last_name")
        email = Faker("email")
        phone_number = Faker("phone_number")
        date_of_birth = Faker("date", pattern="%Y-%m-%d")
        gender = Faker("random_element", elements=Gender.values)
        nationalities = Faker(
            "random_elements",
            elements=["cz", "de", "fr", "gg", "sh", "ve"],
            unique=True,
        )
        spoken_languages = [{"language": "deu", "points": 3}]
        motivation = Faker("paragraph")
        qualification = Faker("paragraph")
        positions = Faker(
            "random_elements",
            elements=schema.initial_application["properties"]["positions"]["items"][
                "allOf"
            ][0]["enum"],
            length=2,
            unique=True,
        )
        certificates = SubFactory(CertificatesListFactory)
        other_certificate = Faker("job")

    data = LazyAttribute(
        lambda params: dataclasses.asdict(
            # We go through ApplicationData to ensure that we didn't miss any fields
            ApplicationData(
                **{
                    field: getattr(params, field)
                    for field in ApplicationData.__dataclass_fields__.keys()
                }
            )
        )
    )
    created = Faker("past_datetime", tzinfo=timezone.utc)


class AvailabilityInquiryFactory(
    DjangoModelFactory, metaclass=TypedFactoryMetaClass[Inquiry]
):
    class Meta:
        model = Inquiry
        rename = {"data_dict": "data"}

    create: ClassVar[Callable[..., Inquiry]]
    build: ClassVar[Callable[..., Inquiry]]

    class Params:
        data = SubFactory(AvailabilityInquiryDataFactory)

    uid = Faker("uuid4", cast_to=None)
    data_dict = LazyAttribute(lambda o: o.data.to_dict())
    type = Inquiry.Type.AVAILABILITY
    created = Faker("past_datetime", tzinfo=timezone.utc)
