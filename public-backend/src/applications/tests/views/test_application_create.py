import dataclasses
import json
from http import HTTPStatus
from logging import LogRecord
from unittest import mock

import pytest
from django.core import mail
from django.http import HttpResponseForbidden, HttpResponseNotAllowed
from django.test import Client, RequestFactory
from django.urls import reverse
from freezegun import freeze_time
from planner_common.test.factories import ApplicationData, ApplicationDataFactory

from ... import views
from ...models import Application

expected_status = HTTPStatus.NO_CONTENT


@pytest.mark.django_db
def test_is_reachable(client: Client):
    application_data = ApplicationDataFactory()
    response = client.post(
        reverse("application_create"),
        application_data.as_json(),
        content_type="application/json",
    )
    assert HTTPStatus(response.status_code) == expected_status


def test_raises_get_not_allowed(rf: RequestFactory):
    request = rf.get("application_create")
    response = views.application_create(request)
    assert isinstance(response, HttpResponseNotAllowed)


@pytest.mark.django_db
def test(rf: RequestFactory):
    alice = ApplicationDataFactory()

    request = rf.post(
        "application_create",
        alice.as_json(),
        content_type="application/json",
    )
    response = views.application_create(request)
    assert HTTPStatus(response.status_code) == expected_status
    assert len(response.content) == 0

    application = Application.objects.last()
    assert application

    assert application.data["first_name"] == alice.first_name
    assert application.data["last_name"] == alice.last_name
    assert application.data["email"] == alice.email
    assert application.data["date_of_birth"] == alice.date_of_birth
    assert application.data["gender"] == alice.gender
    assert application.data["motivation"] == alice.motivation
    assert application.data["qualification"] == alice.qualification
    assert application.data["phone_number"] == alice.phone_number
    assert application.data["positions"] == alice.positions

    assert len(application.data["spoken_languages"]) == 1
    assert (
        application.data["spoken_languages"][0]["language"]
        == alice.spoken_languages[0]["language"]
    )
    assert (
        application.data["spoken_languages"][0]["points"]
        == alice.spoken_languages[0]["points"]
    )


@pytest.mark.django_db
def test_rejects_invalid_positions(rf: RequestFactory):
    alice = ApplicationDataFactory(positions=["invalid"])

    request = rf.post(
        "application_create",
        alice.as_json(),
        content_type="application/json",
    )
    response = views.application_create(request)
    assert response.status_code == 422

    assert Application.objects.count() == 0


@pytest.mark.django_db
def test_rejects_duplicate_positions(rf: RequestFactory):
    alice = ApplicationDataFactory(
        positions=[
            ApplicationData.valid_positions()[0],
            ApplicationData.valid_positions()[0],
        ]
    )

    request = rf.post(
        "application_create",
        alice.as_json(),
        content_type="application/json",
    )
    response = views.application_create(request)
    assert response.status_code == 422

    assert Application.objects.count() == 0


@pytest.mark.django_db
def test_saves_nationalities(rf: RequestFactory):
    expected_nationalities = ["DE", "GB"]
    alice = ApplicationDataFactory(nationalities=expected_nationalities)

    request = rf.post(
        "application_create",
        alice.as_json(),
        content_type="application/json",
    )
    response = views.application_create(request)
    assert HTTPStatus(response.status_code) == expected_status

    assert Application.objects.count() == 1
    application = Application.objects.last()
    assert application
    assert application.data["nationalities"] == expected_nationalities


@pytest.mark.django_db
def test_fails_with_missing_fields(rf: RequestFactory):
    request = rf.post("application_create", {}, content_type="application/json")
    response = views.application_create(request)
    assert response.status_code == 422
    assert json.loads(response.content) == {
        "ok": False,
        "message": "'first_name' is a required property",
    }


@pytest.mark.django_db
def test_accepts_minimal_application(rf: RequestFactory):
    # test if minimal application is complete
    data = ApplicationDataFactory()
    request = rf.post(
        "application_create",
        data.to_dict(),
        content_type="application/json",
    )
    assert views.application_create(request).status_code == 204


@pytest.mark.django_db
def test_raises_without_mandatory_fields(rf: RequestFactory):
    for field in dataclasses.fields(ApplicationData):
        if field.name == "other_certificate":
            # This is optional
            continue

        application_data_dto = ApplicationDataFactory()
        application_data = application_data_dto.to_dict()
        application_data.pop(field.name)
        request = rf.post(
            "application_create",
            application_data,
            content_type="application/json",
        )
        assert (
            views.application_create(request).status_code == 422
        ), f"Application was created with missing field {field.name}"


@pytest.mark.django_db
def test_raises_with_additional_properties(rf: RequestFactory):
    application_data = ApplicationDataFactory()
    application = application_data.to_dict()
    application["x"] = 666
    request = rf.post(
        "application_create",
        application,
        content_type="application/json",
    )
    assert views.application_create(request).status_code == 422
    assert (
        json.loads(views.application_create(request).content)["message"]
        == "Additional properties are not allowed ('x' was unexpected)"
    )


@pytest.mark.django_db
def test_raises_with_additional_property_in_language(
    rf: RequestFactory,
):
    application_data = ApplicationDataFactory()
    application = application_data.to_dict()
    application["spoken_languages"] = [{"language": "deu", "points": 5, "y": 6}]
    request = rf.post(
        "application_create",
        application,
        content_type="application/json",
    )
    assert views.application_create(request).status_code == 422
    assert (
        json.loads(views.application_create(request).content)["message"]
        == "Additional properties are not allowed ('y' was unexpected)"
    )


@freeze_time("2020-04-07")
@pytest.mark.django_db
def test_raises_with_future_date_of_birth(rf: RequestFactory):
    application_data = ApplicationDataFactory()
    application = application_data.to_dict()
    application["date_of_birth"] = "2020-04-08"

    request = rf.post(
        "application_create",
        application,
        content_type="application/json",
    )
    assert views.application_create(request).status_code == 422


@pytest.mark.django_db
def test_raises_with_invalid_date_of_birth(rf: RequestFactory):
    application_data = ApplicationDataFactory()
    application = application_data.to_dict()
    application["date_of_birth"] = "abcd-ef-gh"

    request = rf.post(
        "application_create",
        application,
        content_type="application/json",
    )
    assert views.application_create(request).status_code == 422


@pytest.mark.django_db
def test_doesnt_require_csrf_token():
    application_data = ApplicationDataFactory()
    client = Client(enforce_csrf_checks=True)
    response = client.post(
        reverse("application_create"),
        application_data.as_json(),
        content_type="application/json",
    )
    assert not isinstance(response, HttpResponseForbidden)


@pytest.mark.django_db
def test_sends_email(rf: RequestFactory):
    alice = ApplicationDataFactory(first_name="Alice", email="alice@example.com")

    request = rf.post(
        "application_create",
        alice.as_json(),
        content_type="application/json",
    )
    response = views.application_create(request)
    assert HTTPStatus(response.status_code) == expected_status

    assert len(mail.outbox) == 1
    confirmation_email = mail.outbox[0]
    assert confirmation_email.to == ["alice@example.com"]
    assert "Alice" in confirmation_email.body
    assert "Thank you for signing up" in confirmation_email.body
    assert "http://" not in confirmation_email.body


@pytest.mark.django_db
def test_logs_and_returns_200_if_sending_email_fails(rf: RequestFactory, caplog):
    exception = Exception("marvellous mr watson")
    with mock.patch.object(views.mail, "send_mail") as mail_mock:
        mail_mock.side_effect = exception

        response = views.application_create(
            rf.post(
                "",
                ApplicationDataFactory(email="major.tonb@example.com").as_json(),
                content_type="application/json",
            )
        )
    assert HTTPStatus(response.status_code) == expected_status
    records = caplog.records
    assert len(records) == 1
    r: LogRecord = records[0]
    assert r.name == "applications.views"
    assert r.levelname == "ERROR"
    assert r.message == f"Error sending application confirmation mail to applicant"
    assert r.__dict__.get("applicant_email") == "major.tonb@example.com"
    assert r.exc_info and r.exc_info[1] == exception
