import pytest
from bs4 import BeautifulSoup
from django.test import Client
from django.test.client import RequestFactory
from pytest_django.asserts import assertContains

from ...views import HomeView


def test_renders_application_form_with_correct_title(client: Client):
    response = client.get("/")
    html = BeautifulSoup(response.content, features="html.parser")
    title = html.find("title")
    assert title
    assert title.getText().startswith("Join the Crew • Sea-Watch")


def test_renders_default_title(client: Client):
    response = client.get("/applications/application_success/")
    html = BeautifulSoup(response.content, features="html.parser")
    title = html.find("title")
    assert title
    assert title.getText() == ("Crew Management • Sea-Watch")


@pytest.mark.django_db
def test_shows_data_protection_note(rf: RequestFactory):
    response = HomeView.as_view()(rf.get(".."))
    assertContains(response, "gemäß Art. 13 DSGVO")
