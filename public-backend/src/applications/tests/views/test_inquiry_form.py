import json
from datetime import date
from uuid import UUID

import pytest
from bs4 import BeautifulSoup
from bs4.element import Tag
from django.http import Http404
from django.test import Client, RequestFactory
from django.urls import reverse
from planner_common import dtos
from planner_common.test.factories import AvailabilityFactory
from pytest_django.asserts import assertContains

from applications.models import Inquiry
from applications.tests.factories import AvailabilityInquiryFactory
from applications.views import inquiry_form


@pytest.mark.django_db
def test_renders_availability_form(client: Client):
    inquiry_uid = UUID("96da59c5-fb5e-4227-8f7e-51772b469828")
    mission_uid = UUID("56393aa8-d23f-48c5-88f9-a39914f8260a")
    AvailabilityInquiryFactory(
        uid=inquiry_uid,
        data__name="Cooper",
        data__missions=[
            AvailabilityFactory(
                start_date=date(2021, 8, 9),
                end_date=date(2021, 12, 3),
                mission=mission_uid,
                availability=dtos.Availability.Answer.Unknown,
            )
        ],
    )
    response = client.get(reverse("inquiry_form", kwargs={"uid": inquiry_uid}))
    html = BeautifulSoup(response.content, features="html.parser")
    form = html.find("availability-form")
    assert isinstance(form, Tag)
    missions_json = form["missions"]
    assert isinstance(missions_json, str)
    assert json.loads(missions_json) == [
        {
            "availability": "unknown",
            "start_date": "2021-08-09",
            "end_date": "2021-12-03",
            "mission": str(mission_uid),
        }
    ]
    assert form["name"] == "Cooper"
    assert form["token"] == str(inquiry_uid)


@pytest.mark.django_db
def test_404s_for_answered_inquiries(rf: RequestFactory):
    uid = UUID("cd58cf32-a89b-4e40-9e0c-5f60e43f3b88")
    AvailabilityInquiryFactory(state=Inquiry.State.ANSWERED)
    with pytest.raises(Http404):
        inquiry_form(rf.get(".."), uid=uid)


@pytest.mark.django_db
def test_renders_availability_form_with_correct_title(client: Client):
    inquiry_uid = UUID("96da59c5-fb5e-4227-8f7e-51772b469828")
    AvailabilityInquiryFactory(
        uid=inquiry_uid,
    )
    response = client.get(reverse("inquiry_form", kwargs={"uid": inquiry_uid}))
    html = BeautifulSoup(response.content, features="html.parser")
    title = html.find("title")
    assert title
    assert title.getText().startswith("Update Availabilities")


@pytest.mark.django_db
def test_shows_data_protection_note(rf: RequestFactory):
    inquiry_uid = UUID("b422d3e8-8a0d-4f50-8d33-c0373ecf095c")
    AvailabilityInquiryFactory.create(
        uid=inquiry_uid,
    )
    response = inquiry_form(rf.get(".."), uid=inquiry_uid)
    assertContains(response, "gemäß Art. 13 DSGVO")
