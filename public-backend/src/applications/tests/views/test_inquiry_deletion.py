import json
from http import HTTPStatus
from uuid import uuid4

import pytest
from django.core.exceptions import PermissionDenied
from django.test.client import Client, RequestFactory
from django.test.utils import override_settings
from django.urls.base import reverse

from applications import views
from applications.models import Inquiry
from applications.tests.factories import AvailabilityInquiryFactory


@pytest.mark.django_db
@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
def test_inquiry_delete_is_reachable(client: Client):
    response = client.delete(
        reverse("inquiry_delete", kwargs={"uid": uuid4()}),
        HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN",
    )
    assert response.status_code == 404


@pytest.mark.django_db
@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
def test_inquiry_delete_deletes_inquiry_in_db(rf: RequestFactory):
    uid = uuid4()
    AvailabilityInquiryFactory(uid=uid)
    request = rf.delete(
        "inquiry_delete", HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN"
    )
    response = views.inquiry_delete(request, uid=uid)
    assert response.status_code == 200
    assert json.loads(response.content) == {"ok": True}
    assert Inquiry.objects.filter(uid=uid).count() == 0


def test_inquiry_delete_requires_token(rf: RequestFactory):
    request = rf.delete("inquiry_delete")
    with pytest.raises(PermissionDenied):
        views.inquiry_delete(request, uid="asldalks")


@pytest.mark.parametrize("method", ["get", "post", "put"])
@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="VALID")
def test_inquiry_delete_requires_delete(rf: RequestFactory, method: str):
    request = getattr(rf, method)("inquiry_delete", HTTP_AUTHORIZATION="Token VALID")
    response = views.inquiry_delete(request, uid=uuid4())
    assert HTTPStatus(response.status_code) == HTTPStatus.METHOD_NOT_ALLOWED
