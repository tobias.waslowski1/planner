from unittest.mock import Mock, patch

import pytest
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseForbidden
from django.test import RequestFactory, override_settings

from ..token_authentication import (
    raise_on_invalid_auth_token,
    requires_token_authentication,
)

############################
# raise_on_invalid_auth_token


def test_check_token_auth_raises_on_missing_header(rf: RequestFactory):
    with pytest.raises(PermissionDenied):
        raise_on_invalid_auth_token(rf.get("/"))


@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="VALID")
def test_check_token_auth_raises_on_wrong_header(rf: RequestFactory):
    with pytest.raises(PermissionDenied):
        raise_on_invalid_auth_token(rf.get("/", HTTP_AUTHORIZATION="Token WRONG"))


@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="VALID")
def test_check_token_auth_doesnt_raise_with_correct_token(rf: RequestFactory):
    raise_on_invalid_auth_token(rf.get("/", HTTP_AUTHORIZATION="Token VALID"))


###############################
# requires_token_authentication


@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="VALID")
def test_requires_token_authentication_raises_and_doesnt_call_handler_on_invalid_token(
    rf: RequestFactory,
):
    handler = Mock()
    with pytest.raises(PermissionDenied):
        requires_token_authentication(handler)(rf.get("/"))

    assert handler.call_count == 0


@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="VALID")
def test_requires_token_authentication_passes_through_to_handler_on_valid_token(
    rf: RequestFactory,
):
    handler = Mock(return_value="response")
    actual = requires_token_authentication(handler)(
        rf.get("/", HTTP_AUTHORIZATION="Token VALID")
    )

    assert actual == "response"
