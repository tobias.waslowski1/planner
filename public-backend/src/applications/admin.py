from django.contrib import admin

from . import models

# Register your models here.


@admin.register(models.Application)
class Application(admin.ModelAdmin):
    readonly_fields = ("created",)
