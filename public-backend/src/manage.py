#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import logging
import os
import sys
from pathlib import Path

logger = logging.getLogger(__name__)


def main():
    use_dev_settings = (
        Path(__file__).resolve().parent.parent.parent / ".use-dev-settings"
    ).exists()
    settings_module = "dev" if use_dev_settings else "prod"
    os.environ.setdefault(
        "DJANGO_SETTINGS_MODULE", f"planner_public_backend.settings.{settings_module}"
    )

    try:
        from django.core.management import execute_from_command_line
        from django.core.management.commands.runserver import Command as runserver
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc

    # Override default port for `runserver` command
    runserver.default_port = 8001

    execute_from_command_line(sys.argv)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception("Error while running management command")
