from .base import *

DEBUG = True

SECRET_KEY = "insecure secret key"

PUBLIC_CLIENT_DIR = REPO_DIR + "public-client"

STATICFILES_DIRS = [PUBLIC_CLIENT_DIR("dist")]

WEBPACK_LOADER = {
    "DEFAULT": {
        **WEBPACK_LOADER_DEFAULT_DEFAULTS,
        "CACHE": False,
        "STATS_FILE": PUBLIC_CLIENT_DIR("webpack-stats.json"),
    }
}

# EMAIL CONFIG
EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
EMAIL_FILE_PATH = PROJECT_DIR("sent_emails")
DEFAULT_FROM_EMAIL = "dev@local-sea-watch.test"
EMAIL_URL_HOST = "example.com"

PLANNER_PUBLIC_BACKEND_AUTH_TOKEN = "insecure-preshared-token-for-dev"

DATABASES = {"default": env.db(default=f"sqlite:///{PROJECT_DIR}/db.sqlite")}

SHOW_ENV_WARNING = "dev"
