from .base import *

DEBUG = True
SKIP_JS = True

SECRET_KEY = "insecure secret key for testing"

DATABASES = {"default": {"ENGINE": "django.db.backends.sqlite3", "NAME": ":memory:"}}

# Email Config
EMAIL_BACKEND = "django.core.mail.backends.filebased.EmailBackend"
EMAIL_FILE_PATH = PROJECT_DIR("sent_emails")
DEFAULT_FROM_EMAIL = "dev@planner.sea-watch.org"
EMAIL_URL_HOST = "example.com"
