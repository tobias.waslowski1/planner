import planner_common.features
from django.conf import settings


def skip_js(request):
    try:
        skip_js = settings.SKIP_JS
    except AttributeError:
        skip_js = False

    return {"SKIP_JS": skip_js}


colors = {
    "dev": "green",
    "staging": "hotpink",
}


def env_warning(request):
    env = settings.SHOW_ENV_WARNING
    if not env:
        return {}
    return {"env_warning": {"color": colors[env], "message": " ".join([env] * 100)}}


def features(request):
    return {
        "features": {
            k: v for k, v in planner_common.features.__dict__.items() if type(v) == bool
        }
    }
