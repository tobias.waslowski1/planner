##############
## Basic setup
terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/20207631/terraform/state/planner-staging"
    lock_address   = "https://gitlab.com/api/v4/projects/20207631/terraform/state/planner-staging/lock"
    lock_method    = "POST"
    unlock_address = "https://gitlab.com/api/v4/projects/20207631/terraform/state/planner-staging/lock"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "1.32.2"
    }
    ansible = {
      source  = "nbering/ansible"
      version = "1.0.4"
    }
  }
}

variable "hcloud_token" { sensitive = true }
variable "deploy_ssh_key_pub" {}

provider "hcloud" { token = var.hcloud_token }
provider "ansible" {}

###########
## SSH Keys

resource "hcloud_ssh_key" "simon" {
  name       = "simon"
  public_key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBQMwG8/RLsTUGpxUC/DAWFM+BS0lq3OWOpEyK3k4SXV simon"
  labels     = {}
}

resource "hcloud_ssh_key" "deploy" {
  name       = "deploy"
  public_key = var.deploy_ssh_key_pub
  labels     = {}
}

# ##################
# ## Cloud resources

# get the ip with `terraform state show hcloud_floating_ip.planner-staging`
resource "hcloud_floating_ip" "planner-staging" {
  type              = "ipv4"
  home_location     = "fsn1"
  delete_protection = true
}

resource "hcloud_floating_ip_assignment" "planner-staging" {
  floating_ip_id = hcloud_floating_ip.planner-staging.id
  server_id      = hcloud_server.planner-staging.id
}

# resource "hcloud_rdns" "floating_planner-staging" {
#   floating_ip_id = hcloud_floating_ip.planner-staging.id
#   ip_address = hcloud_floating_ip.planner-staging.ip_address
# }

resource "hcloud_server" "planner-staging" {
  name        = "planner-staging"
  server_type = "cx11"
  image       = "debian-11"
  location    = "fsn1"
  labels      = {}

  ssh_keys = [
    hcloud_ssh_key.simon.name,
    hcloud_ssh_key.deploy.name,
  ]
}

# ################################
# ## Ansible inventory information

resource "ansible_host" "planner-staging" {
  inventory_hostname = hcloud_server.planner-staging.name
  vars = {
    ansible_user = "root"
    ansible_host = hcloud_server.planner-staging.ipv4_address
  }
}

resource "ansible_host_var" "floating_ip" {
  inventory_hostname = hcloud_server.planner-staging.name
  key                = "floating_ip"
  value              = hcloud_floating_ip.planner-staging.ip_address
}
