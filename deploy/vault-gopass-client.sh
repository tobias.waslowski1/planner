#!/usr/bin/env sh

if [ -n "${vault_password}" ]; then
    echo ${vault_password}
else
    exec gopass show -o seawatch-crewingdb/staging/ansible-vault
fi
