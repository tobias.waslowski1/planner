# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hetznercloud/hcloud" {
  version     = "1.32.2"
  constraints = "1.32.2"
  hashes = [
    "h1:xJuDRJWUuQq0ROVwa1VrtXaUVNPP6KvIWZbmDuOQgJM=",
    "zh:06d9ad67eb39397867f41a6a691e1198853c2eeaa91723ea126d14f9921380b5",
    "zh:193cfe86a0161ad3986f6b31daeae4b55c35853dddeba4cfd7c05f14ee1efbf8",
    "zh:2a13de4a174dc43f429a9f98c3a61fc3929b234d475cc26e968f1b7511a7295c",
    "zh:4709968e190d1ff774e524b6577a6b35ae17f9cc0e5cbfc18d1b6fd0c88b529f",
    "zh:5001f0ee6a2a9c75fab28ab3cd84f00813f2b050a3a93534fbadabc42a26cc31",
    "zh:67534f3f4c01cc8d5b3aeae021e0529d8f56f02b19fbab5119b865c477e64324",
    "zh:696f53d3ce4f385209c2efc995d6fc6c139c6bb3dff2bd8baa372a5189c40570",
    "zh:713f876c2d59858c3d6340448575e64d769e770e5e25408fd44b39e0ef3c58cb",
    "zh:71c38e24e8d646e77fc73e2ee28416ebcf6be99aa6d4421dfd3674237ccaaa8c",
    "zh:817a8eb38d97400c0d3e2aa9f9fcc3fb7c5cc752fd15380c78069aa42a191ce1",
    "zh:92e1659e33e3dd2d23c2579a8a9497a270d22dbd867c6e8fafdf1aceb02bd052",
    "zh:a94b51f4c189308b47818112a170af1f5015b35b8482d5be46dcba9d9afd5b57",
    "zh:b5445f6ca4b4207be4677786c802c3a91d2fa543dac88d7e8bc570feb7fdc20d",
    "zh:d0ed8e2b24793bbd38e80ea9ed98bc561becd850e6c1320d94000783fbd4964d",
  ]
}

provider "registry.terraform.io/nbering/ansible" {
  version     = "1.0.4"
  constraints = "1.0.4"
  hashes = [
    "h1:G9OI+CZMXBdfe0Oa+/B7zOVronatrovHxDlvpuy5VOE=",
    "zh:0601b46fc129828b7351662c46946a099e57c97a35eefdba111e97feaed00be1",
    "zh:0a6754c2b93146bbb50a6e7ac6d9b596da352bf43474ac65ebd0214ac703ce92",
    "zh:16cc61e49ab806a7e7b1e510d915c3563c89147e7f49d1cb857b4a98555d58e2",
    "zh:3ad5b27cc4ae71c0794b6c3a0abb5bbc74d750b34ab83bed0a232c722514dabe",
    "zh:4dca1f8fcf5a0749994346f1dc6c7f7284469b80d87370b5bae7e812234d1eba",
    "zh:587ef471ea30c9505fa39570c49c5ed2222d4569e5909982965a963209f5c67f",
    "zh:5cfaae8e4ef9754156008418fc74224461b7ad101fa86812af7315a7e79fd33f",
    "zh:720510a04d17c4593969dd31b44063a0ecca53d930e9616a7714c372b4973e13",
    "zh:84d57af64bfbd007440c6916c40a9bed6c5226b72549e8c408c80981bc9aa13e",
    "zh:e1fe7f6c9826f905333813332e027529d2dae30043c3903afcff05be302b940c",
    "zh:fe5e9e7a8550e97933e50d5471b4e13338754aff76524ecb1cb50bd6bce61145",
  ]
}
