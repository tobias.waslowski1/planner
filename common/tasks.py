import shlex
import sys

from invoke import task


@task
def install(c):
    c.run("poetry install")


@task
def test(c):
    "Run the tests"
    run_with_passthrough(c, "pytest", pty=True)


@task
def watch(c):
    "Continuously re-run all tests whenever files are changed"
    run_with_passthrough(c, "watchexec --clear -e py,html -- pytest", pty=True)


@task
def tdd_reset(c):
    """
    Clear caches and everything.

    Shouldn't usually be necessary, try this if tdd is weird
    """
    c.run("rm -rf .pytest_cache .testmondata", echo="both")


@task(tdd_reset)
def tdd(c):
    "Continuousy re-run affected tests whenever files are changed"
    run_with_passthrough(
        c, "watchexec --clear -e py,html -- pytest --testmon", pty=True
    )


#####################################################################
## Helper functions


def run_with_passthrough(c, cmd, *args, **kwargs):
    return c.run(" ".join([cmd, passthrough_args()]), *args, **kwargs)


def passthrough_args() -> str:
    try:
        dashdash_position = sys.argv.index("--")
    except ValueError:
        return ""
    return " ".join(map(shlex.quote, sys.argv[dashdash_position + 1 :]))
