from __future__ import annotations

from dataclasses import dataclass
from datetime import date, datetime
from enum import Enum
from uuid import UUID


@dataclass
class Availability:
    class Answer(Enum):
        Yes = "yes"
        No = "no"
        Maybe = "maybe"
        Unknown = (
            "unknown"  # TODO this is not really an answer and should be factored out
        )

        def __repr__(self):
            return f"<{self.__class__.__name__}.{self.name}>"

    mission: UUID
    start_date: date
    end_date: date
    availability: Answer

    def to_dict(self):
        return {
            "mission": str(self.mission),
            "start_date": self.start_date.isoformat(),
            "end_date": self.end_date.isoformat(),
            "availability": self.availability.value,
        }

    @classmethod
    def from_dict(cls, d: dict) -> Availability:
        return cls(
            mission=UUID(d["mission"]),
            start_date=date.fromisoformat(d["start_date"]),
            end_date=date.fromisoformat(d["end_date"]),
            availability=cls.Answer(d["availability"].lower()),
        )


@dataclass
class AvailabilityInquiryData:
    email: str
    name: str
    missions: list[Availability]

    def to_dict(self):
        return {
            "email": self.email,
            "name": self.name,
            "missions": [availability.to_dict() for availability in self.missions],
        }

    @classmethod
    def from_dict(cls, d: dict) -> AvailabilityInquiryData:
        return cls(
            email=d["email"],
            name=d["name"],
            missions=[Availability.from_dict(a) for a in d["missions"]],
        )


@dataclass
class Inquiry:
    # TODO: This name is tricky, because it's not actually used as the inquiry (the backoffice only
    # sends the AvailabilityInquiryData) but as the envelope for the answer!
    class Type(Enum):
        Availability = "availability"

        def __repr__(self):
            return f"<{self.__class__.__name__}.{self.name}>"

    uid: UUID
    type: Type
    data: AvailabilityInquiryData

    # TODO: I think this currently referes to the time the inquiry was sent to the
    # public backend. That's not very useful imo.
    created: datetime

    def to_dict(self) -> dict:
        return {
            "uid": str(self.uid),
            "type": self.type.value,
            "created": self.created.isoformat(),
            "data": self.data.to_dict(),
        }

    @classmethod
    def from_dict(cls, d) -> Inquiry:
        return cls(
            uid=UUID(d["uid"]),
            created=datetime.fromisoformat(d["created"]),
            type=cls.Type(d["type"]),
            data=AvailabilityInquiryData.from_dict(d["data"]),
        )
