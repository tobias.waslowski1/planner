import dataclasses
import json
from pathlib import Path
from typing import Callable, ClassVar, Generic, List, TypeVar

import factory
from factory import Factory, Faker, SelfAttribute, SubFactory
from factory.base import FactoryMetaClass

from planner_common import data, schema
from planner_common.dtos import Availability, AvailabilityInquiryData, Inquiry
from planner_common.models import Gender

available_positions = schema.initial_application["properties"]["positions"]["items"][
    "allOf"
][0]["enum"]
assert "master" in available_positions


T = TypeVar("T")


class TypedFactoryMetaClass(Generic[T], FactoryMetaClass):
    __call__: Callable[..., T]
    # Also add the following to the factory definition:
    #
    # create: ClassVar[Callable[..., YourType]]
    # build: ClassVar[Callable[..., YourType]]


class CertificatesListFactory(Factory, metaclass=TypedFactoryMetaClass[list]):
    class Meta:
        model = list
        inline_args = ["elements"]

    create: ClassVar[Callable[..., list]]
    build: ClassVar[Callable[..., list]]

    class Params:
        count = Faker("random_int", min=0, max=3)

    elements = Faker(
        "random_sample",
        elements=[
            certificate["value"]
            for group in data.certificates
            for certificate in group["certificates"]
        ],
        length=SelfAttribute("..count"),
    )


@dataclasses.dataclass
class ApplicationData:
    first_name: str
    last_name: str
    email: str
    phone_number: str
    date_of_birth: str
    gender: str
    nationalities: list
    spoken_languages: list
    motivation: str
    qualification: str
    positions: List[str]
    certificates: List[str]
    other_certificate: str

    @staticmethod
    def valid_positions():
        return available_positions

    def to_dict(self):
        return dataclasses.asdict(self)

    def as_json(self):
        return json.dumps(self.to_dict())


class ApplicationDataFactory(Factory, metaclass=TypedFactoryMetaClass[ApplicationData]):
    class Meta:
        model = ApplicationData

    create: ClassVar[Callable[..., ApplicationData]]
    build: ClassVar[Callable[..., ApplicationData]]

    first_name = Faker("first_name")
    last_name = Faker("last_name")
    email = Faker("email")
    phone_number = Faker("phone_number")
    date_of_birth = Faker("date", pattern="%Y-%m-%d")
    gender = Faker("random_element", elements=Gender.values)
    positions = Faker(
        "random_elements", elements=ApplicationData.valid_positions(), unique=True
    )
    # @todo make this use faker
    nationalities = ["de", "gb", "pl"]
    spoken_languages = [{"language": "de", "points": 5}]
    motivation = Faker("paragraph")
    qualification = Faker("paragraph")
    phone_number = Faker("bothify", text="+############")
    certificates = SubFactory(CertificatesListFactory)
    other_certificate = Faker("job")


class AvailabilityFactory(Factory, metaclass=TypedFactoryMetaClass[Availability]):
    class Meta:
        model = Availability

    create: ClassVar[Callable[..., Availability]]
    build: ClassVar[Callable[..., Availability]]

    mission = Faker("uuid4", cast_to=None)
    start_date = Faker("date_between", end_date=SelfAttribute("..end_date"))
    end_date = Faker("future_date")
    availability = Faker("random_element", elements=list(Availability.Answer))


class AvailabilityInquiryDataFactory(
    Factory, metaclass=TypedFactoryMetaClass[AvailabilityInquiryData]
):
    class Meta:
        model = AvailabilityInquiryData

    create: ClassVar[Callable[..., AvailabilityInquiryData]]
    build: ClassVar[Callable[..., AvailabilityInquiryData]]

    email = Faker("email")
    name = Faker("first_name")
    missions = factory.List([SubFactory(AvailabilityFactory) for _ in range(4)])


class InquiryFactory(Factory, metaclass=TypedFactoryMetaClass[Inquiry]):
    class Meta:
        model = Inquiry

    create: ClassVar[Callable[..., Inquiry]]
    build: ClassVar[Callable[..., Inquiry]]

    uid = Faker("uuid4", cast_to=None)  # casts to string by default
    type = Inquiry.Type.Availability
    created = Faker("past_datetime", start_date="-1d")
    data = SubFactory(AvailabilityInquiryDataFactory)
