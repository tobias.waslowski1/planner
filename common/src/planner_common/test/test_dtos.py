import json

from pytest import mark

from planner_common.dtos import Availability, AvailabilityInquiryData, Inquiry
from planner_common.test.factories import (
    AvailabilityFactory,
    AvailabilityInquiryDataFactory,
    InquiryFactory,
)


def assert_roundtrips_to_json(cls, dto):
    assert cls.from_dict(json.loads(json.dumps(dto.to_dict()))) == dto


class TestInquiry:
    def test_roundtrip_dict_dto_dict(self):
        availability_data: AvailabilityInquiryData = AvailabilityInquiryDataFactory()  # type: ignore
        data: dict = {
            "uid": "7d6ea538-0287-4fa1-8174-004bc28e9b03",
            "type": "availability",
            "created": "2021-03-21T12:42:33+00:00",
            "data": availability_data.to_dict(),
        }
        assert Inquiry.from_dict(data).to_dict() == data

    def test_roundtrips_to_json(self):
        assert_roundtrips_to_json(Inquiry, InquiryFactory())


class TestAvailability:
    def test_roundtrip_dict_dto_dict(self):
        data = {
            "mission": "998ed864-a153-415f-b919-72e01e73447c",
            "start_date": "2021-01-04",
            "end_date": "2029-02-03",
            "availability": "maybe",
        }
        assert Availability.from_dict(data).to_dict() == data

    def test_roundtrips_to_json(self):
        assert_roundtrips_to_json(Availability, AvailabilityFactory())

    @mark.parametrize(
        "uppercased,expected", [(a.value.upper(), a) for a in Availability.Answer]
    )
    def test_parses_uppercase_answers(self, uppercased, expected):
        data = {**AvailabilityFactory.create().to_dict(), "availability": uppercased}

        assert Availability.from_dict(data).availability == expected


class TestAvailabilityInquiryData:
    def test_roundtrip_dict_dto_dict(self):
        data = {
            "email": "hell-yeah@example.tld",
            "name": "Willem Goldman",
            "missions": [a.to_dict() for a in AvailabilityFactory.build_batch(2)],
        }
        assert AvailabilityInquiryData.from_dict(data).to_dict() == data

    def test_roundtrips_to_json(self):
        assert_roundtrips_to_json(
            AvailabilityInquiryData, AvailabilityInquiryDataFactory()
        )
