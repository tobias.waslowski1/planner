import os
import sys
import warnings

additional_positions = True
""" gitlab #182, Simon"""

edit_volunteers = True
""" gitlab #149, Simon"""

archive_volunteers = False
""" gitlab #189, Eru/Simon"""

upload_cv = False
""" gitlab #158, Thibault"""

self_module = sys.modules[__name__]
for feature in os.environ.get("PLANNER_FEATURES", "").split(";"):
    if feature == "":
        # ''.split(';') == [''], which is annoying here
        continue

    if feature in self_module.__dict__:
        self_module.__dict__[feature] = True
    else:
        warnings.warn(f"Unknown feature {feature}")
