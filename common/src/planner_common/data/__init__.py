import json
from importlib.resources import open_text

with open_text("planner_common.data", "certificates.json") as f:
    certificates = json.load(f)

with open_text("planner_common.data", "languages.json") as f:
    languages = json.load(f)
