import { Validator } from "jsonschema";
import availabilitySchema from "common/schema/availability.json";
import applicationSchema from "common/schema/initialApplication.json";

class ResponseError extends window.Error {
  constructor(status, text) {
    super("ResponseError");
    this.status = status;
    this.text = text;
  }

  toString() {
    return `${super.toString()} (Status ${this.status})\n${this.text}`;
  }

  static async fromResponse(response) {
    return new ResponseError(response.status, await response.text());
  }
}

export async function sendApplicationForm(data) {
  const validationResult = new Validator().validate(data, applicationSchema);
  if (!validationResult.valid) {
    throw validationResult;
  }

  const response = await fetch("/applications/create/", {
    method: "POST",
    headers: { "Content-Type": "application/json", Accept: "application/json" },
    body: JSON.stringify(data),
  });

  if (response.status !== 204) {
    throw await ResponseError.fromResponse(response);
  }
}

/** @typedef {string} UUID */
/** @typedef {("yes" | "no" | "maybe" )} Availability */
/** @typedef {(Availability | "unknown")} AvailabilityOrUnknown */
/** @typedef {{mission: UUID, start_date: string, end_date: string, availability: Availability }} Mission */
/** @typedef {{mission: UUID, start_date: string, end_date: string, availability: AvailabilityOrUnknown }} MissionRequest */

/**
 * @param {UUID} token
 * @param {{mission: UUID, availability: Availability}[]} missions
 */
export async function sendAvailabilityForm(token, missions) {
  const data = { token: token, missions };
  const validationResult = new Validator().validate(data, availabilitySchema);
  if (!validationResult.valid) {
    throw validationResult;
  }

  // XXX: This url path isn't great
  const response = await fetch("/applications/update/", {
    method: "POST",
    headers: { "Content-Type": "application/json", Accept: "application/json" },
    body: JSON.stringify(data),
  });

  if (response.status !== 204) {
    throw await ResponseError.fromResponse(response);
  }
}
