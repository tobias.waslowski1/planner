import { LitElement, html } from "lit";
import styles from "./styles.lit.css";
import { sendAvailabilityForm } from "/APIService";

const choices = [
  { value: "yes", label: "Yes" },
  { value: "no", label: "No" },
  { value: "maybe", label: "Maybe" },
];

/**
 * @typedef {import("../../APIService").UUID} UUID
 * @typedef {import("../../APIService").Availability} Availability
 * @typedef {import("../../APIService").Mission} Mission
 * @typedef {import("../../APIService").MissionRequest} MissionRequest
 */

export class AvailabilityForm extends LitElement {
  sendAvailabilityForm = sendAvailabilityForm; // overridable for testing

  static properties = {
    name: { type: String },
    token: { type: String },
    missions: { type: Array },
    notification: { type: String },
  };

  static styles = styles;

  constructor() {
    super();
    /** @type MissionRequest[] */
    this.missions = [];

    this.name = "there"; // This will make us say "Hi there" until we have a name
    this.token = null;
    this.notification = null;
  }

  onSuccess() {
    window.location.assign("/applications/inquiry_success/");
  }

  onFailure(e) {
    console.error("AvailabilityForm.onFailure", e, e.stack);
    this.notification = e.toString();
  }

  async onSubmit(e) {
    e.preventDefault();
    if (allMissionsKnown(this.missions)) {
      const missionData = this.missions.map(({ mission, availability }) => ({
        mission,
        availability,
      }));
      try {
        await this.sendAvailabilityForm(this.token, missionData);
        this.onSuccess();
      } catch (e) {
        this.onFailure(e);
      }
    } else {
      this.notification = "UNKNOWN mission not caught by form validation";
    }
  }

  render() {
    return html`
      <link rel="stylesheet" href="${window.contentsCss}" />

      <form class="main" @submit="${this.onSubmit}">
        <div class="header">
          <h1>Hi ${this.name}, nice to see you again!</h1>
          <h2>
            We need further information from you in order to plan our missions. Please
            fill out the following form.
          </h2>
        </div>

        ${this.renderMisconfiguredWarningIfMisconfigured()}
        ${this.notification &&
        renderNotification(this.notification, () => {
          this.notification = null;
        })}

        <fieldset>
          <h2>Availability</h2>
          <p>
            We are currently planing missions on the following dates. Which ones are you
            available for?
          </p>
          <small
            >This is not a committment and can be changed later, but it helps us plan
            the year.</small
          >
          ${renderMissionsFormControls(this.missions, ({ mission, value }) => {
            this.missions = this.missions.map((m) => {
              if (m.mission == mission) {
                return { ...m, availability: value };
              } else {
                return m;
              }
            });
          })}
        </fieldset>
        <div class="buttons">
          <input type="submit" value="Submit" class="button" />
        </div>
      </form>
    `;
  }

  renderMisconfiguredWarningIfMisconfigured() {
    if (!this.token) {
      return renderNotification(
        "The component was misconfigured (token property missing). If you're not a developer, " +
          "there is nothing you can do."
      );
    }
  }
}
customElements.define("availability-form", AvailabilityForm);

/**
 * @param {String} notification
 * @param {() => void} onDismiss
 *
 * @returns {import('lit').TemplateResult}
 */
function renderNotification(notification, onDismiss = null) {
  return html`
    <section role="alert">
      ${onDismiss && html`<button aria-label="Dismiss" @click=${onDismiss}>×</button>`}
      Something went wrong, sorry about that.<br />
      <small>${notification}</small>
    </section>
  `;
}

/**
 * @param {MissionRequest[]} missions
 * @param {({mission: UUID, value: AvailabilityOrUnknown }) => void} onChange
 */
function renderMissionsFormControls(missions, onChange) {
  return html`<div class="availabilities">
    ${missions.map((mission) => {
      const uid = mission.mission;
      return html`<div
          class="mission"
        >
          <label for="mission-select-${uid}"
                 >${formatIsoDate(mission.start_date)}<br/>to&nbsp;${formatIsoDate(
        mission.end_date
      )}
          </label>
          <select
            class="small"
            name="${uid}"
            id="mission-select-${uid}"
            required
            @change="${(e) => onChange({ mission: uid, value: e.target.value })}"
            >
            ${renderOptions(mission.availability)}
        </div>`;
    })}
  </div>`;
}

function renderOptions(selected) {
  return html`
    <option
      value=""
      ?selected="${!choices.find(({ value }) => value == selected)}"
      disabled
      hidden
    >
      - -
    </option>
    ${choices.map(
      ({ value, label }) =>
        html`<option value="${value}" ?selected="${selected == value}">
          ${label}
        </option>`
    )}
  `;
}

const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];
function dayString(day) {
  switch (day) {
    case 1:
      return "1st";
    case 2:
      return "2nd";
    case 3:
      return "3rd";
    default:
      return `${day}th`;
  }
}
function formatIsoDate(isodate) {
  const d = new Date(isodate);
  return html`${months[d.getMonth()]}&nbsp;${dayString(
    d.getDate()
  )},&nbsp;${d.getFullYear()}`;
}

/**
 * @param {MissionRequest[]} missions
 * @return {missions is Mission[]}
 */
export function allMissionsKnown(missions) {
  return missions.find((m) => m.availability == "UNKNOWN") === undefined;
}
