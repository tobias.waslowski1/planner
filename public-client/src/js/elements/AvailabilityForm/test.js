import "@babel/polyfill/noConflict";

import { fixture, html, expect } from "@open-wc/testing";
import { AvailabilityForm, allMissionsKnown } from ".";

/**
 * @typedef {import("../../APIService").MissionRequest} MissionRequest
 */

const yes = "yes";
const no = "no";
const maybe = "maybe";

describe("allMissionsKnown", () => {
  const mkM = (a) => ({
    mission: "asdf",
    start_date: "1923-01-21",
    end_date: "1259-01-04",
    availability: a,
  });

  it("is true for missions with YES", () => {
    expect(allMissionsKnown([mkM(yes)])).to.be.true;
  });
  it("is not true if there is one mission with UNKNOWN", () => {
    expect(allMissionsKnown([mkM(yes), mkM("UNKNOWN")])).to.be.false;
  });
});

describe("AvailabilityForm", () => {
  it("follow basic accessibility guidelines", async () => {
    const form = /** @type AvailabilityForm */ await fixture(
      "<availability-form></availability-form>"
    );
    await expect(form).to.be.accessible();
    expect(form).to.be.instanceof(AvailabilityForm);
  });
  it("greets by name", async () => {
    /** @type AvailabilityForm */
    const form = await fixture(
      "<availability-form name='Theodore'></availability-form>"
    );
    expect(form.renderRoot).to.contain.text("Hi Theodore,");
  });
  it("renders missions", async () => {
    /** @type MissionRequest[] */
    const missions = [
      {
        mission: "1",
        start_date: "2021-03-01",
        end_date: "2021-04-01",
        availability: "UNKNOWN",
      },
    ];
    const form = await fixture(
      html`<availability-form .missions=${missions}></availability-form>`
    );
    const label = form.renderRoot.querySelector('label[for="mission-select-1"]');
    expect(label).lightDom.to.equal(
      "March&nbsp;1st,&nbsp;2021<br/>to&nbsp;April&nbsp;1st,&nbsp;2021"
    );
  });
  it("submits the data", async () => {
    /** @type MissionRequest[] */
    const missions = [
      {
        mission: "1",
        start_date: "2021-03-01",
        end_date: "2021-04-01",
        availability: "UNKNOWN",
      },
    ];
    /** @type AvailabilityForm */
    const form = await fixture(
      html`<availability-form token="token" .missions=${missions}></availability-form>`
    );
    const calls = [];
    form.sendAvailabilityForm = async (...args) => {
      calls.push(args);
    };
    let success = null;
    form.onSuccess = () => {
      success = true;
    };

    const select = form.renderRoot.querySelector("select");
    select.value = yes;
    await select.dispatchEvent(new Event("change"));

    /** @type HTMLButtonElement */
    const submitButton = form.renderRoot.querySelector("input[type=submit]");
    await submitButton.click();

    expect(calls).to.deep.equal([["token", [{ mission: "1", availability: yes }]]]);
    expect(success).to.be.true;
  });
  it("doesn't submit UNNKNOWN values", async () => {
    /** @type MissionRequest[] */
    const missions = [
      {
        mission: "1",
        start_date: "2021-03-01",
        end_date: "2021-04-01",
        availability: yes,
      },
    ];
    /** @type AvailabilityForm */
    const form = await fixture(
      html`<availability-form .missions=${missions}></availability-form>`
    );
    let called = false;
    form.sendAvailabilityForm = async () => {
      called = true;
      throw "no";
    };

    const select = form.renderRoot.querySelector("select");
    select.value = "UNKNOWN"; // fails here
    await select.dispatchEvent(new Event("change"));

    /** @type HTMLButtonElement */
    const submitButton = form.renderRoot.querySelector("input[type=submit]");
    await submitButton.click();

    expect(called).to.be.false;
  });
});
