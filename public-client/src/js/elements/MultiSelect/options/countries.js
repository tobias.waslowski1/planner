import countriesList from "countries-list";

export default Object.entries(countriesList.countries).map(([key, value]) => ({
  value: key,
  label: value.name,
}));
