// This should be kept in sync with the CertificatesListFactory in common/src/planner_common/test/factories.py
import certificates_data from "common/data/certificates.json";

const groups = certificates_data.map(({ group, group_label, certificates }) => [
  { value: group, label: group_label, disabled: true },
  ...certificates,
]);

groups[0].push({ value: "other", label: "Other (free text)" });

export default groups.flat();
