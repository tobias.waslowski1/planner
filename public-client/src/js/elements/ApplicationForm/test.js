import "@babel/polyfill/noConflict";
import { fixture, expect, nextFrame } from "@open-wc/testing";
import ApplicationForm from ".";

/**
 * @typedef {import("../DatePicker").DatePicker} DatePicker
 * @typedef {import("../MultiSelect").MultiSelect} MultiSelect
 * @typedef {import("../LanguageMapping").LanguageMapping} LanguageMapping
 * @typedef {import("../TextareaCounter").TextareaCounter} TextareaCounter
 */

const nullApi = {
  sendApplicationForm: async () => {},
  sendAvailabilityForm: async () => {},
};

describe("ApplicationForm", () => {
  it("shows and hides the gender tooltip when the gender select is focused", async () => {
    const form = /** @type ApplicationForm */ (
      await fixture("<application-form></application-form>")
    );
    const input = /** @type {HTMLElement} */ (
      form.renderRoot.querySelector('select[name="gender"]')
    );
    const tooltip = form.renderRoot.querySelector("#gender_tooltip");

    await input.dispatchEvent(new FocusEvent("focus"));
    expect(tooltip).to.have.attribute("aria-hidden", "false");

    await input.dispatchEvent(new FocusEvent("blur"));
    expect(tooltip).to.have.attribute("aria-hidden", "true");
  });

  it("does not show text field by default", async () => {
    const form = /** @type ApplicationForm */ (
      await fixture("<application-form></application-form>")
    );
    const textbox = form.renderRoot.querySelector("input[id=input_other_certificate]");
    expect(textbox).to.have.attribute("aria-hidden", "true");
  });

  it("shows text field when other certificate option selected", async () => {
    const form = /** @type ApplicationForm */ (
      await fixture("<application-form></application-form>")
    );
    const otherOption = /*HTMLElement*/ await form.renderRoot
      .querySelector('multi-select[name="certificates"]')
      .renderRoot.querySelector('.choices__item[data-value="other"]');
    const textbox = form.renderRoot.querySelector("input[id=input_other_certificate]");

    await otherOption.dispatchEvent(new Event("mousedown"));
    expect(textbox).to.have.attribute("aria-hidden", "false");
  });

  it("hides text field when other certificate option not selected", async () => {
    const form = /** @type ApplicationForm */ (
      await fixture("<application-form></application-form>")
    );
    const otherOption = /*HTMLElement*/ await form.renderRoot
      .querySelector('multi-select[name="certificates"]')
      .renderRoot.querySelector('.choices__item[data-value="other"]');
    const textbox = form.renderRoot.querySelector("input[id=input_other_certificate]");

    await otherOption.dispatchEvent(new Event("mousedown"));

    const otherOptionRemoveButton = /*HTMLElement*/ await form.renderRoot
      .querySelector('multi-select[name="certificates"]')
      .renderRoot.querySelector('.choices__item[data-value="other"]')
      .querySelector("button");

    await otherOptionRemoveButton.dispatchEvent(new Event("mousedown"));
    expect(textbox).to.have.attribute("aria-hidden", "true");
  });

  it("sends the data and redirects to success on submit", async () => {
    const form = /** @type ApplicationForm */ (
      await fixture("<application-form></application-form>")
    );

    let calledSend = false;
    const api = {
      sendApplicationForm: async () => {
        calledSend = true;
      },
    };
    // @ts-ignore
    form.apiService = api;

    form.allFieldsAreValid = () => true;

    let calledSuccess = false;
    form.redirectToSuccessPage = () => {
      calledSuccess = true;
    };
    await /** @type {HTMLElement} */ (
      form.renderRoot.querySelector('input[type="submit"]')
    ).click();
    expect(calledSend).to.be.true;
    expect(calledSuccess).to.be.true;
  });

  it("doesnt submit if data is invalid", async () => {
    const form = /** @type ApplicationForm */ (
      await fixture("<application-form></application-form>")
    );

    let calledSend = false;
    const api = {
      ...nullApi,
      sendApplicationForm: async () => {
        calledSend = true;
      },
    };
    form.apiService = api;

    form.allFieldsAreValid = () => false;

    let calledSuccess = false;
    form.redirectToSuccessPage = () => {
      calledSuccess = true;
    };
    /** @type {HTMLElement} */ (
      form.renderRoot.querySelector('input[type="submit"]')
    ).click();
    expect(calledSend).to.be.false;
    expect(calledSuccess).to.be.false;
  });

  it("prevents double submission", async () => {
    // GIVEN a valid form
    const form = /** @type ApplicationForm */ (
      await fixture("<application-form></application-form>")
    );
    form.redirectToSuccessPage = () => {}; // Deactivate for better error messages

    let simulateServerError = null;
    form.apiService = {
      ...nullApi,
      sendApplicationForm: () =>
        new Promise((_, reject) => {
          simulateServerError = () => reject("simulated error");
        }),
    };
    form.allFieldsAreValid = () => true;

    // WHEN submitting it
    const /** @type HTMLInputElement */ button =
        form.renderRoot.querySelector('input[type="submit"]');
    button.click();
    await nextFrame();

    // THEN the submit button is disabled
    expect(button.value).to.equal("Submitting...");
    expect(button.disabled).to.be.true;

    // AND further submits are prevented
    let submittedAnyways = false;
    form.apiService.sendApplicationForm = async () => {
      submittedAnyways = true;
    };
    form.renderRoot.querySelector("form").dispatchEvent(new SubmitEvent("submit"));
    expect(submittedAnyways).to.be.false;

    // WHEN the server responds with an error
    // @ts-ignore
    simulateServerError();
    await nextFrame();

    // THEN the button is re-enabled
    expect(button.value).to.equal("Submit");
    expect(button.disabled).to.be.false;

    // AND submitting the form works again
    let submitted = false;
    form.apiService.sendApplicationForm = async () => {
      submitted = true;
    };
    form.renderRoot.querySelector("form").dispatchEvent(new SubmitEvent("submit"));
    expect(submitted).to.be.true;
  });

  it("shows an error message on error", async () => {
    const form = /** @type ApplicationForm */ (
      await fixture("<application-form></application-form>")
    );

    const api = {
      sendApplicationForm: async () => {
        throw new Error("example error message");
      },
    };
    // @ts-ignore
    form.apiService = api;

    form.allFieldsAreValid = () => true;

    /** @type {HTMLElement} */ (
      form.renderRoot.querySelector('input[type="submit"]')
    ).click();

    await nextFrame();

    const alert = form.renderRoot.querySelector('[role="alert"]');
    expect(alert).to.be.ok;
    expect(alert.textContent).to.contain("example error message");
  });

  it("validates fields", async () => {
    const fields = [
      "first_name",
      "last_name",
      "phone_number",
      "email",
      "date_of_birth",
      "gender",
      "nationalities",
      "spoken_languages",
      "positions",
      // certificates isn't included because it's optional
      "motivation",
      "qualification",
      "SKIP_NOTHING_EXPECT_VALID",
    ];
    await Promise.all(
      fields.map(async (invalid_field) => {
        const form = /** @type ApplicationForm */ (
          await fixture("<application-form></application-form>")
        );

        if (invalid_field != "first_name") {
          /** @type {HTMLInputElement} */ (
            form.renderRoot.querySelector('input[name="first_name"]')
          ).value = "first name";
        }

        if (invalid_field != "last_name") {
          /** @type {HTMLInputElement} */ (
            form.renderRoot.querySelector('input[name="last_name"]')
          ).value = "last name";
        }

        if (invalid_field != "phone_number") {
          /** @type {HTMLInputElement} */ (
            form.renderRoot.querySelector('input[name="phone_number"]')
          ).value = "+49 12345 12341234";
        }

        if (invalid_field != "email") {
          /** @type {HTMLInputElement} */ (
            form.renderRoot.querySelector('input[name="email"]')
          ).value = "test@example.com";
        }

        /** @type {DatePicker} */ (
          form.renderRoot.querySelector('date-picker[name="date_of_birth"]')
        ).reportValidity = () => invalid_field != "date_of_birth";

        if (invalid_field != "gender") {
          /** @type {HTMLSelectElement} */ (
            form.renderRoot.querySelector('select[name="gender"]')
          ).value = "prefer_not_say";
        }

        /** @type {MultiSelect} */ (
          form.renderRoot.querySelector('multi-select[name="nationalities"]')
        ).reportValidity = () => invalid_field != "nationalities";

        /** @type {LanguageMapping} */ (
          form.renderRoot.querySelector('language-mapping[name="spoken_languages"]')
        ).reportValidity = () => invalid_field != "spoken_languages";

        /** @type {MultiSelect} */ (
          form.renderRoot.querySelector('multi-select[name="positions"]')
        ).reportValidity = () => invalid_field != "positions";

        /** @type {TextareaCounter} */ (
          form.renderRoot.querySelector('textarea-counter[name="motivation"]')
        ).reportValidity = () => invalid_field != "motivation";

        /** @type {TextareaCounter} */ (
          form.renderRoot.querySelector('textarea-counter[name="qualification"]')
        ).reportValidity = () => invalid_field != "qualification";

        if (invalid_field == "SKIP_NOTHING_EXPECT_VALID") {
          expect(
            form.allFieldsAreValid(),
            "expected allFieldsAreValid() to be true when everything is filled in"
          ).to.be.true;
        } else {
          expect(
            form.allFieldsAreValid(),
            `expected allFieldsAreValid() to be false when "${invalid_field}" is missing`
          ).to.be.false;
        }
      })
    );
  });

  describe("phone number validation", async () => {
    const mkForm = async () =>
      /** @type ApplicationForm */ (
        await fixture("<application-form></application-form>")
      );

    const validNumber = "+44 113 496 0000";
    const invalidNumber = "+121255522";

    /** @returns {HTMLInputElement} */
    function getPhoneNumberInput(form) {
      return form.renderRoot.querySelector("#input_phone_number");
    }

    it("allows valid numbers", async () => {
      const form = await mkForm();
      const input = getPhoneNumberInput(form);
      input.value = validNumber;
      expect(form._fieldIsValid(input)).to.be.true;
    });

    it("denies invalid numbers", async () => {
      const form = await mkForm();
      const input = getPhoneNumberInput(form);
      input.value = invalidNumber;
      expect(form._fieldIsValid(input)).to.be.false;
      expect(input.validationMessage).to.equal(
        "This doesn't seem to be a valid phone number"
      );
    });

    it("allows correcting invalid numbers", async () => {
      const form = await mkForm();
      const input = getPhoneNumberInput(form);
      input.value = invalidNumber;
      form._fieldIsValid(input);
      input.value = validNumber;
      expect(form._fieldIsValid(input)).to.be.true;
    });
  });
});
