import "@babel/polyfill/noConflict";

import { expect, fixture, html, nextFrame, triggerFocusFor } from "@open-wc/testing";

import "/elements/TextareaCounter/index.js";

describe("TextareaCounter", () => {
  it("contains a required textarea when required is set", async () => {
    const el = await fixture(`<textarea-counter required></textarea-counter>`);
    expect(el.shadowRoot.querySelector("textarea")).to.have.attribute("required");
  });
  it("the textarea is focused when the component receives focus", async () => {
    const el = await fixture(`<textarea-counter></textarea-counter>`);
    const textarea = el.shadowRoot.querySelector("textarea");
    expect(textarea).to.be.accessible;

    await triggerFocusFor(el);

    await nextFrame();
    expect(el.shadowRoot.activeElement).to.equal(textarea);
  });

  it("passes the maxlength attribute to the textarea", async () => {
    const el = await fixture(`<textarea-counter maxLength="666"></textarea-counter>`);
    const textarea = el.shadowRoot.querySelector("textarea");
    expect(textarea.maxLength).to.equal(666);
  });

  it("counts input length", async () => {
    const el = await fixture(`<textarea-counter maxLength="666"></textarea-counter>`);
    const textarea = el.shadowRoot.querySelector("textarea");
    const counter = el.shadowRoot.querySelector(".textarea-counter");

    textarea.value = "Hi!";
    textarea.dispatchEvent(new Event("input"));

    await nextFrame();
    expect(counter.innerText).to.equal("3/666");
  });

  it("displays buttons", async () => {
    const el = await fixture(`<textarea-counter></textarea-counter>`);
    const buttons = el.shadowRoot.querySelector(".buttonlist");

    el.buttons = [{ label: "test", url: "" }];

    await nextFrame();

    expect(buttons.innerText).to.equal("test");
    expect(buttons.children.length).to.equal(1);
  });
});
