import json
import urllib.parse
from typing import Dict, List, Optional
from uuid import UUID

import requests
from django.conf import settings
from planner_common import dtos

from seawatch_planner.public_backend import auth_headers
from volunteers.models import Volunteer


def request_availabilities(
    volunteer: Volunteer, availability_dtos: List[dtos.Availability]
) -> None:
    data = dtos.AvailabilityInquiryData(
        name=volunteer.first_name,
        email=volunteer.email,
        missions=availability_dtos,
    )
    response = requests.post(
        settings.PLANNER_PUBLIC_BACKEND_URL + "/applications/request_availabilities/",
        json.dumps(data.to_dict()),
        headers=auth_headers(),
    )
    response.raise_for_status()


def fetch_inquiry_data_from_public_backend() -> Optional[Dict]:
    response = requests.get(
        settings.PLANNER_PUBLIC_BACKEND_URL + "/applications/inquiries/",
        headers=auth_headers(),
    )
    response.raise_for_status()
    if response.status_code != 204:
        return response.json()


def import_inquiries(
    fetch_data=fetch_inquiry_data_from_public_backend,
) -> List[dtos.Inquiry]:
    # TODO Handle poison messages
    data = (fetch_data() or {}).get("inquiries", [])
    return [dtos.Inquiry.from_dict(d) for d in data]


def acknowledge_inquiry_import(inquiry_id: UUID) -> None:
    quoted_inquiry_id = urllib.parse.quote(str(inquiry_id))
    response = requests.delete(
        f"{settings.PLANNER_PUBLIC_BACKEND_URL}/applications/inquiry/{quoted_inquiry_id}/",
        headers=auth_headers(),
    )
    response.raise_for_status()
