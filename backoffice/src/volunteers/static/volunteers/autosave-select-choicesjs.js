class AutosaveChoicesJS extends HTMLSelectElement {
  constructor() {
    super();
    this._choices = new Choices(this, {
      removeItemButton: true,
      duplicateItemsAllowed: false,
      shouldSort: false,
    });
    this.dispatchEvent(new Event("initialized"));

    this.addEventListener("htmx:responseError", (e) => {
      this.closest(".input-container").classList.add("autosave-error");
    });
  }
}
customElements.define("autosave-choices-js", AutosaveChoicesJS, { extends: "select" });
