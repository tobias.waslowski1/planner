function indicateHTMXServerErrors(element) {
  element.addEventListener("htmx:responseError", (event) => {
    // Hide the checkmark if it is shown
    const savedIndicator = element
      .closest(".input-container")
      .querySelector(".autosave-indicator-saved");
    if (savedIndicator) {
      savedIndicator.style.opacity = 0;
    }

    // Add the error class
    element.closest(".input-container").classList.add("autosave-error");
  });
}

////////////////////
// Autosave Elements

class AutosaveSelect extends HTMLSelectElement {
  constructor() {
    super();
    indicateHTMXServerErrors(this);
  }
}
customElements.define("autosave-select", AutosaveSelect, { extends: "select" });

class AutosaveInput extends HTMLInputElement {
  constructor() {
    super();
    indicateHTMXServerErrors(this);

    // Reset on escape key
    this.originalValue = this.value;
    this.addEventListener("keydown", (e) => {
      if (e.key == "Escape") this.value = this.originalValue;
    });

    // Allow re-send with enter on error
    this.addEventListener("htmx:responseError", (_event) => {
      if (!this.hasResendHandler) {
        this.addEventListener("keydown", (e) => {
          if (e.key == "Enter") {
            htmx.trigger(this, "change");
          }
        });
      }
      this.hasResendHandler = true;
    });
  }
}
customElements.define("autosave-input", AutosaveInput, { extends: "input" });
