import datetime
from typing import TYPE_CHECKING

from django.contrib.contenttypes.fields import GenericRelation
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext as _
from phonenumber_field.modelfields import PhoneNumberField
from planner_common import data
from planner_common.models import Gender

from applications.models import Application, PositionDecision, nationalities_separator
from history.models import HistoryItem
from seawatch_registration.models import Position, ShortTextField

if TYPE_CHECKING:
    from django.db.models.manager import RelatedManager

    from missions.models import Availability

valid_language_codes = {l["code"] for l in data.languages}


def validate_languages_dict(value):
    if type(value) != dict:
        raise ValidationError("languages must be a dict")
    for code, level in value.items():
        if code not in valid_language_codes:
            raise ValidationError(
                _("%(code)r is not a valid language code"), params={"code": code}
            )
        if level not in [0, 1, 2, 3]:
            raise ValidationError(
                _("%(level)r is not a valid language level"), params={"level": level}
            )


class VolunteerManager(models.Manager):
    def create_from_application(self, application: Application):
        obj = self.create(
            first_name=application.first_name,
            last_name=application.last_name,
            phone_number=application.phone_number,
            email=application.email,
            date_of_birth=application.date_of_birth,
            gender=application.gender,
            certificates=application.certificates,
            languages={l.code: l.points for l in application.languages.all()},
            nationalities=application.nationalities.split(nationalities_separator),
            application=application,
        )
        obj.positions.set(
            pd.position
            for pd in application.position_decisions.filter(
                decision=PositionDecision.Decision.ACCEPT
            )
        )

        return obj


class Volunteer(models.Model):
    objects: VolunteerManager = VolunteerManager()

    history = GenericRelation(HistoryItem)  # type: ignore

    ### fields

    id = models.AutoField(primary_key=True)
    first_name = ShortTextField()
    last_name = ShortTextField()
    phone_number = PhoneNumberField()
    # TODO: Propper Email field
    email = ShortTextField()

    date_of_birth = models.DateField()
    gender = ShortTextField(choices=Gender.choices)

    positions = models.ManyToManyField(Position)
    certificates = models.JSONField(blank=True)  # ["drivers-license", "more-things"]
    languages = models.JSONField(
        validators=[validate_languages_dict]
    )  # {"deu": 3, "eng": 2}
    nationalities = models.JSONField()  # ["jp", "de"]

    application = models.ForeignKey(Application, on_delete=models.PROTECT)

    if TYPE_CHECKING:
        availability_set = RelatedManager[Availability]()

    @property
    def name(self):
        return f"{self.first_name} {self.last_name}".strip()

    def __str__(self):
        return f"<Volunteer: {self.name}>"

    def to_history_dict(self):
        return {
            "first_name": self.first_name,
            "last_name": self.last_name,
            "phone_number": self.phone_number,
            "email": self.email,
            "date_of_birth": self.date_of_birth.isoformat(),
            "gender": self.gender,
            "positions": ",".join(p.key for p in self.positions.all()),
            "certificates": self.certificates,
            "languages": self.languages,
            "nationalities": [n.lower() for n in self.nationalities],
        }


class MalformedInquiry(models.Model):
    class Meta:
        verbose_name_plural = "malformed inquiries"

    id = models.AutoField(primary_key=True)
    submitted_data = models.JSONField()
    uid = ShortTextField()
    created = models.DateTimeField(editable=False)

    def save(self, *args, **kwargs):
        """on save, create timestamp"""
        if not self.id and self.created is None:
            self.created = datetime.datetime.now(datetime.timezone.utc)
        return super().save(*args, **kwargs)
