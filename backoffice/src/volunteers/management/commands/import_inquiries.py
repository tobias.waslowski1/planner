import logging
from typing import Callable, List, NoReturn
from uuid import UUID

from django.core.management.base import BaseCommand
from django.db import transaction
from planner_common.dtos import Inquiry

from missions.models import Availability
from volunteers.service import acknowledge_inquiry_import, import_inquiries

logger = logging.getLogger(__name__)


def assert_never(x: NoReturn) -> NoReturn:
    assert False, f"Unhandled type: {type(x).__name__}"


class Command(BaseCommand):
    help = "Import inquiries from public backend"

    def __init__(
        self,
        import_inquiries: Callable[[], List[Inquiry]] = import_inquiries,
        acknowledge_import: Callable[[UUID], None] = acknowledge_inquiry_import,
        *args,
        **kwargs,
    ):
        self._import_inquiries = import_inquiries
        self._acknowledge_import = acknowledge_import
        super().__init__(*args, **kwargs)

    def handle(self, *args, **options):
        dtos = self._import_inquiries()
        for dto in dtos:
            # Proper tying for atomic is unreleased as of now:
            #   https://github.com/typeddjango/django-stubs/pull/586
            with transaction.atomic(durable=True):  # type: ignore
                type = dto.type
                if type == Inquiry.Type.Availability:
                    for mission in dto.data.missions:
                        try:
                            availability = Availability.objects.get(uid=mission.mission)
                            availability.availability = mission.availability.value
                            availability.save()
                        except Availability.DoesNotExist:
                            logger.critical(
                                "received unknown availability" + str(mission)
                            )
                else:
                    assert_never(type)
            try:
                self._acknowledge_import(dto.uid)
            except Exception:
                logger.warning(
                    f'Acknowledging inquiry "{dto.uid}" failed', exc_info=True
                )
