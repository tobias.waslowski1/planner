from typing import List

from django import template

register = template.Library()


@register.filter
def mapattr(value, attribute):
    return [getattr(v, attribute) for v in value]
