import json
from typing import Any

import django_countries
from django import forms
from django.forms.fields import MultipleChoiceField
from django.utils.translation import gettext as _
from planner_common import data

from applications.forms import CertificatesField, ChoicesJsMedia
from volunteers.models import Volunteer


class LanguagesWidget(forms.TextInput):
    template_name = "volunteers/forms/widgets/languages.html"
    Media = ChoicesJsMedia

    def get_context(self, name, value, attrs):
        ctx = super().get_context(name, value, attrs)
        if isinstance(value, str):
            value = json.loads(value)
        ctx["available_languages"] = [
            (l["code"], l["name"], l["code"] in value)
            for l in data.languages
            if l["backoffice"]
        ]
        return ctx

    def format_value(self, value):
        if isinstance(value, str):
            value = json.loads(value)
        return value


_certificates_choices = [
    (g["group_label"], [(e["value"], e["label"]) for e in g["certificates"]])
    for g in data.certificates
]
_certificates_choices[0][1].append(("other", "Other (free text)"))


class AutosavingCertificatesWidget(forms.SelectMultiple):
    template_name = "volunteers/forms/widgets/autosaving-certificates.html"

    def get_context(self, name, value, attrs):
        other = None
        for idx, cert in enumerate(value):
            if cert.startswith("other: "):
                other = cert
                value[idx] = "other"
        ctx = super().get_context(name, value, attrs)
        if other:
            ctx["other"] = other.removeprefix("other: ")
        return ctx


class CertificatesField(MultipleChoiceField):
    widget = AutosavingCertificatesWidget

    def __init__(self, *, choices=_certificates_choices, **kwargs):
        # ModelForm passes those for JSONFields
        kwargs.pop("encoder", None)
        kwargs.pop("decoder", None)

        super().__init__(choices=choices, **kwargs)

    def valid_value(self, value: Any) -> bool:
        if str(value).startswith("other: "):
            return True
        else:
            return super().valid_value(value)


class VolunteerForm(forms.ModelForm):
    class Meta:
        model = Volunteer
        fields = [
            "first_name",
            "last_name",
            "phone_number",
            "email",
            "date_of_birth",
            "gender",
            "nationalities",
            "languages",
            "positions",
            "certificates",
        ]
        field_classes = {
            "certificates": CertificatesField,
            # ModelForm passes de/encoder because the model has a json field
            "nationalities": lambda *a, encoder=None, decoder=None, **k: MultipleChoiceField(
                *a, **k
            ),
        }
        widgets = {
            "languages": LanguagesWidget,
            "nationalities": forms.widgets.SelectMultiple,
        }
        labels = {
            "first_name": _("First Name"),
            "last_name": _("Last Name"),
            "phone_number": _("Phone Number"),
            "email": _("Email Address"),
            "date_of_birth": _("Date of Birth"),
            "gender": _("Gender"),
            "nationalities": _("Nationalities"),
            "languages": _("Spoken Languages"),
            "positions": _("Positions"),
            "certificates": _("Certificates"),
        }

    class Media:
        js = ["volunteers/autosave-input.js", "volunteers/autosave-select-choicesjs.js"]

    _custom_elements_by_widget = {
        forms.widgets.TextInput: "autosave-input",
        forms.widgets.DateInput: "autosave-input",
        forms.widgets.Select: "autosave-select",
        forms.widgets.SelectMultiple: "autosave-choices-js",
        LanguagesWidget: None,
        AutosavingCertificatesWidget: None,
    }

    def __init__(self, **kwargs):
        instance = kwargs.get("instance")
        if instance:
            instance.nationalities = [n.upper() for n in instance.nationalities]

        super().__init__(**kwargs)
        for field_name, field in self.fields.items():
            if field_name not in ["certificates", "languages"]:
                field.widget.attrs = {
                    **(field.widget.attrs or {}),
                    "hx-post": "autosave/",
                    "hx-indicator": "closest .input-container",
                    "hx-target": "closest .field",
                    "hx-swap": "outerHTML",
                }
                if isinstance(field.widget, forms.widgets.SelectMultiple):
                    field.widget.attrs["hx-ext"] = "json-enc"

            # We don't use get() here because we want to know when we forgot to add one.
            custom_element = self._custom_elements_by_widget[type(field.widget)]
            if custom_element:
                field.widget.attrs["is"] = custom_element

        nationalities_field = self.fields.get("nationalities")
        if nationalities_field:
            nationalities_field.choices = django_countries.countries
