import json
from datetime import date
from http import HTTPStatus
from typing import Callable, List
from urllib.parse import urlencode

from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db import transaction
from django.db.models import Q
from django.forms.models import modelform_factory
from django.http.request import HttpRequest
from django.http.response import (
    HttpResponse,
    HttpResponseBadRequest,
    HttpResponseRedirect,
)
from django.shortcuts import get_object_or_404, render
from django.template import Template
from django.template.context import Context
from django.template.loader import render_to_string
from django.urls import reverse
from django.views.generic import DetailView, ListView
from django.views.generic.edit import UpdateView
from planner_common import dtos

from history.models import HistoryItem
from missions.models import Availability, Mission
from seawatch_registration.models import Position
from volunteers.forms import VolunteerForm

from . import service
from .models import Volunteer


def get_position_or_none(key):
    try:
        return Position.objects.get(key=key)
    except Position.DoesNotExist:
        return None


class VolunteerListView(PermissionRequiredMixin, ListView):
    model = Volunteer

    permission_required = "volunteers.view_volunteer"

    def get_queryset(self):
        qs = super().get_queryset()

        position = self.request.GET.get("position")
        if position:
            qs = qs.filter(positions__key=position)

        self.search = self.request.GET.get("search")
        if self.search:
            search_without_spaces = self.search.split(" ")
            for word in search_without_spaces:
                qs = qs.filter(
                    Q(first_name__icontains=word) | Q(last_name__icontains=word)
                )
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["missions"] = [str(m) for m in Mission.objects.all()]
        context["positions"] = [
            {"object": pos, "link": self._build_link(pos)}
            for pos in Position.objects.all().order_by("name")
        ]
        context["search"] = self.search or ""
        context["selected_position"] = get_position_or_none(
            self.request.GET.get("position")
        )
        context["all_positions_link"] = (
            f'?{urlencode({"search": self.search})}' if self.search else "?"
        )
        return context

    def _build_link(self, pos):
        query_search = {"position": pos.key}
        if self.search:
            query_search["search"] = self.search
        return f"?{urlencode(query_search)}"


class VolunteerProfileView(PermissionRequiredMixin, DetailView):
    permission_required = "volunteers.view_volunteer"
    model = Volunteer


@permission_required("volunteers.view_volunteer", raise_exception=True)
def request_availabilities_view(
    request,
    volunteer_id: int,
    request_availabilities: Callable[
        [Volunteer, List[dtos.Availability]], None
    ] = service.request_availabilities,
):
    volunteer = get_object_or_404(Volunteer, pk=volunteer_id)
    missions = Mission.objects.filter(start_date__gte=date.today())
    if request.method == "POST":
        _send_availability_request(volunteer, missions, request_availabilities)
        messages.success(
            request,
            f"Sent availabilities request to {volunteer.first_name} at {volunteer.email}",
        )
        return HttpResponseRedirect(reverse("volunteer_list"))
    else:
        return _render_availability_request_template(request, volunteer, missions)


def _send_availability_request(volunteer, missions, request_availabilities):
    availability_dtos = []
    for mission in missions:
        avail, _ = Availability.objects.get_or_create(
            mission=mission,
            volunteer=volunteer,
            mission_version=mission.version,
        )
        availability_dtos.append(avail.to_dto())
    request_availabilities(volunteer, availability_dtos)


def _render_availability_request_template(
    request: HttpRequest, volunteer: Volunteer, missions
):
    def fetch_last_response(mission: Mission):
        a = (
            volunteer.availability_set.filter(mission=mission)
            .exclude(availability=Availability.UNKNOWN)
            .order_by("-mission_version")
            .first()
        )
        if a is None:
            return None
        return {"availability": a, "outdated": a.mission_version != mission.version}

    entries = [
        {"mission": mission, "last_response": fetch_last_response(mission)}
        for mission in missions.order_by("start_date")
    ]

    return render(
        request,
        "volunteers/volunteer_availabilities.html",
        {
            "volunteer": volunteer,
            "entries": entries,
        },
    )


class VolunteerUpdateView(UpdateView):
    model = Volunteer
    form_class = VolunteerForm


def volunteer_update_autosave(request: HttpRequest, pk: int):
    if request.content_type == "application/json":
        data = json.load(request)
    else:
        data = request.POST

    # Retrieve data from request
    try:
        [field] = data.keys()
    except ValueError:
        # too many values
        return HttpResponseBadRequest()
    if field not in VolunteerForm.Meta.fields:
        return HttpResponseBadRequest()

    # Save the data
    with transaction.atomic():
        volunteer = get_object_or_404(Volunteer, pk=pk)
        old_data = volunteer.to_history_dict()
        Form = modelform_factory(Volunteer, form=VolunteerForm, fields=[field])
        form = Form(data=data, instance=volunteer)

        if form.is_valid():
            form.save()
            HistoryItem.objects.create_from_dicts(
                volunteer, old=old_data, new=volunteer.to_history_dict()
            )

    # Render the response
    if "HX-Request" in request.headers:
        # TODO: Render form errors when needed
        ctx = {"field": form[field], "autosave_success": form.is_valid()}
        if field in ["date_of_birth", "gender"]:
            ctx["cls"] = "narrow"
        content = render_to_string(
            "volunteers/forms/autosave_form_field.html", ctx, request=request
        )

        if field in ["first_name", "last_name"]:
            content = "\n".join(
                [
                    content,
                    Template(
                        "<span id='header-volunteer-name' hx-swap-oob='true'>{{ volunteer.name }}</span>"
                    ).render(Context({"volunteer": volunteer})),
                ]
            )
        return HttpResponse(content=content)
    else:
        return (
            HttpResponse(status=HTTPStatus.NO_CONTENT)
            if form.is_valid()
            else HttpResponseBadRequest(content=json.dumps({"errors": form.errors}))
        )
