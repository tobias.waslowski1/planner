from django.urls import path
from planner_common import features

from . import views


def notImplemented(request):
    raise NotImplementedError()


urlpatterns = [
    path("", views.VolunteerListView.as_view(), name="volunteer_list"),
    path(
        "<int:pk>/",
        (
            views.VolunteerUpdateView
            if features.edit_volunteers
            else views.VolunteerProfileView
        ).as_view(),
        name="volunteer_detail",
    ),
    path("<int:pk>/autosave/", views.volunteer_update_autosave),
    path("<int:pk>/archive/", notImplemented, name="volunteer_archive"),
    path(
        "request_availabilities/<int:volunteer_id>/",
        views.request_availabilities_view,
        name="volunteer_request_availabilities",
    ),
]
