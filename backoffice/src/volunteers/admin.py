from django.contrib import admin

from . import models


@admin.register(models.Volunteer)
class VolunteerAdmin(admin.ModelAdmin):
    list_display = ["name"]
    exclude = ["application"]
