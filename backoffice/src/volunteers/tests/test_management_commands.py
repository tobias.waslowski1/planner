from unittest.mock import Mock
from uuid import UUID

from planner_common import dtos
from planner_common.test.factories import (
    AvailabilityFactory as CommonAvailabilityFactory,
)
from planner_common.test.factories import AvailabilityInquiryDataFactory, InquiryFactory
from pytest import mark

from applications.models import MalformedApplication
from applications.service import RemoteApplications
from missions.models import Availability
from missions.tests.factories import AvailabilityFactory
from volunteers.models import MalformedInquiry

from ..management.commands.import_inquiries import Command as ImportInquiriesCommand

# @mark.django_db
# def test_import_inquiry_creates_malformed_inquiry():
#     # TODO figure out if this still makes sense
#     import_inquiries_mock = Mock()
#     inq: dtos.Inquiry = InquiryFactory(type="ignored")  # type: ignore
#     import_inquiries_mock.return_value = [inq]
#
#     ImportInquiriesCommand(import_inquiries=import_inquiries_mock).handle()
#
#     assert MalformedInquiry.objects.all().count() == 1
#     malformed = MalformedInquiry.objects.last()
#     assert malformed
#     assert malformed.submitted_data == inq.to_dict()


@mark.django_db
def test_import_inquiry_creates_availability_from_availability_dtos():
    availability = AvailabilityFactory(availability="unknown")
    import_inquiries_mock = Mock()
    inq = InquiryFactory(
        type=dtos.Inquiry.Type.Availability,
        data__missions=[
            CommonAvailabilityFactory(
                mission=availability.uid, availability=dtos.Availability.Answer.Yes
            )
        ],
    )
    import_inquiries_mock.return_value = [inq]

    ImportInquiriesCommand(import_inquiries=import_inquiries_mock).handle()

    assert (
        MalformedInquiry.objects.all().count() == 0
    ), "availability inquiry couldnt be parsed!"
    assert Availability.objects.all().count() == 1
    availability_in_db = Availability.objects.get(uid=availability.uid)
    assert availability_in_db.availability == "yes"


@mark.django_db
def test_import_inquiry_creates_availability_from_availability_dtos_2():
    # This test had the same name as the one above, and I'm apparently too tired to
    # understand what they're supposed to do. So I added _2 to the name, because
    # otherwise the other one won't run *shrug*
    availability = AvailabilityFactory(availability="unknown")
    availability2 = AvailabilityFactory(
        volunteer=availability.volunteer, availability="unknown"
    )
    import_inquiries_mock = Mock()
    inquiry_data = AvailabilityInquiryDataFactory(
        missions=[
            CommonAvailabilityFactory(
                mission=availability.uid, availability=dtos.Availability.Answer.Yes
            ),
            CommonAvailabilityFactory(
                mission=availability2.uid, availability=dtos.Availability.Answer.No
            ),
        ]
    )
    inq = InquiryFactory(
        type=dtos.Inquiry.Type.Availability,
        data=inquiry_data,
    )
    import_inquiries_mock.return_value = [inq]
    ImportInquiriesCommand(import_inquiries=import_inquiries_mock).handle()
    assert Availability.objects.all().count() == 2


@mark.django_db
def test_import_inquiry_acks_inquiry():
    availability = AvailabilityFactory(availability="unknown")
    id = UUID("f4d3608c-1844-4b1f-9a83-477c778a1a28")
    inq = InquiryFactory(
        uid=id,
        data=AvailabilityInquiryDataFactory(
            missions=[
                CommonAvailabilityFactory(
                    mission=UUID(availability.uid),
                    availability=dtos.Availability.Answer.Yes,
                )
            ]
        ),
    )
    ack_mock = Mock()
    import_mock = Mock(return_value=[inq])
    ImportInquiriesCommand(
        import_inquiries=import_mock, acknowledge_import=ack_mock
    ).handle()
    ack_mock.assert_called_with(id)


@mark.django_db
def test_exception_during_acknowledge_saves_malformed_application_anyways():
    # The appl
    def raise_exception():
        raise Exception("simulated network error")

    availability = AvailabilityFactory(availability=Availability.UNKNOWN)

    try:
        ImportInquiriesCommand(
            import_inquiries=lambda: [
                InquiryFactory(
                    type=dtos.Inquiry.Type.Availability,
                    data__missions=[
                        CommonAvailabilityFactory(
                            mission=availability.uid,
                            availability=dtos.Availability.Answer.Yes,
                        )
                    ],
                )
            ],
            acknowledge_import=lambda _: raise_exception(),
        ).handle()
    except:
        pass

    expected = Availability.YES
    actual = Availability.objects.get(uid=availability.uid).availability
    assert (
        expected.lower() == actual.lower()
    )  # Case insensitive until https://gitlab.com/sea-watch.org/planner/-/issues/169
