from django.http.response import HttpResponse
from django.template.response import TemplateResponse
from django.test import Client, RequestFactory
from django.urls import reverse
from pytest import mark
from pytest_django.asserts import assertContains, assertNotContains

from missions.tests.factories import MissionFactory
from seawatch_planner.tests.asserts import assert_requires_permissions
from seawatch_registration.tests.factories import UserFactory

from ...views import VolunteerListView
from ..factories import VolunteerFactory


@mark.django_db
def test_is_reachable(client: Client):
    client.force_login(UserFactory.create(permission="volunteers.view_volunteer"))
    response = client.get(reverse("volunteer_list"))
    assert response.status_code == 200


@mark.django_db
def test_has_volunteers(rf: RequestFactory):
    VolunteerFactory(name="Harry Potter")
    request = rf.get("volunteer_list")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")

    response = VolunteerListView.as_view()(request)
    assert isinstance(response, TemplateResponse)
    assert "Harry Potter" in response.rendered_content


@mark.django_db
def test_requires_permission(rf: RequestFactory):
    assert_requires_permissions(rf, VolunteerListView, ["volunteers.view_volunteer"])


@mark.django_db
def test_shows_missions(rf: RequestFactory):
    # Given there is one mission
    mission = MissionFactory()

    # When I navigate to the volunteer list
    request = rf.get("volunteer_list")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    response = VolunteerListView.as_view()(request)

    # Then I expect the mission to be shown on the page
    assert isinstance(response, TemplateResponse)
    assert str(mission) in response.rendered_content


@mark.django_db
def test_filters_by_position(rf: RequestFactory):
    VolunteerFactory(name="Mary Poppins", positions__keys=["cook"])
    VolunteerFactory(name="Captain Hook", positions__keys=["master"])

    request = rf.get("", {"position": "cook"})
    request.user = UserFactory.create(view_permissions=VolunteerListView)
    response = VolunteerListView.as_view()(request)
    assert isinstance(response, HttpResponse)

    assertContains(response, "Mary Poppins")
    assertNotContains(response, "Captain Hook")
