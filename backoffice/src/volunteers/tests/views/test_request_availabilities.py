from datetime import date, timedelta
from typing import Callable
from unittest.mock import Mock

from bs4 import BeautifulSoup
from bs4.element import Tag
from django.template.response import TemplateResponse
from django.test import Client, RequestFactory
from django.urls import reverse
from freezegun import freeze_time
from planner_common import dtos
from pytest import mark

from missions.models import Availability, Mission
from missions.tests.factories import AvailabilityFactory, MissionFactory
from seawatch_planner.tests.asserts import assert_requires_permissions
from seawatch_planner.tests.messages import MessagesRequestFactory
from seawatch_registration.tests.factories import UserFactory
from volunteers.models import Volunteer

from ...views import VolunteerListView, request_availabilities_view
from ..factories import VolunteerFactory


def noop(*args, **kwargs):
    return None


@mark.django_db
def test_volunteer_list_items_show_button_to_request_availability(
    mrf: MessagesRequestFactory,
):
    # Given there is one volunteer and one mission
    volunteer: Volunteer = VolunteerFactory.create(name="Mail Gebson")
    MissionFactory()

    # When I navigate to the volunteer list
    request = mrf.get("volunteer_list")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    response = VolunteerListView.as_view()(request)
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    # I expect to see a button with which i can request availabilities from the vol
    button = html.find(
        "a",
        href=reverse(
            "volunteer_request_availabilities", kwargs={"volunteer_id": volunteer.pk}
        ),
    )
    assert button is not None, "Could not find button to request availabilities"
    assert button.text == "Request Availabilities"


@mark.django_db
def test_request_availabilities_shows_success_message(mrf: MessagesRequestFactory):
    vol: Volunteer = VolunteerFactory.create(first_name="asdf", email="a@b.de")

    request = mrf.post("unused param", {"confirm": "Confirm"})
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    request_availabilities_view(request, vol.pk, request_availabilities=Mock())

    assert request._messages.messages == [
        (25, "Sent availabilities request to asdf at a@b.de", "")
    ]


@mark.django_db
def test_request_availabilities_includes_volunteer_availabilities(
    mrf: MessagesRequestFactory,
):
    avail: Availability = AvailabilityFactory.create(availability="yes")
    yes = dtos.Availability.Answer("yes")
    name = avail.volunteer.name
    email = avail.volunteer.email
    dto = avail.to_dto()
    assert dto.availability == yes
    request = mrf.post("unused param", {"confirm": "Confirm"})
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    request_availabilities_mock = Mock()
    volpk = avail.volunteer.pk
    request_availabilities_view(
        request, volpk, request_availabilities=request_availabilities_mock
    )
    request_availabilities_mock.assert_called_with(avail.volunteer, [dto])


@mark.django_db
def test_request_availabilities_calls_request_availabilities(
    mrf: MessagesRequestFactory,
):
    # Given there is one volunteer and a mission
    volunteer: Volunteer = VolunteerFactory.create(
        first_name="Emailuel", email="cant@reine-vernunft.de"
    )
    mission: Mission = MissionFactory.create()
    # When I call the vavailability request function
    request = mrf.post("unused param", {"confirm": "Confirm"})
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    request_availabilities_mock = Mock()
    request_availabilities_view(
        request, volunteer.pk, request_availabilities=request_availabilities_mock
    )

    # Then I expect that request availability was called
    assert request_availabilities_mock.call_count == 1
    # And I expect that a new availability object was created
    avail = Availability.objects.get(mission=mission, volunteer_id=volunteer)
    # And i expect the mission version to be correct
    assert avail.mission_version == mission.version
    # And i expect that the availability field is set to UNKNOWN
    assert avail.availability == Availability.UNKNOWN
    # And i expect that request availabilites was called with the correct object
    dto = avail.to_dto()
    request_availabilities_mock.assert_called_with(volunteer, [dto])


@mark.django_db
def test_page_produces_404_when_no_valid_volunteer_id_is_passed(client: Client):
    user = UserFactory.create(permission="volunteers.view_volunteer")
    client.force_login(user)
    response = client.get(
        reverse("volunteer_request_availabilities", kwargs={"volunteer_id": 1})
    )
    assert response.status_code == 404


@mark.django_db
def test_returns_404_on_missing_user(mrf: MessagesRequestFactory):
    request = mrf.post(reverse("volunteer_list"), {"volunteer_id": "19223984712098"})
    request.user = UserFactory.create(permission="volunteers.view_volunteer")


@mark.django_db
def test_request_availabilities_page_is_restricted(rf: RequestFactory):
    assert_requires_permissions(
        rf, request_availabilities_view, ["volunteers.view_volunteer"]
    )


@mark.django_db
def test_request_availabilities_page_shows_volunteer_details(rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    name = "Carola"
    email = "carola@rackete.org"
    volunteer: Volunteer = VolunteerFactory.create(first_name=name, email=email)
    MissionFactory.create()
    response = request_availabilities_view(request, volunteer.pk)
    html = BeautifulSoup(response.content, features="html.parser")
    assert name in html.text
    assert email in html.text


@mark.django_db
@freeze_time(date(2020, 1, 21))
def test_request_availabilities_page_shows_mission_details(rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    name = "Pia"
    volunteer: Volunteer = VolunteerFactory.create(first_name=name)
    MissionFactory.create(start_date=date.fromisoformat("2022-01-12"))
    mission2: Mission = MissionFactory.create(
        start_date=date.fromisoformat("2022-02-12")
    )
    avail = AvailabilityFactory.create(volunteer=volunteer, mission=mission2)
    response = request_availabilities_view(request, volunteer.pk)
    html = BeautifulSoup(response.content, features="html.parser")
    assert "12.01.2022 - " in html.text
    assert avail.availability.title() in html.text


@mark.django_db
def test_request_availabilities_page_shows_an_indicator_for_outdated_information(
    rf: RequestFactory,
):
    request = rf.get("")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    volunteer: Volunteer = VolunteerFactory.create()
    mission: Mission = MissionFactory.create()
    AvailabilityFactory.create(volunteer=volunteer, mission=mission)
    mission.version = 1234
    mission.save()
    response = request_availabilities_view(request, volunteer.pk)
    html = BeautifulSoup(response.content, features="html.parser")
    assert "(outdated)" in html.text


@mark.django_db
def test_request_availabilities_page_sends_request_with_post_to_public_backend(
    mrf: MessagesRequestFactory,
):
    # given there is one volunteer and a mission
    name = "emailuel"
    email = "mail@reine-vernunft.de"
    volunteer: Volunteer = VolunteerFactory.create(
        first_name=name, last_name="Cant", email=email
    )
    mission: Mission = MissionFactory.create()

    # when i call the availability request view using POST function
    request = mrf.post("", {"confirm": "Confirm"})
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    request_availabilities_mock = Mock()
    request_availabilities_view(
        request,
        volunteer_id=volunteer.pk,
        request_availabilities=request_availabilities_mock,
    )

    # then i expect that request availability was called
    assert request_availabilities_mock.call_count == 1
    # and i expect that a new availability object was created
    avail = Availability.objects.get(mission=mission, volunteer=volunteer)
    # and i expect the mission version to be correct
    assert avail.mission_version == mission.version
    # and i expect that the availability field is set to unknown
    assert avail.availability == Availability.UNKNOWN
    # and i expect that request availabilites was called with the correct object
    dto = avail.to_dto()
    request_availabilities_mock.assert_called_with(volunteer, [dto])


@mark.django_db
def test_request_availabilities_page_has_form_submit(rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    volunteer: Volunteer = VolunteerFactory.create()
    MissionFactory.create()
    response = request_availabilities_view(request, volunteer.pk)
    html = BeautifulSoup(response.content, features="html.parser")
    form = html.find("form")
    assert isinstance(form, Tag)
    assert form.attrs["action"] == reverse(
        "volunteer_request_availabilities", kwargs={"volunteer_id": volunteer.pk}
    )


@mark.django_db
@freeze_time(date(2022, 1, 21))
def test_request_does_not_include_missions_that_are_over(mrf: MessagesRequestFactory):
    request = mrf.post("")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    volunteer: Volunteer = VolunteerFactory.create()
    MissionFactory.create(start_date=date(2010, 12, 21))
    future_mission_start_date = date(2024, 12, 21)
    MissionFactory.create(start_date=future_mission_start_date)
    request_availability_mock = Mock()
    response = request_availabilities_view(
        request, volunteer.pk, request_availability_mock
    )
    dtos = request_availability_mock.call_args[0][1]
    assert [d.start_date for d in dtos] == [future_mission_start_date]


@mark.django_db
@freeze_time(date(2022, 1, 21))
def test_view_does_not_include_missions_that_are_over(rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    volunteer: Volunteer = VolunteerFactory.create()
    MissionFactory.create(start_date=date(2010, 12, 21))
    MissionFactory.create(start_date=date(2024, 12, 21))
    response = request_availabilities_view(request, volunteer.pk)
    html = BeautifulSoup(response.content, features="html.parser")
    assert "21.12.2010 - " not in html.text
    assert "21.12.2024 - " in html.text


@mark.django_db
def test_shows_newest_availabilities_if_there_are_multiple_versions(
    mrf: MessagesRequestFactory,
):
    volunteer = VolunteerFactory()
    mission = MissionFactory()
    AvailabilityFactory(
        volunteer=volunteer, mission=mission, availability=Availability.YES
    )

    mission.end_date += timedelta(days=7)
    mission.save()
    AvailabilityFactory(
        volunteer=volunteer, mission=mission, availability=Availability.MAYBE
    )

    request = mrf.get("")
    request.user = UserFactory(permissions=["volunteers.view_volunteer"])
    response = request_availabilities_view(request, volunteer.id)
    html = BeautifulSoup(response.content, features="html.parser")

    matcher: Callable[[Tag], bool] = lambda e: e.name == "tr" and mission.name in e.text
    row = html.find(matcher)
    assert isinstance(row, Tag)
    assert row.find_all("td")[-1].text.strip() == "Maybe"


@mark.django_db
def test_shows_previous_responses_as_outdated_if_mission_changed(
    mrf: MessagesRequestFactory,
):
    outdated = AvailabilityFactory(availability=Availability.MAYBE)
    volunteer = outdated.volunteer
    mission = outdated.mission
    mission.end_date += timedelta(days=1)
    mission.save()
    current = AvailabilityFactory(
        mission=mission,
        mission_version=mission.version,
        volunteer=volunteer,
        availability=Availability.UNKNOWN,
    )
    assert outdated.mission_version < current.mission_version  # consistency check

    request = mrf.get("")
    request.user = UserFactory(permissions=["volunteers.view_volunteer"])
    response = request_availabilities_view(request, outdated.volunteer.id)

    html = BeautifulSoup(response.content, features="html.parser")

    matcher: Callable[[Tag], bool] = lambda e: e.name == "tr" and mission.name in e.text
    row = html.find(matcher)
    assert isinstance(row, Tag)
    assert row.find_all("td")[-1].text.strip() == "Maybe (outdated)"
