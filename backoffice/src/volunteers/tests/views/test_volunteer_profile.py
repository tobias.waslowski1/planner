from datetime import date
from typing import cast

import bs4
from _pytest.python_api import raises
from bs4.element import Tag
from django.core.exceptions import PermissionDenied
from django.template.response import TemplateResponse
from django.test.client import Client, RequestFactory
from django.urls import reverse
from planner_common import features
from planner_common.models import Gender
from pytest import mark

from seawatch_registration.tests.factories import PositionFactory, UserFactory
from volunteers.models import Volunteer
from volunteers.tests.factories import VolunteerFactory
from volunteers.views import VolunteerListView, VolunteerProfileView


@mark.django_db
def test_list_links_to_profile(rf: RequestFactory):
    volunteer: Volunteer = VolunteerFactory.create(name="Super Mario")

    request = rf.get("")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    response = VolunteerListView.as_view()(request)
    assert isinstance(response, TemplateResponse)

    html = bs4.BeautifulSoup(response.rendered_content, features="html.parser")

    entry: Tag = [
        th
        for th in html.find_all("th")
        if th.text.strip() == "Super Mario"  # type: ignore
    ][0]
    assert entry

    link = entry.find("a")
    assert link
    assert link["href"] == reverse("volunteer_detail", kwargs={"pk": volunteer.pk})  # type: ignore


@mark.django_db
def test_is_reachable(client: Client):
    volunteer: Volunteer = VolunteerFactory.create()
    client.force_login(UserFactory.create(view_permissions=VolunteerProfileView))
    response = client.get(reverse("volunteer_detail", kwargs={"pk": volunteer.pk}))
    assert response.status_code == 200


@mark.django_db
def test_requires_permissions(rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory()  # type: ignore # missing required permission

    with raises(PermissionDenied):
        VolunteerProfileView.as_view()(request, pk=VolunteerFactory.create().pk)


@mark.django_db
def test_shows_fields(rf: RequestFactory):
    simple_to_verify_fields = {
        "name": "Marceline the Vampire Queen",
        "phone_number": "+414204242",
        "email": "this-is-fine+foo@example.com",
    }
    hard_to_verify_fields = {
        "gender": Gender.PREFER_NOT_SAY,
        "date_of_birth": date(2132, 3, 15),
        "positions": [
            PositionFactory(name="Left pos"),
            PositionFactory(name="Right pos"),
        ],
        "certificates": ["drivers-license", "general-awesomeness"],
        "languages": {"deu": 1, "eng": 3},
        "nationalities": ["jp", "kz"],
    }
    volunteer: Volunteer = VolunteerFactory.create(
        **simple_to_verify_fields, **hard_to_verify_fields
    )
    request = rf.get("")
    request.user = UserFactory.create(view_permissions=VolunteerProfileView)

    response = VolunteerProfileView.as_view()(request, pk=volunteer.pk)

    assert isinstance(response, TemplateResponse)
    for value in simple_to_verify_fields.values():
        assert value in response.rendered_content

    assert str(hard_to_verify_fields["gender"].label) in response.rendered_content

    assert "15.03.2132" in response.rendered_content

    for position in hard_to_verify_fields["positions"]:
        assert position.name in response.rendered_content

    for cert in hard_to_verify_fields["certificates"]:
        assert cert in response.rendered_content

    assert "German (B1-B2)" in response.rendered_content
    assert "English (Native)" in response.rendered_content

    assert "Japanese" in response.rendered_content
    assert "Kazakhstani" in response.rendered_content


if features.archive_volunteers:

    @mark.django_db
    def test_archive_button(rf: RequestFactory):
        # GIVEN a profile page
        volunteer = VolunteerFactory()

        # WHEN I open it
        request = rf.get("")
        request.user = UserFactory(view_permissions=VolunteerProfileView)
        response = VolunteerProfileView.as_view()(request, pk=volunteer.id)

        # THEN There's a delete button in a form with the right url and method
        html = bs4.BeautifulSoup(
            cast(TemplateResponse, response).rendered_content, features="html.parser"
        )
        form = html.find(
            "form",
            method="POST",
            action=reverse("volunteer_archive", kwargs={"pk": volunteer.id}),
        )
        assert isinstance(form, Tag)
        submit = form.find("input", type="submit")
        assert isinstance(submit, Tag)
        assert submit["value"] == "Archive Profile"
