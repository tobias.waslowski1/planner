import json
from http import HTTPStatus

from django.test.client import RequestFactory
from pytest import mark

from volunteers.tests.factories import VolunteerFactory
from volunteers.views import volunteer_update_autosave


@mark.parametrize(
    "data",
    [
        {},
        {"foo": 5},
        {"first_name": "x", "last_name": "y"},
        {"first_name": "y", "foo": 5},
    ],
)
def test_bad_request(data, rf: RequestFactory):
    request = rf.post("", data=data)
    assert (
        HTTPStatus(volunteer_update_autosave(request, 42).status_code)
        == HTTPStatus.BAD_REQUEST
    )


@mark.django_db
def test_saves_regular_field(rf: RequestFactory):
    volunteer = VolunteerFactory()
    request = rf.post("", data={"first_name": "foo"})
    response = volunteer_update_autosave(request, pk=volunteer.id)
    assert 200 <= response.status_code < 300

    volunteer.refresh_from_db()
    assert volunteer.first_name == "foo"


@mark.django_db
def test_saves_languages(rf: RequestFactory):
    new_languages = {"deu": 2, "eng": 3}
    volunteer = VolunteerFactory()
    request = rf.post("", data={"languages": json.dumps(new_languages)})
    response = volunteer_update_autosave(request, pk=volunteer.id)
    assert 200 <= response.status_code < 300

    volunteer.refresh_from_db()
    assert volunteer.languages == new_languages


@mark.django_db
def test_validates_languages(rf: RequestFactory):
    volunteer = VolunteerFactory()
    request = rf.post("", data={"languages": json.dumps({"asdf": 42})})
    response = volunteer_update_autosave(request, pk=volunteer.id)
    assert HTTPStatus(response.status_code) == HTTPStatus.BAD_REQUEST
    assert json.loads(response.content) == {
        "errors": {"languages": ["'asdf' is not a valid language code"]}
    }


@mark.django_db
def test_returns_no_content(rf: RequestFactory):
    request = rf.post("", data={"first_name": "x"})
    response = volunteer_update_autosave(request, pk=VolunteerFactory().id)

    assert HTTPStatus(response.status_code) == HTTPStatus.NO_CONTENT


@mark.django_db
def test_renders_errors_in_json(rf: RequestFactory):
    request = rf.post("", data={"first_name": ""})
    response = volunteer_update_autosave(request, pk=VolunteerFactory().id)
    assert HTTPStatus(response.status_code) == HTTPStatus.BAD_REQUEST
    assert json.loads(response.content) == {
        "errors": {"first_name": ["This field is required."]}
    }


@mark.django_db
def test_saves_history(rf: RequestFactory):
    volunteer = VolunteerFactory(first_name="foo")
    request = rf.post("", data={"first_name": "bar"})
    response = volunteer_update_autosave(request, pk=volunteer.id)

    assert HTTPStatus(response.status_code) == HTTPStatus.NO_CONTENT
    assert volunteer.history.last().change_json == [
        {"field": "first_name", "old": "foo", "new": "bar"}
    ]
