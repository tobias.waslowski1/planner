from bs4 import BeautifulSoup
from django.template.response import TemplateResponse
from django.test import Client
from django.urls import reverse
from pytest import mark

from seawatch_registration.tests.factories import UserFactory

from .factories import VolunteerFactory


@mark.django_db
def test_volunteer_admin(client: Client):
    client.force_login(UserFactory.create(is_staff=True, permission="view_volunteer"))
    assert (
        client.get(reverse("admin:volunteers_volunteer_changelist")).status_code == 200
    )


@mark.django_db
def test_volunteer_name_in_list(client: Client):
    name = "Reason Davis"
    VolunteerFactory(id=94, name=name)
    client.force_login(UserFactory.create(is_staff=True, permission="view_volunteer"))

    response = client.get(reverse("admin:volunteers_volunteer_changelist"))
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    detailsUrl = reverse("admin:volunteers_volunteer_change", kwargs={"object_id": 94})

    links = html.find_all("a", href=detailsUrl)
    assert len(links) == 1
    assert links[0].text == name
