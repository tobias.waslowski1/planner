from datetime import timedelta
from http import HTTPStatus

from pytest import mark

from missions.models import Availability, Mission
from missions.tests.factories import MissionFactory
from seawatch_planner.tests.messages import MessagesRequestFactory
from seawatch_registration.tests.factories import UserFactory
from volunteers.tests.factories import VolunteerFactory
from volunteers.views import request_availabilities_view


def noop(*args, **kwargs):
    return None


@mark.django_db
def test_request_availabilities_after_changed_mission(mrf: MessagesRequestFactory):
    # GIVEN a volunteer and a mission
    volunteer = VolunteerFactory()
    mission = MissionFactory()
    user = UserFactory(permissions=["volunteers.view_volunteer"])
    old_mission_version = mission.version

    # AND we request availabilities for that mission
    request = mrf.post("")
    request.user = user

    assert request_availabilities_view(
        request, volunteer.id, request_availabilities=noop
    )
    # AND the mission changes
    mission.end_date = mission.end_date + timedelta(days=1)
    mission.save()
    mission.refresh_from_db()
    assert mission.version != old_mission_version  # consistency check
    # AND we request availabilities again
    request = mrf.post("")
    request.user = user

    assert request_availabilities_view(
        request, volunteer.id, request_availabilities=noop
    )

    assert (
        Availability.objects.filter(volunteer=volunteer).count() == 2
    )  # consistency check

    # WHEN I visit the request-availabilities-page
    request = mrf.get("")
    request.user = user
    response = request_availabilities_view(
        request, volunteer.id, request_availabilities=noop
    )

    # THEN I expect the request to be successful
    assert response.status_code == 200
