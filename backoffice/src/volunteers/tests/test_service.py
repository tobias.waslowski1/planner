import json
from typing import List

from planner_common import dtos
from planner_common.test.factories import AvailabilityFactory, InquiryFactory
from pytest_httpserver import HTTPServer

from volunteers.models import Volunteer
from volunteers.tests.factories import VolunteerFactory

from ..service import import_inquiries, request_availabilities


def test_request_availabilities_requests_availabilites_for_all_missions(
    settings, httpserver: HTTPServer
):
    # Given there are 2 missions and a user defined
    settings.PLANNER_PUBLIC_BACKEND_URL = httpserver.url_for("")
    settings.PLANNER_PUBLIC_BACKEND_AUTH_TOKEN = "insecure-preshared-token-for-dev"
    volunteer: Volunteer = VolunteerFactory.build(
        email="thefittest@darwin.net", first_name="Charles"
    )
    availability_dtos: List[dtos.Availability] = [
        AvailabilityFactory.create(),
        AvailabilityFactory.create(),
    ]
    data = dtos.AvailabilityInquiryData(
        name=volunteer.first_name,
        email=volunteer.email,
        missions=availability_dtos,
    )

    # I expect the correct data to be passed to the api call
    httpserver.expect_request(
        "/applications/request_availabilities/",
        method="POST",
        json=data.to_dict(),
    ).respond_with_json({})

    # When i execute the request_availabilities function
    try:
        request_availabilities(volunteer, availability_dtos)
    finally:
        httpserver.check()


def test_import_inquries_parses_dtos(settings, httpserver: HTTPServer):
    settings.PLANNER_PUBLIC_BACKEND_URL = httpserver.url_for("")
    settings.PLANNER_PUBLIC_BACKEND_AUTH_TOKEN = "insecure-preshared-token-for-dev"

    # Given there is one Inquiry present in the public backend
    inquiry_dtos: List[dtos.Inquiry] = InquiryFactory.build_batch(2)
    httpserver.expect_request("/applications/inquiries/").respond_with_json(
        {"inquiries": [d.to_dict() for d in inquiry_dtos]}
    )

    # When I execute the import inquiry function
    imported_dtos = import_inquiries()

    # I expect that the import function return the DTO
    assert imported_dtos == inquiry_dtos


def test_request_availabilities_requests_availabilites_with_availabilities(
    settings, httpserver: HTTPServer
):
    # Given there are 2 missions and a volunteer
    settings.PLANNER_PUBLIC_BACKEND_URL = httpserver.url_for("")
    settings.PLANNER_PUBLIC_BACKEND_AUTH_TOKEN = "insecure-preshared-token-for-dev"
    volunteer: Volunteer = VolunteerFactory.build(
        email="not@yetdecid.ed", first_name="Daut"
    )
    availability_dtos: List[dtos.Availability] = [
        AvailabilityFactory.create(availability=dtos.Availability.Answer.Maybe),
        AvailabilityFactory.create(availability=dtos.Availability.Answer.Unknown),
    ]
    data = dtos.AvailabilityInquiryData(
        name=volunteer.first_name,
        email=volunteer.email,
        missions=availability_dtos,
    )

    # I expect the correct data to be passed to the api call
    httpserver.expect_request(
        "/applications/request_availabilities/",
        method="POST",
        json=data.to_dict(),
    ).respond_with_json({})

    # When i execute the request_availabilities function
    try:
        request_availabilities(volunteer, availability_dtos)
    finally:
        httpserver.check()
