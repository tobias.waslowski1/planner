from os import environ

from playwright.sync_api import BrowserContext
from pytest import mark

from applications.tests.factories import ApplicationFactory

pytestmark = mark.browser


@mark.skipif(
    environ.get("PLANNER_PLAYWRIGHT_VERSION") is None,
    reason="playwright browsers haven't been installed via nix",
)
def test_example_is_working(
    logged_in_context: BrowserContext,
):
    application = ApplicationFactory(first_name="chaos and confusion")

    page = logged_in_context.new_page()
    page.goto(application.get_absolute_url())
    label = page.query_selector("label:text('First Name')")
    assert label
    assert page.input_value(f"#{label.get_attribute('for')}") == "chaos and confusion"
