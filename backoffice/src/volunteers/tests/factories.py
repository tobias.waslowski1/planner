import random
from typing import Callable, ClassVar, List, TypeVar, Union

from django.utils.timezone import utc
from factory import Faker, LazyFunction, SubFactory, post_generation
from factory.declarations import LazyAttribute, SelfAttribute
from factory.django import DjangoModelFactory
from phonenumber_field.phonenumber import PhoneNumber
from planner_common import data
from planner_common.models import Gender
from planner_common.test.factories import CertificatesListFactory, TypedFactoryMetaClass

from applications.tests.factories import ApplicationFactory
from seawatch_registration.models import Position

from ..models import Volunteer


class VolunteerFactory(DjangoModelFactory, metaclass=TypedFactoryMetaClass[Volunteer]):
    class Meta:
        model = Volunteer

    create: ClassVar[Callable[..., Volunteer]]
    build: ClassVar[Callable[..., Volunteer]]

    class Params:
        # The seed is used so we only depend on the randomness from factory_boy
        positions__seed = Faker("random_int")
        positions__count = Faker("random_int", min=1, max=3)
        name = Faker("name")
        accepted = Faker("past_datetime", tzinfo=utc)
        languages_count = Faker("random_int", min=1, max=5)
        language_codes = Faker(
            "random_elements",
            unique=True,
            length=SelfAttribute("..languages_count"),
            elements=[l["code"] for l in data.languages],
        )
        language_levels = Faker(
            "random_choices",
            length=SelfAttribute("..languages_count"),
            elements=[0, 1, 2, 3],
        )
        phone_number_tail = Faker("numerify", text="#######")

    first_name = LazyAttribute(lambda s: s.name.split(" ", 1)[0])
    last_name = LazyAttribute(lambda s: s.name.split(" ", 1)[1])
    phone_number = LazyAttribute(
        lambda s: PhoneNumber.from_string(f"+49 170 {s.phone_number_tail}")
    )
    email = Faker("ascii_email")
    date_of_birth = Faker("date_of_birth")
    gender = Faker("random_element", elements=Gender.values)
    certificates = SubFactory(CertificatesListFactory)
    languages = LazyAttribute(
        lambda s: {
            code: level for (code, level) in zip(s.language_codes, s.language_levels)
        }
    )
    nationalities = ["gb", "it", "au"]

    application = SubFactory(ApplicationFactory, accepted=SelfAttribute("..accepted"))

    @post_generation
    def positions(obj, create, positions, **kwargs):
        if create:
            if positions is None:
                if "keys" in kwargs:
                    positions = [
                        Position.objects.get(key=key) for key in kwargs["keys"]
                    ]
                else:
                    all_positions = list(Position.objects.all())
                    positions = random.Random(kwargs["seed"]).sample(
                        all_positions, min(len(all_positions), kwargs["count"])
                    )
            obj.positions.set(positions)
