from pytest import mark

from applications.models import PositionDecision
from applications.tests.factories import ApplicationFactory
from seawatch_registration.tests.factories import PositionFactory
from volunteers.models import Volunteer


@mark.django_db
def test_create_from_Application_references_application():
    application = ApplicationFactory()
    volunteer = Volunteer.objects.create_from_application(application)
    assert volunteer.application == application


@mark.django_db
def test_create_from_application_uses_only_accepted_positions():
    application = ApplicationFactory(
        positions=[
            PositionFactory(key="unclear"),
            PositionFactory(key="yes"),
            PositionFactory(key="no"),
        ]
    )
    application.position_decisions.filter(position__key="unclear").update(
        decision=PositionDecision.Decision.OPEN
    )
    application.position_decisions.filter(position__key="yes").update(
        decision=PositionDecision.Decision.ACCEPT
    )
    application.position_decisions.filter(position__key="no").update(
        decision=PositionDecision.Decision.REJECT
    )

    volunteer = Volunteer.objects.create_from_application(application)

    assert {p.key for p in volunteer.positions.all()} == {"yes"}


@mark.django_db
def test_Application_acceptedPositions_returns_accepted_positions():
    application = ApplicationFactory(
        positions=[
            PositionFactory(key="yes"),
            PositionFactory(key="no"),
        ]
    )
    application.position_decisions.filter(position__key="yes").update(
        decision=PositionDecision.Decision.ACCEPT
    )
    application.position_decisions.filter(position__key="no").update(
        decision=PositionDecision.Decision.REJECT
    )

    assert {p.key for p in application.accepted_positions} == {"yes"}
