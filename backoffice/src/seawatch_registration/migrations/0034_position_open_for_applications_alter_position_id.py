# Generated by Django 4.0.4 on 2022-04-25 11:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("seawatch_registration", "0033_alter_position_options"),
    ]

    operations = [
        migrations.AddField(
            model_name="position",
            name="open_for_applications",
            field=models.BooleanField(default=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name="position",
            name="id",
            field=models.AutoField(primary_key=True, serialize=False),
        ),
    ]
