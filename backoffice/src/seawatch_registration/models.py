from django import forms
from django.db import models
from django.db.models.fields import BooleanField
from django.utils.translation import gettext_lazy as _


class ShortTextField(models.TextField):
    description = _("TextField that uses a forms.TextInput by default")

    def formfield(self, **kwargs):
        return super().formfield(
            **{
                **({} if self.choices is not None else {"widget": forms.TextInput}),
                **kwargs,
            }
        )


class Position(models.Model):
    class Meta:
        ordering = ["key"]

    id = models.AutoField(primary_key=True)

    key = ShortTextField(unique=True)
    name = ShortTextField(verbose_name=_("Name"))

    open_for_applications = BooleanField()

    def __str__(self):
        return self.name
