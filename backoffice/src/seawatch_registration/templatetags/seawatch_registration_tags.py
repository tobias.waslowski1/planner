from typing import List, Optional

from countryinfo import CountryInfo
from django import template
from django.conf import settings
from django.utils.translation import pgettext
from django_countries.fields import Country
from planner_common import data

register = template.Library()


@register.simple_tag
def feedback_url() -> Optional[str]:
    return settings.FEEDBACK_URL


language_names = {l["code"]: l["name"] for l in data.languages if l["backoffice"]}
levels = {
    0: "A1-A2",
    1: "B1-B2",
    2: "C1-C2",
    3: pgettext("As in native speaker", "Native"),
}


@register.filter
def format_spoken_languages(languages):
    return ", ".join(
        sorted(
            f"{language_names[code]} ({levels[points]})"
            for code, points in languages.items()
        )
    )


@register.filter
def format_list_of_spoken_language_objects(languages):
    return format_spoken_languages(
        {language.code: language.points for language in languages}
    )


def _format_nationality(nationality: str) -> str:
    try:
        return CountryInfo(nationality).demonym()
    except KeyError:
        return Country(nationality).name


@register.filter
def format_nationalities(nationalities: List[str]) -> str:
    return ", ".join(sorted(_format_nationality(n) for n in nationalities))
