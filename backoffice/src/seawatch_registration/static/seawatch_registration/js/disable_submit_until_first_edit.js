window.addEventListener("load", () => {
  Array.from(document.querySelectorAll("[data-disable-until-first-edit]")).forEach(
    (element) => {
      // Grab the enclosing form
      const form = element.closest("form");
      if (form === null) {
        console.error(
          "[data-disable-until-first-edit] used, but couldn't find parent form",
          element
        );
        return;
      }

      form.disabled_until_first_edit = true;
      // Prevent submission and disable the element
      form.addEventListener("submit", (e) => {
        if (form.disabled_until_first_edit) {
          e.preventDefault();
        }
      });

      element.setAttribute("aria-disabled", "true");
      element.setAttribute("title", "Edit the data before trying to save");

      // On change, re-enable to button and stop preventing submission
      ["change", "input"].forEach((eventType) => {
        form.addEventListener(
          eventType,
          () => {
            element.removeAttribute("aria-disabled");
            element.removeAttribute("title");
            form.disabled_until_first_edit = false;
          },
          { once: true }
        );
      });
    }
  );
});
