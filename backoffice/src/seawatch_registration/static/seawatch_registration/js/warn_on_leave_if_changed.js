window.addEventListener("load", () => {
  Array.from(document.querySelectorAll("form[data-warn-on-leave-if-changed]")).forEach(
    (form) => {
      form.warn_on_leave = false;
      window.addEventListener(
        "beforeunload",
        (event) => {
          if (form.warn_on_leave) {
            event.preventDefault();
            event.returnValue = ""; // chrome requires this to show the warning
          }
        },
        { capture: true }
      );

      form.addEventListener("change", () => {
        form.warn_on_leave = true;
      });
      form.addEventListener("input", () => {
        form.warn_on_leave = true;
      });
      form.addEventListener("submit", () => {
        form.warn_on_leave = false;
      });
    }
  );
});
