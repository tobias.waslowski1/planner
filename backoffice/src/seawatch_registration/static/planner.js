///////////////////////
// HTMX Error Listeners

document.body.addEventListener("htmx:responseError", (event) => {
  console.log("server response error", event);
  alert("Server response error. Please reload the page and try again.");
});
document.body.addEventListener("htmx:sendError", (event) => {
  console.log("send error", event);
  alert("Error sending rqeuest to the server. Please reload the page and try again.");
});

/////////////////////////////////
// Dialog close-on-backdrop-click

function closeDialogOnBackdropClick(dialog) {
  dialog.addEventListener("click", function (event) {
    var rect = dialog.getBoundingClientRect();
    var isInDialog =
      rect.top <= event.clientY &&
      event.clientY <= rect.top + rect.height &&
      rect.left <= event.clientX &&
      event.clientX <= rect.left + rect.width;
    if (!isInDialog) {
      dialog.close();
    }
  });
}

Array.from(document.body.querySelectorAll("dialog")).forEach((dialog) =>
  closeDialogOnBackdropClick(dialog)
);

const dialogBackdropMutationObserver = new MutationObserver(
  (mutationList, _observer) => {
    mutationList
      .map((ml) => Array.from(ml.addedNodes))
      .flat()
      .filter((n) => n.nodeName == "DIALOG")
      .forEach((d) => closeDialogOnBackdropClick(d));
  }
);
dialogBackdropMutationObserver.observe(document.body, {
  childList: true,
  subtree: true,
});

///////////////////////////
// Custom elements

class ChoicesJS extends HTMLSelectElement {
  constructor() {
    super();
    this._choices = new window.Choices(this, {
      removeItemButton: true,
      duplicateItemsAllowed: false,
      shouldSort: false,
    });
    this.dispatchEvent(new Event("initialized"));
  }
}
customElements.define("choices-js", ChoicesJS, { extends: "select" });

class AutosavingCertificates extends HTMLElement {
  get _select() {
    return this.querySelector("select");
  }
  get _otherLabel() {
    return this.querySelector("label.other-certificates");
  }
  get _otherInput() {
    return this.querySelector("input.other-certificates");
  }

  constructor() {
    super();

    // Setup choices.js
    this._choices = new window.Choices(this._select, {
      removeItemButton: true,
      duplicateItemsAllowed: false,
      shouldSort: false,
    });

    // Update "other" visibility depending on selected elements
    this._select.addEventListener("change", this._update_other_visibility.bind(this));
    this._select.addEventListener(
      "initialized",
      this._update_other_visibility.bind(this),
      { once: true }
    );

    // auto-save the new value on change or enter
    this._select.addEventListener(
      "change",
      this._submit.bind(this, ".choices-container")
    );
    this._otherInput.addEventListener(
      "change",
      this._submit.bind(this, ".input-container")
    );
    this._update_other_visibility();
  }

  _update_other_visibility() {
    if (this._choices.getValue().find((e) => e.value == "other")) {
      // show
      this._otherInput.removeAttribute("disabled");
      this._otherInput.removeAttribute("aria-hidden");
      this._otherLabel.removeAttribute("aria-hidden");
    } else {
      this._otherInput.setAttribute("disabled", "");
      this._otherInput.setAttribute("aria-hidden", "true");
      this._otherLabel.setAttribute("aria-hidden", "true");
    }
  }

  _resetIndicators() {
    Array.from(this.querySelectorAll(".autosave-indicator")).forEach((indicator) => {
      indicator.style.opacity = 0;
    });
  }

  _reportError(container, errorOrResponse) {
    this._resetIndicators();
    this.querySelector(`${container} .autosave-indicator-error`).style.opacity = 1;
    console.log(errorOrResponse);
    alert("Error saving certificates. Please reload the page and try again");
  }

  _reportSuccess(container) {
    this._resetIndicators();
    this.querySelector(`${container} .autosave-indicator-saved`).style.opacity = 1;
    setTimeout(() => {
      this.querySelector(`${container} .autosave-indicator-saved`).style.opacity = 0;
    }, 3000);
  }

  _submit(container) {
    this._resetIndicators();
    this.querySelector(`${container} .autosave-indicator-saving`).style.opacity = 1;
    const responseP = this._doFetch();

    responseP.catch((error) => this._reportError(container, error));

    responseP.then((response) => {
      if (response.ok) {
        this._reportSuccess(container);
      } else {
        this._reportError(container, response);
      }
    });
  }

  _doFetch() {
    const /** @type string[] */ selectedCertificates = this._choices.getValue(true);

    return fetch("autosave/", {
      method: "POST",
      headers: {
        "X-CSRFToken": document.body.dataset.csrfToken,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        certificates: selectedCertificates.map((cert) =>
          // If the "other" input has a value, we append it to the "other" entry in the multiselect,
          // otherwise we just leave a potential "other" alone.
          cert == "other" && this._otherInput.value
            ? `other: ${this._otherInput.value}`
            : cert
        ),
      }),
      credentials: "same-origin",
    });
  }
}
customElements.define("autosaving-certificates", AutosavingCertificates);
