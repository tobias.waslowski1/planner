from typing import Optional

from django import forms
from django.contrib import admin
from django.core.validators import validate_slug

from . import models


@admin.register(models.Position)
class PositionAdmin(admin.ModelAdmin):
    exclude = ["open_for_applications"]

    def get_readonly_fields(self, request, obj: Optional[models.Position]):
        if obj:
            return ["key"]
        else:
            return []

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        queryset = queryset.filter(open_for_applications=False)
        return queryset

    def save_model(self, request, obj: models.Position, form, change) -> None:
        obj.open_for_applications = False
        return super().save_model(request, obj, form, change)

    class form(forms.ModelForm):
        key = forms.fields.CharField(validators=[validate_slug])
