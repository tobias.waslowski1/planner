from http import HTTPStatus

from django.contrib.auth.decorators import permission_required
from django.http.response import HttpResponseServerError
from django.test import Client
from django.urls import reverse
from phonenumbers import is_possible_number_string
from pytest import mark
from pytest_django.asserts import assertNotContains

from seawatch_planner.tests.test_middleware_RequireLoginMiddleware import respondStub
from seawatch_registration.models import Position
from seawatch_registration.tests.factories import PositionFactory, UserFactory


@mark.django_db
def test_PositionAdmin_allows_creation(client: Client):
    client.force_login(
        UserFactory(is_staff=True, permissions=["seawatch_registration.add_position"])
    )
    response = client.post(
        reverse("admin:seawatch_registration_position_add"),
        {"key": "test-position", "name": "Test Position"},
    )
    assert HTTPStatus(response.status_code) == HTTPStatus.FOUND
    position = Position.objects.get(key="test-position")
    assert position.name == "Test Position"


@mark.django_db
def test_PositionAdmin_rejects_modifications_to_key(client: Client):
    client.force_login(
        UserFactory(
            is_staff=True, permissions=["seawatch_registration.change_position"]
        )
    )
    position = PositionFactory(key="foo", open_for_applications=False)

    response = client.post(
        reverse(
            "admin:seawatch_registration_position_change",
            kwargs={"object_id": position.pk},
        ),
        {"key": "bar", "name": "whatever"},
    )
    assert HTTPStatus(response.status_code) == HTTPStatus.FOUND

    position.refresh_from_db()
    assert position.key == "foo"


@mark.django_db
def test_PositionAdmin_only_shows_additional_positions(client: Client):
    client.force_login(
        UserFactory(is_staff=True, permissions=["seawatch_registration.view_position"])
    )
    response = client.get(reverse("admin:seawatch_registration_position_changelist"))
    assert Position.objects.filter(name="Bosun").exists()  # consistency check
    assertNotContains(response, "Bosun")


@mark.django_db
def test_PositionAdmin_forms_dont_include_additional_positions_field(client: Client):
    client.force_login(
        UserFactory(
            is_staff=True,
            permissions=[
                f"seawatch_registration.{x}_position" for x in ["add", "change"]
            ],
        )
    )
    response = client.get(reverse("admin:seawatch_registration_position_add"))
    assertNotContains(response, "open_for_applications")


@mark.django_db
def test_PositionAdmin_doesnt_allow_changing_positions_that_are_open_for_applications(
    client: Client,
):
    client.force_login(
        UserFactory(
            is_staff=True, permissions=["seawatch_registration.change_position"]
        )
    )
    position = PositionFactory(key="k", name="n", open_for_applications=True)
    response = client.post(
        reverse(
            "admin:seawatch_registration_position_change",
            kwargs={"object_id": position.id},
        ),
        {"key": "test-position", "name": "Test Position"},
    )
    assert HTTPStatus(response.status_code) == HTTPStatus.FOUND
    position.refresh_from_db()
    assert position.key == "k"
    assert position.name == "n"


@mark.django_db
def test_PositionAdmin_validates_key_as_slug(client: Client):
    client.force_login(
        UserFactory(is_staff=True, permissions=["seawatch_registration.add_position"])
    )
    response = client.post(
        reverse("admin:seawatch_registration_position_add"),
        {"key": "invalid key", "name": "Test Position"},
    )
    assert HTTPStatus(response.status_code) == HTTPStatus.OK
    assert response.context["adminform"].errors == {
        "key": [
            "Enter a valid “slug” consisting of letters, numbers, underscores or hyphens."
        ]
    }
