from seawatch_registration.templatetags.seawatch_registration_tags import (
    format_nationalities,
)


def test_format_nationalities_supports_andorra():
    # Andorra is not supported by countryinfo
    assert format_nationalities(["ad"]) == "Andorra"
