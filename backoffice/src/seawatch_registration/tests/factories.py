from typing import Callable, ClassVar, Iterable
from typing import Sequence as SequenceType
from typing import Type, Union, cast

from django.contrib.auth.models import Permission, User
from django.views import View
from factory import Faker, LazyAttribute, post_generation
from factory.django import DjangoModelFactory
from planner_common.test.factories import TypedFactoryMetaClass

from ..models import Position


class UserFactory(DjangoModelFactory, metaclass=TypedFactoryMetaClass[User]):
    class Meta:
        model = User
        django_get_or_create = ("email",)

    create: ClassVar[Callable[..., User]]
    build: ClassVar[Callable[..., User]]

    first_name = Faker("first_name")
    last_name = Faker("last_name")
    email = Faker("email")
    username = Faker("user_name")
    is_active = True
    is_superuser = False

    @post_generation
    def permission(obj: User, create: bool, extracted: str, **kwargs):  # type: ignore
        if create and extracted is not None:
            obj.user_permissions.set([get_permission(extracted)])

    @post_generation
    def permissions(obj: User, create: bool, extracted: SequenceType[str], **kwargs):  # type: ignore
        if create and extracted is not None:
            obj.user_permissions.set(map(get_permission, extracted))

    @post_generation
    def view_permissions(
        obj: User, create: bool, view: Union[Type[View], Iterable[Type[View]]], **kwargs
    ):
        """This is meant to work with django.contrib.auth.mixins.PermissionRequiredMixin"""
        if create and view is not None:
            views = [view] if type(view) == type else view

            permissions = []
            for v in cast(Iterable[Type[View]], views):
                view_permissions = getattr(v, "permission_required", [])
                if isinstance(view_permissions, str):
                    view_permissions = [view_permissions]
                permissions.extend(view_permissions)
            obj.user_permissions.set(map(get_permission, permissions))


def get_permission(permission_str: str) -> Permission:
    if "." not in permission_str:
        return Permission.objects.get(codename=permission_str)
    else:
        app_label, codename = permission_str.split(".")
        return Permission.objects.get(
            content_type__app_label=app_label, codename=codename
        )


class PositionFactory(DjangoModelFactory, metaclass=TypedFactoryMetaClass[Position]):
    class Meta:
        model = Position

    create: ClassVar[Callable[..., Position]]
    build: ClassVar[Callable[..., Position]]

    name = Faker("job")
    key = LazyAttribute(lambda p: p.name)
    open_for_applications = True
