from django.contrib import admin

from . import models


@admin.register(models.Application)
class ApplicationAdmin(admin.ModelAdmin):
    pass


@admin.register(models.MalformedApplication)
class MalformedApplicationAdmin(admin.ModelAdmin):
    pass
