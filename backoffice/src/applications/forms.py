import json
from typing import Any, cast

from django import forms
from django.forms.models import ModelMultipleChoiceField
from django.http.request import QueryDict
from django.utils.translation import gettext as _
from django_countries import countries
from planner_common import data, features

from applications.models import (
    Application,
    Language,
    certificates_separator,
    nationalities_separator,
)
from seawatch_registration.models import Position


class ChoicesJsMedia:
    css = {"all": ["vendor/choices.js/choices.min.css"]}
    js = ["vendor/choices.js/choices.min.js"]


class ChoicesJSWidget(forms.widgets.SelectMultiple):
    template_name = "applications/forms/widgets/choices_js.html"

    Media = ChoicesJsMedia


class CSVField(forms.MultipleChoiceField):
    def __init__(self, *args, separator=";", **kwargs):
        super().__init__(*args, **kwargs)
        self.__separator = separator

    def clean(self, value):
        v = super().clean(value)
        return self.__separator.join(sorted(v))

    def prepare_value(self, value):
        if value is None:
            return []
        if isinstance(value, list):
            return value
        else:
            return value.split(self.__separator)


class LanguagesWidget(forms.SelectMultiple):
    template_name = "applications/forms/widgets/languages.html"

    Media = ChoicesJsMedia

    def value_from_datadict(self, data, files, name):
        return data[f"{name}__data"]

    def format_value(self, value):
        if isinstance(value, str):
            value = json.loads(value)
        return value

    def get_context(self, name, value, attrs):
        ctx = super().get_context(name, value, attrs)
        ctx["value_json"] = json.dumps(ctx["widget"]["value"])
        return ctx

    def optgroups(self, name, value_or_string, attrs):
        if isinstance(value_or_string, str):
            value = json.loads(value_or_string)
        else:
            value = value_or_string
        selected_values = [v["language"] for v in value]
        return [
            (
                None,
                [
                    {
                        "name": name,
                        "value": option_value,
                        "label": option_label,
                        **(
                            {"selected": True, "attrs": {"selected": True}}
                            if option_value in selected_values
                            else {"selected": False, "attrs": {}}
                        ),
                        "index": index,
                        "type": "select",
                        "template_name": "django/forms/widgets/select_option.html",
                        "wrap_label": True,
                    }
                ],
                0,
            )
            for index, (option_value, option_label) in enumerate(self.choices)
        ]


class LanguagesField(forms.Field):
    widget = LanguagesWidget

    choices = [(l["code"], l["name"]) for l in data.languages if l["backoffice"]]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.widget.choices = self.choices  # type: ignore

    def to_python(self, value):
        if not value:
            return None

        v = json.loads(value)
        languages = set(l for (l, _) in self.choices)

        # These checks aren't part of the validation because they are programming errors, not user
        # errors. We should trigger an alert, not show a form error to the user.
        if type(v) != list:
            raise Exception(f"Expected list, got {type(v)}")
        for entry in v:
            if type(entry) != dict:
                raise Exception(f"Expected dict, got {type(entry)}")
            if set(entry.keys()) != {"language", "points"}:
                raise Exception(
                    f"Expected dict with language and points, got {set(entry.keys())} instad"
                )
            if entry["language"] not in languages:
                raise Exception(f"Unknown language \"{entry['language']}\"")
            if str(entry["points"]) not in ["0", "1", "2", "3"]:
                raise Exception(
                    f"Expected points to be between 0 and 3, got {entry['points']}"
                )
        return v


class CertificatesWidget(forms.SelectMultiple):
    template_name = "applications/forms/widgets/certificates.html"

    def value_from_datadict(self, data: QueryDict, files, name):
        return [
            f"other: {data[f'{name}__other']}" if cert == "other" else cert
            for cert in data.getlist(name)
        ]

    def get_context(self, name, value, attrs):
        other = None
        for idx, cert in enumerate(value):
            if cert.startswith("other: "):
                other = cert
                value[idx] = "other"
        ctx = super().get_context(name, value, attrs)
        if other:
            ctx["other"] = other.removeprefix("other: ")
        return ctx


class CertificatesField(CSVField):
    def valid_value(self, value: Any) -> bool:
        if str(value) == "other" or str(value).startswith("other: "):
            return True
        else:
            return super().valid_value(value)


class WarnOnLeavingIfChangedMixin:
    class Media:
        js = [
            "seawatch_registration/js/disable_submit_until_first_edit.js",
            "seawatch_registration/js/warn_on_leave_if_changed.js",
        ]


certificates = [
    (g["group_label"], [(e["value"], e["label"]) for e in g["certificates"]])
    for g in data.certificates
]
certificates[0][1].append(("other", "Other (free text)"))


class ApplicationForm(WarnOnLeavingIfChangedMixin, forms.ModelForm):
    template_name_label = "applications/forms/label.html"

    def __init__(self, *args, **kwargs):
        kwargs.setdefault("label_suffix", "")
        super().__init__(*args, **kwargs)

        if self.instance:
            self.initial["languages"] = [
                {"language": l.code, "points": l.points}
                for l in self.instance.languages.all()
            ]
            self.initial["additional_positions"] = self.instance.positions.filter(
                open_for_applications=False
            )

    class Meta:
        model = Application
        label_suffix = ""
        fields = [
            "first_name",
            "last_name",
            "phone_number",
            "email",
            "date_of_birth",
            "gender",
            "nationalities",
            "languages",
            "positions",
            "certificates_csv",
        ]
        labels = {
            "first_name": _("First Name"),
            "last_name": _("Last Name"),
            "phone_number": _("Phone Number"),
            "email": _("Email Address"),
            "date_of_birth": _("Date of Birth"),
            "gender": _("Gender"),
            "nationalities": _("Nationalities"),
            "motivation": _("Why do you want to work for Sea-Watch?"),
            "qualification": _("What qualifies you for the chosen positions?"),
        }
        widgets = {"positions": ChoicesJSWidget}

    template_name = "applications/forms/application_form.html"

    nationalities = CSVField(
        separator=nationalities_separator, widget=ChoicesJSWidget, choices=countries
    )
    certificates_csv = CertificatesField(
        label=_("Certificates"),
        separator=certificates_separator,
        widget=CertificatesWidget,
        choices=certificates,
        required=False,
    )
    languages = LanguagesField(
        label=_("Spoken Languages"),
        help_text="We only ask for specific languages, applicants might know others",
    )
    positions = ModelMultipleChoiceField(
        queryset=Position.objects.filter(open_for_applications=True),
        label=_("Positions"),
        widget=ChoicesJSWidget,
    )

    # This one is only for rendering the additional positions field, not parsing
    additional_positions = ModelMultipleChoiceField(
        queryset=Position.objects.filter(open_for_applications=False),
        label=_("Additional Positions"),
        widget=ChoicesJSWidget,
        required=False,
    )

    def get_context(self, **kwargs):
        # django-types isn't up to date on this, so ignore the type error for now
        ctx = super().get_context(**kwargs)  # type: ignore
        ctx["features"] = features
        return ctx

    def clean_languages(self):
        languages = []
        for entry in self.cleaned_data["languages"]:
            try:
                l = Language.objects.get(
                    application=self.instance, code__iexact=entry["language"]
                )
            except Language.DoesNotExist:
                l = Language(application=self.instance, code=entry["language"])
            l.points = int(entry["points"])
            languages.append(l)
        return languages

    def clean_positions(self):
        return self.cleaned_data["positions"] | cast(
            Application, self.instance
        ).positions.filter(open_for_applications=False)

    def save(self, *args, **kwargs):
        instance = super().save(*args, **kwargs)
        languages = self.cleaned_data["languages"]
        for language in languages:
            language.save()
        Language.objects.filter(application=instance).exclude(
            id__in=[l.id for l in languages]
        ).delete()
        return instance
