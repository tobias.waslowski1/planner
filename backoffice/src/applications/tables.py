import django_tables2 as tables
from django.template.loader import render_to_string
from django.utils.html import format_html

from .models import Application, PositionDecision


class OpenApplicationsTable(tables.Table):
    name = tables.Column(
        accessor="name_reversed",
        linkify=True,
        order_by=("last_name", "first_name"),
        attrs={"header": True},
    )

    def render_positions(self, record: Application):
        return render_to_string(
            "applications/application_list_table_positions_cell.html",
            context={"application": record},
        )

    def render_created(self, record: Application):
        html = record.created.date().strftime("%Y-%m-%d")
        return format_html(html)

    class Meta:
        model = Application
        template_name = "applications/application_list_table.html"
        fields = ["positions", "created"]
        sequence = ["name", "positions", "created"]
        attrs = {"style": "min-width: 500px;"}
        empty_text = "There are no open applications at the moment."
