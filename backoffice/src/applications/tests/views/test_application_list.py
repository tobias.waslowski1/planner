import datetime
import re
from datetime import timedelta
from typing import cast
from unittest.mock import Mock

from bs4 import BeautifulSoup
from bs4.element import NavigableString, Tag
from django.contrib import messages
from django.template.response import TemplateResponse
from django.test import Client, RequestFactory
from django.urls import reverse
from django.utils import timezone
from freezegun import freeze_time
from pytest import mark
from pytest_django.asserts import assertContains, assertNotContains
from pytest_mock import MockerFixture

from missions.models import Availability
from missions.tests.factories import AvailabilityFactory
from seawatch_planner.tests.asserts import assert_requires_permissions
from seawatch_planner.tests.messages import MessagesRequestFactory, StubMessageStorage
from seawatch_registration.tests.factories import PositionFactory, UserFactory
from volunteers.management.commands.import_inquiries import (
    Command as ImportInquiriesCommand,
)

from ...management.commands.import_applications import (
    Command as ImportApplicationsCommand,
)
from ...models import Import
from ...views import ApplicationListView
from ..factories import ApplicationFactory


@mark.django_db
def test_is_reachable(client: Client):
    client.force_login(UserFactory.create(permission="applications.view_application"))
    response = client.get("/applications/")
    assertContains(response, "Applications")


@mark.django_db
def test_requires_permission(rf: RequestFactory):
    assert_requires_permissions(
        rf, ApplicationListView, ["applications.view_application"]
    )


@mark.django_db
def test_import_application_requires_permission(rf: RequestFactory):
    assert_requires_permissions(
        rf, ApplicationListView, ["applications.view_application"], method="post"
    )


@mark.django_db
def test_empty(rf: RequestFactory):
    request = rf.get("ignored")
    request.user = UserFactory.create(view_permissions=ApplicationListView)
    response = ApplicationListView.as_view()(request)

    assertContains(response, "There are no open applications at the moment")


@mark.django_db
def test_application_list(rf: RequestFactory):
    ApplicationFactory(first_name="foo")
    ApplicationFactory(first_name="bar")

    request = rf.get("ignored")
    request.user = UserFactory.create(view_permissions=ApplicationListView)
    response = ApplicationListView.as_view()(request)

    assertContains(response, "foo")
    assertContains(response, "bar")


@mark.django_db
def test_post_calls_import_applications(
    mrf: MessagesRequestFactory, mocker: MockerFixture
):
    import_fn_mock = Mock()
    mocker.patch.object(ImportApplicationsCommand, "handle", import_fn_mock)
    mocker.patch.object(ImportInquiriesCommand, "handle", lambda _: None)

    request = mrf.post("ignored")
    request.user = UserFactory.create(view_permissions=ApplicationListView)
    ApplicationListView.as_view()(request)

    assert import_fn_mock.call_count == 1


@mark.django_db
def test_post_calls_import_inquiries(
    mrf: MessagesRequestFactory, mocker: MockerFixture
):
    import_fn_mock = Mock()
    mocker.patch.object(ImportInquiriesCommand, "handle", import_fn_mock)
    mocker.patch.object(ImportApplicationsCommand, "handle", lambda x: None)

    request = mrf.post("ignored")
    request.user = UserFactory.create(view_permissions=ApplicationListView)
    ApplicationListView.as_view()(request)

    assert import_fn_mock.call_count == 1


@mark.django_db
def test_post_shows_error_message_when_import_fails(
    mrf: MessagesRequestFactory, mocker: MockerFixture, caplog
):
    import_error = Exception()
    import_fn_mock = Mock(side_effect=import_error)
    mocker.patch.object(ImportApplicationsCommand, "handle", import_fn_mock)

    request = mrf.post(reverse("application_list"))
    request.user = UserFactory.create(view_permissions=ApplicationListView)
    ApplicationListView.as_view()(request)

    messages = request._messages.messages
    assert messages == [
        (
            40,
            "An error occured during import. Sorry about that. "
            "The administrator has alread been notified.",
            "",
        )
    ]

    log = caplog.get_records("call")[0]
    assert log.levelname == "ERROR"
    assert log.exc_info[1] == import_error


@mark.django_db
def test_post_shows_message_with_both_app_and_avail_when_import_succeeds(
    mrf: MessagesRequestFactory, mocker: MockerFixture
):
    import_fn_mock = ApplicationFactory
    new_availability = AvailabilityFactory.create(availability=Availability.UNKNOWN)

    def set_new_availability_to_yes(_):
        new_availability.availability = Availability.YES
        new_availability.save()

    mocker.patch.object(ImportApplicationsCommand, "handle", import_fn_mock)
    mocker.patch.object(ImportInquiriesCommand, "handle", set_new_availability_to_yes)

    request = mrf.post(reverse("application_list"))
    request._messages = StubMessageStorage(request)
    request.user = UserFactory.create(view_permissions=ApplicationListView)
    ApplicationListView.as_view()(request)

    assert request._messages.messages == [
        (
            messages.INFO,
            "Import succeeded. Found 1 new applications and 1 new status updates.",
            "",
        )
    ]


@mark.django_db
def test_post_shows_message_when_import_succeeds(
    mrf: MessagesRequestFactory, mocker: MockerFixture
):
    import_fn_mock = ApplicationFactory
    mocker.patch.object(ImportApplicationsCommand, "handle", import_fn_mock)
    mocker.patch.object(ImportInquiriesCommand, "handle", lambda _: None)

    request = mrf.post(reverse("application_list"))
    request.user = UserFactory.create(view_permissions=ApplicationListView)
    ApplicationListView.as_view()(request)

    messages = request._messages.messages
    assert messages == [(20, "Import succeeded. Found 1 new applications", "")]


@mark.django_db
def test_filters_by_position(rf: RequestFactory):

    ApplicationFactory(first_name="Henry Cook", positions__keys=["cook"])
    ApplicationFactory(first_name="Meister Eder", positions__keys=["master"])

    request = rf.get("ignored", {"position": "cook"})
    request.user = UserFactory.create(view_permissions=ApplicationListView)
    response = ApplicationListView.as_view()(request)

    assertContains(response, "Henry Cook")
    assertNotContains(response, "Meister Eder")


@mark.django_db
def test_position_select_shows_all_when_not_filtered(rf: RequestFactory):
    request = rf.get("ignored")
    request.user = UserFactory.create(view_permissions=ApplicationListView)
    response = ApplicationListView.as_view()(request)

    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    position_select = html.find("select", id="position-filter")
    assert isinstance(position_select, Tag)
    assert not position_select.find("option", selected=True)
    assert cast(Tag, position_select.find("option")).text.strip() == "All Positions"


@mark.django_db
def test_position_select_shows_position_when_filtered(rf: RequestFactory):
    request = rf.get("ignored", {"position": "master"})
    request.user = UserFactory.create(view_permissions=ApplicationListView)
    response = ApplicationListView.as_view()(request)

    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    position_select = html.find("select", id="position-filter")
    assert isinstance(position_select, Tag)
    selected = position_select.find("option", selected=True)
    assert isinstance(selected, Tag)
    assert selected.text.strip() == "Master"


@mark.django_db
def test_applications_import_button_visible_which_triggers_post(rf: RequestFactory):
    ApplicationFactory()

    request = rf.get("ignored")
    request.user = UserFactory.create(view_permissions=ApplicationListView)
    response = ApplicationListView.as_view()(request)

    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    form = html.find("form", method="post", action=reverse("application_list"))
    assert form
    assert form.find("input", type="submit", value="Manual Import")  # type: ignore


@mark.django_db
def test_applications_import_button_label_shows_never_if_never_imported(
    rf: RequestFactory,
):
    ApplicationFactory()

    request = rf.get("ignored")
    request.user = UserFactory.create(view_permissions=ApplicationListView)
    response = ApplicationListView.as_view()(request)

    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    form = html.find("form", method="post", action=reverse("application_list"))
    assert isinstance(form, Tag)

    label_element = form.find("label")
    assert isinstance(label_element, Tag)
    label = label_element.find(string=re.compile("^Last import: (.*)"))
    assert isinstance(label, NavigableString)
    result = re.search("^Last import: (.*)", label)
    assert result
    date = result.group(1)
    assert date == "Never"


@mark.django_db
@freeze_time(datetime.datetime(2034, 7, 6, 4, 4, 3))
def test_applications_import_button_label_shows_relative_time(
    rf: RequestFactory,
):
    ApplicationFactory()

    last_import = timezone.now() - timedelta(hours=1)
    Import.objects.create(time=last_import)

    request = rf.get("ignored")
    request.user = UserFactory.create(view_permissions=ApplicationListView)
    response = ApplicationListView.as_view()(request)

    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")
    form = html.find("form", method="post", action=reverse("application_list"))
    assert isinstance(form, Tag)

    label_element = form.find("label")
    assert isinstance(label_element, Tag)
    label = label_element.find(string=re.compile("^Last import: (.*)"))
    assert isinstance(label, str)

    result = re.search("^Last import: (.*)", label)
    assert result
    date = result.group(1)
    assert date == "an hour ago"


@mark.django_db
def test_applications_list_shows_positions(rf: RequestFactory):
    ApplicationFactory(positions=[PositionFactory(name="wizard")])
    request = rf.get("")
    request.user = UserFactory.create(permission="applications.view_application")
    response = ApplicationListView.as_view()(request)
    assert isinstance(response, TemplateResponse)
    assert "wizard" in response.rendered_content


@mark.django_db
def test_search_one_application_match_one(rf: RequestFactory):
    # given application and search param
    search_param = "alon"
    not_search_param = "simon"
    ApplicationFactory(first_name=search_param)
    ApplicationFactory(first_name=not_search_param)

    request = rf.get("", {"search": search_param})

    # wehn an a search is being done
    request.user = UserFactory.create(permission="applications.view_application")
    response = ApplicationListView.as_view()(request)

    # response should contain the search application and not include the opther user
    assert search_param in response.rendered_content
    assert not_search_param not in response.rendered_content


@mark.django_db
def test_search_one_application_match_one_when_position_isexisting(rf: RequestFactory):
    # given application and search param
    search_param = "Ashley"
    ApplicationFactory(first_name=search_param)

    request = rf.get("", {"search": search_param, "position": "test"})

    # when a search is being done
    request.user = UserFactory.create(permission="applications.view_application")
    response = ApplicationListView.as_view()(request)
    # then the response should not contain any result
    assert "There are no open applications at the moment." in response.rendered_content
