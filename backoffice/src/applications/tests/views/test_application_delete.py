from http import HTTPStatus

from bs4 import BeautifulSoup
from bs4.element import Tag
from django.core.exceptions import PermissionDenied
from django.core.mail.message import EmailMessage
from django.http.response import HttpResponseRedirect
from django.template.response import TemplateResponse
from django.test import Client, RequestFactory
from django.test.utils import override_settings
from django.urls import reverse
from pytest import mark, raises

from seawatch_planner.tests.messages import (
    MessagesRequestFactory,
    WSGIRequestWithStubMessages,
)
from seawatch_registration.tests.factories import PositionFactory, UserFactory

from ...models import Application
from ...views import ApplicationDeleteView, ApplicationUpdateView
from ..factories import ApplicationFactory


def mk_request(mrf: MessagesRequestFactory) -> WSGIRequestWithStubMessages:
    request = mrf.post("", {"send_email": "false"})
    request.user = UserFactory(view_permissions=ApplicationDeleteView)
    return request


@mark.django_db
def test_delete_application_reachable(client: Client):
    ApplicationFactory(id=42)
    client.force_login(UserFactory(permission="delete_application"))
    response = client.get(reverse("application_delete", kwargs={"pk": 42}))

    assert response.status_code == 200


@mark.django_db
def test_delete_application_confirmation(rf: RequestFactory):
    ApplicationFactory(id=24, first_name="Cayce", last_name="Pollard")

    request = rf.get("")
    request.user = UserFactory(
        permissions=["applications.view_application", "applications.delete_application"]
    )

    response = ApplicationDeleteView.as_view()(request, pk=24)
    assert isinstance(response, TemplateResponse)
    assert "Cayce Pollard's application" in response.rendered_content


@mark.django_db
def test_delete_application(mrf: MessagesRequestFactory):
    ApplicationFactory(id=9123)

    request = mk_request(mrf)

    response = ApplicationDeleteView.as_view()(request, pk=9123)

    assert isinstance(response, HttpResponseRedirect)
    assert response.url == reverse("application_list")
    assert Application.objects.filter(id=9123).count() == 0


@mark.django_db
def test_delete_application_requires_permisson(rf: RequestFactory):
    request = rf.post("")
    request.user = UserFactory()

    with raises(PermissionDenied):
        ApplicationDeleteView.as_view()(request, pk=5)


@mark.django_db
def test_delete_application_from_details(mrf: MessagesRequestFactory):
    ApplicationFactory(id=4332)

    request = mrf.get("")
    request.user = UserFactory(
        view_permissions=[ApplicationDeleteView, ApplicationUpdateView]
    )

    response = ApplicationUpdateView.as_view()(request, pk=4332)
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    delete_button = html.find("button", id="deleteApplication")
    assert isinstance(delete_button, Tag)
    assert delete_button.string is not None
    assert delete_button.string.strip() == "Delete application"


@mark.django_db
def test_delete_application_shows_message(mrf: MessagesRequestFactory):
    application = ApplicationFactory(id=9347)

    request = mk_request(mrf)
    ApplicationDeleteView.as_view()(request, pk=9347)

    messages = request._messages.messages
    assert messages == [
        (
            20,
            f"{application.first_name}'s application has been deleted.",
            "",
        )
    ]


@mark.django_db
@mark.parametrize("send_email,expect_email", [("true", True), ("false", False)])
def test_delete_sends_email_iff_specified_in_request(
    send_email: str,
    expect_email: bool,
    mrf: MessagesRequestFactory,
    mailoutbox: list[EmailMessage],
):
    application = ApplicationFactory(
        positions=[PositionFactory(name="Foo"), PositionFactory(name="Bar")]
    )

    request = mrf.post("", {"send_email": send_email})
    request.user = UserFactory(view_permissions=ApplicationDeleteView)

    from_email = "test-external-from@example.com"

    with override_settings(EXTERNAL_EMAIL_FROM=from_email):
        ApplicationDeleteView.as_view()(request, pk=application.id)

    if expect_email:
        assert len(mailoutbox) == 1
        m = mailoutbox[0]
        assert "Your application for" in m.subject
        assert "cannot accept you into our" in m.body
        assert "positions of Bar and Foo" in m.body
        assert m.to == [application.email]
        assert m.from_email == from_email
    else:
        assert len(mailoutbox) == 0


@mark.django_db
@mark.parametrize("data", [None, {"foo": "bar"}, {"send_email": "whatever"}])
def test_returns_bad_request_if_unspecified_whether_to_send_mail(
    data, mrf: MessagesRequestFactory
):
    request = mrf.post("", data)
    request.user = UserFactory(view_permissions=ApplicationDeleteView)

    response = ApplicationDeleteView.as_view()(request, pk=42)

    assert HTTPStatus(response.status_code) == HTTPStatus.BAD_REQUEST


@mark.django_db
def test_doesnt_delete_on_mail_send_error(mrf: MessagesRequestFactory):
    a = ApplicationFactory()

    request = mrf.post("", {"send_email": "true"})
    request.user = UserFactory(view_permissions=ApplicationDeleteView)

    exception = Exception("mocked send_mail failure")

    def send_mail_mock(*a, **k):
        raise exception

    with raises(Exception) as exc_info:
        ApplicationDeleteView.as_view(send_mail=send_mail_mock)(request, pk=a.id)

    assert exc_info.value == exception
    assert Application.objects.filter(id=a.id).exists()


@mark.django_db
def test_only_sends_mail_for_positions_which_are_open_for_application(
    mrf: MessagesRequestFactory,
    mailoutbox: list[EmailMessage],
):
    # GIVEN
    application = ApplicationFactory(
        positions=[
            PositionFactory(name="The Open One", open_for_applications=True),
            PositionFactory(name="The Additional One", open_for_applications=False),
        ]
    )

    # WHEN
    request = mrf.post("", {"send_email": "true"})
    request.user = UserFactory(view_permissions=ApplicationDeleteView)
    ApplicationDeleteView.as_view()(request, pk=application.id)

    # THEN
    body = mailoutbox[0].body
    assert "The Open One" in body
    assert "The Additional One" not in body


@mark.django_db
def test_dialog_only_shows_preview_for_positions_which_are_open_for_application(
    mrf: MessagesRequestFactory,
):
    # GIVEN
    application = ApplicationFactory(
        positions=[
            PositionFactory(name="The Open One", open_for_applications=True),
            PositionFactory(name="The Additional One", open_for_applications=False),
        ]
    )

    # WHEN
    request = mrf.get("")
    request.user = UserFactory(view_permissions=ApplicationDeleteView)
    response = ApplicationDeleteView.as_view()(request, pk=application.id)

    # THEN
    assert isinstance(response, TemplateResponse)
    body = response.rendered_content
    assert "The Open One" in body
    assert "The Additional One" not in body
