import datetime
import json
from typing import cast

from bs4 import BeautifulSoup
from bs4.element import Tag
from django.core.exceptions import PermissionDenied
from django.template.response import TemplateResponse
from django.test import Client, RequestFactory
from django.urls import reverse
from planner_common.models import Gender
from pytest import mark, raises
from pytest_django.asserts import assertContains

from seawatch_planner.tests.messages import MessagesRequestFactory
from seawatch_registration.tests.factories import PositionFactory, UserFactory

from ...views import ApplicationListView, ApplicationUpdateView
from ..factories import ApplicationFactory


@mark.django_db
def test_requires_permissions(rf: RequestFactory):
    ApplicationFactory(id=123)
    request = rf.get("/applications/ignored")
    request.user = UserFactory.create()

    with raises(PermissionDenied):
        ApplicationUpdateView.as_view()(request, pk=123)


@mark.django_db
def test_reachable(client: Client):
    ApplicationFactory(id=1234)
    client.force_login(UserFactory.create(permission="view_application"))
    response = client.get(reverse("application_update", kwargs={"pk": 1234}))
    assert response.status_code == 200


@mark.django_db
def test_linked_from_list(rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory.create(permission="view_application")

    ApplicationFactory(id=7842, first_name="Molly", last_name="Millions")

    response = ApplicationListView.as_view()(request)
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    assert html.find(
        "a",
        href=reverse("application_update", kwargs={"pk": 7842}),
        string=lambda s: s.strip() == "Millions, Molly",
    )


@mark.django_db
def test_shows_fields(rf: RequestFactory):
    ApplicationFactory(
        id=1764,
        first_name="Peter",
        email="ghjkmail@test.com",
        phone_number="999888777",
        motivation="Motivation Oh yeah!",
        qualification="Qualification Seriously!",
        date_of_birth=datetime.date(2000, 1, 1),
        positions__keys=[
            "master",
            "bosun",
        ],
        nationalities=["AX", "PL", "IT"],
        gender=Gender.NONE_OTHER,
        languages={"deu": 2, "eng": 1},
    )

    request = rf.get("/applications/ignored")
    request.user = UserFactory.create(permission="view_application")

    response = ApplicationUpdateView.as_view()(request, pk=1764)
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    assertContains(response, "Peter")
    assertContains(response, "ghjkmail@test.com")
    assertContains(response, "999888777")
    assertContains(response, "Motivation Oh yeah!")
    assertContains(response, "Qualification Seriously!")
    assertContains(response, "2000-01-01")

    nationalities_select = html.find("select", attrs={"name": "nationalities"})
    assert isinstance(nationalities_select, Tag)
    assert {
        (t["value"], t.text)
        for t in nationalities_select.find_all("option", selected=True)
    } == {("AX", "Åland Islands"), ("IT", "Italy"), ("PL", "Poland")}

    languages_select = html.find("select", attrs={"name": "languages"})
    assert isinstance(languages_select, Tag)
    assert {
        (t["value"], t.text) for t in languages_select.find_all("option", selected=True)
    } == {("deu", "German"), ("eng", "English")}
    languages_data = html.find("input", attrs={"name": "languages__data"})
    assert isinstance(languages_data, Tag)
    assert json.loads(cast(str, languages_data["value"])) == [
        {"language": "deu", "points": 2},
        {"language": "eng", "points": 1},
    ]

    assertContains(response, "None/Other")


@mark.django_db
def test_creates_history_item(mrf: MessagesRequestFactory):
    a = ApplicationFactory.create(last_name="Kirk")

    request = mrf.get("")
    request.user = UserFactory.create(permission="view_application")
    form_response = ApplicationUpdateView.as_view()(request, pk=a.id)
    form = BeautifulSoup(
        cast(TemplateResponse, form_response).rendered_content,
        features="html.parser",
    ).find("form")
    assert isinstance(form, Tag)

    original_data = {
        **{
            e["name"]: e["value"]
            for e in form.find_all("input")
            if e.has_attr("value") and e.has_attr("name")
        },
        **{
            s["name"]: [o["value"] for o in s.find_all("option", selected=True)]
            for s in form.find_all("select")
            if s.has_attr("name")
        },
    }

    request = mrf.post("", {**original_data, "last_name": "Spock"})
    request.user = UserFactory.create(permission="view_application")

    response = ApplicationUpdateView.as_view()(request, pk=a.id)
    assert response.status_code == 302

    h = a.history.last()
    assert h.change_json == [{"field": "last_name", "old": "Kirk", "new": "Spock"}]


@mark.django_db
def test_shows_only_open_for_application_positions_in_reject_email_preview(
    mrf: MessagesRequestFactory,
):
    application = ApplicationFactory(
        positions=[
            PositionFactory(name="The Open One", open_for_applications=True),
            PositionFactory(name="The Additional One", open_for_applications=False),
        ]
    )

    request = mrf.get("")
    request.user = UserFactory.create(permission="view_application")
    form_response = ApplicationUpdateView.as_view()(request, pk=application.id)

    dialog = BeautifulSoup(
        cast(TemplateResponse, form_response).rendered_content,
        features="html.parser",
    ).find("dialog", id="deleteApplicationDialog")

    assert isinstance(dialog, Tag)
    assert "The Open One" in dialog.text
    assert "The Additional One" not in dialog.text
