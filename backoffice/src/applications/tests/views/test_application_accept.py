import logging
from datetime import date, datetime, timezone
from http import HTTPStatus
from typing import Any, cast

from bs4 import BeautifulSoup
from bs4.element import Tag
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.core.mail.message import EmailMessage
from django.http.response import HttpResponseRedirect
from django.template.response import TemplateResponse
from django.test import Client, RequestFactory, override_settings
from django.urls import reverse
from django.utils.timezone import utc
from freezegun.api import freeze_time
from planner_common.models import Gender
from pytest import LogCaptureFixture, mark, raises
from pytest_django.asserts import assertRedirects

from seawatch_planner.tests.messages import MessagesRequestFactory
from seawatch_registration.tests.factories import PositionFactory, UserFactory
from volunteers.models import Volunteer

from ...models import PositionDecision
from ...views import ApplicationAcceptView, ApplicationUpdateView
from ..factories import ApplicationFactory


@mark.django_db
def test_confirmation_page(mrf: MessagesRequestFactory):
    application = ApplicationFactory(
        first_name="Paul",
        last_name="Paulson",
        positions=[
            PositionFactory(key="1", name="Foo"),
            PositionFactory(key="2", name="Bar"),
        ],
    )
    application.position_decisions.all().update(
        decision=PositionDecision.Decision.ACCEPT
    )
    request = mrf.get("", pk=application.id)
    request.user = UserFactory(view_permissions=ApplicationAcceptView)

    response = ApplicationAcceptView.as_view()(request, pk=application.id)

    assert response.status_code == 200
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")
    assert "Application from Paul Paulson" in html.text
    assert "This will create a volunteer profile" in html.text

    form = html.find("form", method="post", action=None)
    assert isinstance(form, Tag)
    submit = form.find("input", type="submit")
    assert isinstance(submit, Tag)
    assert submit.attrs["value"] == "Create profile as foo and bar"


@mark.django_db
def test_creates_matching_profile(
    mrf: MessagesRequestFactory, mailoutbox: list[EmailMessage]
):
    phone = "+49877492099"
    email = "blub@foo.test"
    positions = set([PositionFactory(name="farmer"), PositionFactory(name="gardener")])
    certificates = ["certA", "certB"]
    languages = {"de": 2, "fr": 5}
    gender = Gender.PREFER_NOT_SAY
    date_of_birth = date(2001, 9, 13)
    nationalities = ["jp", "se"]
    application = ApplicationFactory(
        id=7391,
        first_name="Yours",
        last_name="Truly",
        date_of_birth=date_of_birth,
        gender=gender,
        phone_number=phone,
        nationalities=nationalities,
        email=email,
        positions=positions,
        positions__ready_to_accept=True,
        certificates=certificates,
        languages=languages,
    )

    request = mrf.post("")
    request.user = UserFactory(permissions=["volunteers.add_volunteer"])

    frozen_time = datetime(2002, 12, 24, 18, tzinfo=timezone.utc)
    with freeze_time(frozen_time):
        response = ApplicationAcceptView.as_view()(request, pk=7391)

    assert isinstance(response, HttpResponseRedirect)
    assert response.url == reverse("application_list")
    volunteer = Volunteer.objects.get(last_name="Truly")

    assert volunteer.name == "Yours Truly"

    assert volunteer.phone_number == phone
    assert volunteer.email == email

    assert volunteer.date_of_birth == date_of_birth
    assert volunteer.gender == gender
    assert volunteer.nationalities == nationalities

    assert volunteer.languages == languages

    assert volunteer.certificates == certificates

    application.refresh_from_db()
    assert application.accepted == frozen_time


@mark.django_db
def test_sends_email(mrf: MessagesRequestFactory, mailoutbox: list[EmailMessage]):
    email = "foo@example.com"
    application = ApplicationFactory(email=email, positions__ready_to_accept=True)

    request = mrf.post("")
    request.user = UserFactory(view_permissions=ApplicationAcceptView)

    from_email = "test-external-from@example.com"

    with override_settings(EXTERNAL_EMAIL_FROM=from_email):
        response = ApplicationAcceptView.as_view()(request, pk=application.id)
    assert HTTPStatus(response.status_code) == HTTPStatus.FOUND

    assert len(mailoutbox) == 1
    mail = mailoutbox[0]
    assert mail.to == [email]
    assert mail.from_email == from_email
    assert "Accepted" in mail.subject
    assert "you are now part of our crewing pool" in mail.body


@mark.django_db
def test_accepts_and_shows_error_if_sending_mail_fails(
    mrf: MessagesRequestFactory,
    mailoutbox: list[EmailMessage],
    caplog: LogCaptureFixture,
):
    email = "foo@example.com"
    application = ApplicationFactory(email=email, positions__ready_to_accept=True)

    request = mrf.post("")
    request.user = UserFactory(view_permissions=ApplicationAcceptView)

    exception = Exception("mock error")

    def mock_send_mail(*a, **k):
        raise exception

    response = ApplicationAcceptView.as_view(send_mail=mock_send_mail)(
        request, pk=application.id
    )

    assert HTTPStatus(response.status_code) == HTTPStatus.FOUND
    assert (
        messages.ERROR,
        (
            "There was an error sending the mail. Please notify the applicant yourself.\n"
            "We have already been notified about this and will look into the cause as soon "
            "as possible. Sorry for the inconvenience."
        ),
        "",
    ) in request._messages.messages

    assert len(caplog.records) == 1
    rec = caplog.records[0]
    assert rec.exc_info
    assert rec.exc_info[1] == exception
    assert rec.levelno == logging.ERROR
    assert rec.message == "Error sending acceptance email"
    assert cast(Any, rec).request == request


@mark.django_db
def test_creates_history_item(mrf: MessagesRequestFactory):
    application = ApplicationFactory(positions__ready_to_accept=True)
    before_count = application.history.count()

    request = mrf.post("")
    request.user = UserFactory(permissions=["volunteers.add_volunteer"])

    frozen_time = datetime(2013, 4, 13, 9, 15, tzinfo=utc)

    with freeze_time(frozen_time):
        ApplicationAcceptView.as_view()(request, application.id)

    assert application.history.count() == before_count + 1
    assert application.history.last().change_json == [
        {"field": "accepted", "old": None, "new": frozen_time.isoformat()}
    ]


@mark.django_db
def test_reachable(client: Client):
    application = ApplicationFactory(positions__ready_to_accept=True)

    client.force_login(
        UserFactory(
            permissions=[
                "volunteers.add_volunteer",
            ]
        )
    )
    response = client.post(reverse("application_accept", kwargs={"pk": application.id}))
    assertRedirects(
        response, reverse("application_list"), fetch_redirect_response=False
    )


@mark.django_db
def test_accept_requires_permissions(mrf: MessagesRequestFactory):
    # Missing "add_volunteer"
    ApplicationFactory(id=28)
    request = mrf.post("")
    request.user = UserFactory()

    with raises(PermissionDenied):
        ApplicationAcceptView.as_view()(request, pk=28)


@mark.django_db
def test_from_details_page(rf: RequestFactory):
    ApplicationFactory(id=812)

    request = rf.get("")
    request.user = UserFactory(permission="applications.view_application")

    response = ApplicationUpdateView.as_view()(request, pk=812)
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    form = html.find(
        "form", method="post", action=reverse("application_accept", kwargs={"pk": 812})
    )
    assert form
    assert form.find("input", attrs={"name": "csrfmiddlewaretoken"})  # type: ignore
    assert html.find("button", id="acceptApplication")


@mark.django_db
def test_from_details_page_generates_info_message(
    mrf: MessagesRequestFactory,
):
    application = ApplicationFactory(positions__ready_to_accept=True)
    request = mrf.post("")
    request.user = UserFactory(permissions=["volunteers.add_volunteer"])
    response = ApplicationAcceptView.as_view()(request, pk=application.id)
    assert isinstance(response, HttpResponseRedirect)
    assert response.url == reverse("application_list")
    messages = request._messages.messages
    assert messages == [
        (
            20,
            f"Application accepted, {application.first_name} is now a Volunteer!",
            "",
        )
    ]


@mark.django_db
@mark.parametrize("method", ["get", "post"])
def test_redirect_with_message_if_not_all_positions_are_decided(
    method, mrf: MessagesRequestFactory, admin_user, caplog
):
    application = ApplicationFactory(positions=[PositionFactory()])
    application.position_decisions.all().update(decision=PositionDecision.Decision.OPEN)

    request = getattr(mrf, method)("")
    request.user = admin_user

    response = ApplicationAcceptView.as_view()(request, pk=application.pk)

    assert not Volunteer.objects.filter(application=application).exists()
    assert HTTPStatus(response.status_code) == HTTPStatus.FOUND
    assert (
        messages.WARNING,
        "Cannot accept applications before all positions have been decided",
        "",
    ) in request._messages.messages
    assert len(caplog.records) == 1
    assert caplog.records[0].levelno == logging.WARNING
    assert caplog.records[0].request == request
