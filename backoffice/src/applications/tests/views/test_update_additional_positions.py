from http import HTTPStatus
from typing import cast

from _pytest.python_api import raises
from bs4 import BeautifulSoup
from bs4.element import Tag
from django.core.exceptions import PermissionDenied
from django.template.response import TemplateResponse
from django.test.client import Client
from pytest import mark

from applications.tests.factories import ApplicationFactory
from applications.views import update_additional_positions
from seawatch_planner.tests.messages import MessagesRequestFactory
from seawatch_registration.tests.factories import PositionFactory, UserFactory

required_permissions = ["applications.change_application"]


@mark.django_db
def test_is_reachable_and_requires_post(client: Client):
    client.force_login(UserFactory(permissions=required_permissions))
    response = client.get("/applications/42/update_additional_positions/")

    assert HTTPStatus(response.status_code) == HTTPStatus.METHOD_NOT_ALLOWED


@mark.django_db
def test_requires_permission(mrf: MessagesRequestFactory):
    request = mrf.get("")  # permissions are more important than http method

    request.user = UserFactory()  # No permissions

    with raises(PermissionDenied):
        update_additional_positions(request, pk=1323)


@mark.django_db
def test_saves_the_position(mrf: MessagesRequestFactory):
    application = ApplicationFactory(
        positions=[
            PositionFactory(name="regular", open_for_applications=True),
            PositionFactory(name="to_be_removed", open_for_applications=False),
        ]
    )
    additional_position = PositionFactory(
        name="to_be_added", open_for_applications=False
    )

    request = mrf.post("", {"additional_positions": str(additional_position.id)})
    request.user = UserFactory(permissions=required_permissions)

    response = update_additional_positions(request, pk=application.id)
    assert HTTPStatus(response.status_code) == HTTPStatus.OK

    assert {position.key for position in application.positions.all()} == {
        "regular",
        "to_be_added",
    }


@mark.django_db
def test_returns_position_selects(mrf: MessagesRequestFactory):
    application = ApplicationFactory(
        positions=[PositionFactory(key=k) for k in ("first", "second")]
    )

    request = mrf.post("")
    request.user = UserFactory(permissions=required_permissions)

    response = update_additional_positions(request, pk=application.pk)
    html = BeautifulSoup(
        cast(TemplateResponse, response).rendered_content, features="html.parser"
    )

    assert {select["id"] for select in cast(list[Tag], html.find_all("select"))} == {
        "position-decision-first",
        "position-decision-second",
    }
