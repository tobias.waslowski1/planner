import datetime
import re
from http import HTTPStatus

from _pytest.python_api import raises
from django.http.response import Http404
from django.test import Client, RequestFactory
from django.utils import timezone
from freezegun import freeze_time
from pytest import mark
from pytest_django.asserts import assertContains

from seawatch_registration.tests.factories import UserFactory

from ...views import add_comment
from ..factories import ApplicationFactory


@mark.django_db
def test_is_reachable(client: Client):
    client.force_login(UserFactory.create())
    application = ApplicationFactory()
    response = client.get(f"/applications/{application.id}/add_comment/")
    assert HTTPStatus(response.status_code) == HTTPStatus.METHOD_NOT_ALLOWED


@mark.django_db
@freeze_time("2022-03-05T18:15:47")
def test_post_response(rf: RequestFactory):
    application = ApplicationFactory()
    data = {"comment": "great application"}

    author = UserFactory()
    request = rf.post("", data)
    request.user = author

    response = add_comment(request, application.id)

    assert HTTPStatus(response.status_code) == HTTPStatus.OK

    assertContains(response, data["comment"])
    assertContains(response, author.username)
    assertContains(response, "05.03.2022, 18:15")


@mark.django_db
@freeze_time("2022-03-05T18:15:47")
def test_write_to_database(rf: RequestFactory):
    # UserFactory
    application = ApplicationFactory()
    data = {"comment": "great application"}

    author = UserFactory()
    request = rf.post("", data)
    request.user = author

    add_comment(request, application.id)

    # Assert that the comment is saved in the database
    application.refresh_from_db()
    comment = application.comments.get()
    assert comment.text == "great application"
    assert comment.date == datetime.datetime(
        2022, 3, 5, 18, 15, 47, tzinfo=timezone.utc
    )
    assert comment.author == author


@mark.django_db
def test_returns_bad_request_if_missing_comment_value(rf: RequestFactory):
    data = {}  # no "comment" entry

    request = rf.post("", data)
    request.user = UserFactory()

    response = add_comment(request, ApplicationFactory().id)
    assert HTTPStatus(response.status_code) == HTTPStatus.BAD_REQUEST


@mark.django_db
def test_returns_not_found_if_application_does_not_exist(rf: RequestFactory):
    with raises(Http404):
        add_comment(rf.post("", {}), "4223")
