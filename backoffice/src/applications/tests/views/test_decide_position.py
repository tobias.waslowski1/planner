from datetime import datetime
from http import HTTPStatus
from typing import cast

from bs4 import BeautifulSoup
from django.core.exceptions import PermissionDenied
from django.template.response import TemplateResponse
from django.test.client import Client
from django.utils.timezone import utc
from freezegun.api import freeze_time
from pytest import mark, raises

from applications.models import PositionDecision
from applications.tests.factories import ApplicationFactory
from applications.views import decide_position
from seawatch_planner.tests.messages import MessagesRequestFactory
from seawatch_registration.tests.factories import PositionFactory, UserFactory

required_permissions = ["applications.change_application"]


@mark.django_db
def test_is_reachable(client: Client):
    client.force_login(UserFactory(permissions=required_permissions))

    assert (
        HTTPStatus(
            client.post(
                "/applications/42/decide_position/",
                data={"position": "foo", "decision": PositionDecision.Decision.REJECT},
            ).status_code
        )
        == HTTPStatus.NOT_FOUND
    )


@mark.django_db
def test_requires_post(mrf: MessagesRequestFactory):
    request = mrf.get("")
    request.user = UserFactory(permissions=required_permissions)

    assert (
        HTTPStatus(decide_position(request, "42").status_code)
        == HTTPStatus.METHOD_NOT_ALLOWED
    )


@mark.django_db
def test_stores_decision(mrf: MessagesRequestFactory):
    position_key = "mediator"
    decision = PositionDecision.Decision.REJECT

    position = PositionFactory(key=position_key)
    application = ApplicationFactory(positions=[position, PositionFactory(key="foo")])

    request = mrf.post(
        "",
        data={"position": position_key, "decision": decision},
    )
    request.user = UserFactory(permissions=required_permissions)
    time = datetime(2010, 5, 3, 18, tzinfo=utc)
    with freeze_time(time):
        decide_position(request, application.pk)

    application.refresh_from_db()
    # We didn't touch that one
    assert (
        application.position_decisions.get(position__key="foo").decision
        == PositionDecision.Decision.OPEN
    )

    pd = application.position_decisions.get(position__key=position_key)
    assert pd.decision == decision
    assert pd.by == request.user
    assert pd.time == time


@mark.django_db
@freeze_time("2022-09-01T13:00")
def test_renders_last_updated(mrf: MessagesRequestFactory):
    application = ApplicationFactory(positions=[PositionFactory(key="gremlin")])

    request = mrf.post(
        "", data={"position": "gremlin", "decision": PositionDecision.Decision.ACCEPT}
    )
    request.user = UserFactory(
        username="personMcExampleName", permissions=required_permissions
    )
    response = decide_position(request, application.id)

    assert HTTPStatus(response.status_code) == HTTPStatus.OK
    assert (
        "Updated 01.09.2022 by personMcExampleName"
        in cast(TemplateResponse, response).rendered_content
    )


@mark.django_db
def test_requires_permission(mrf: MessagesRequestFactory):
    request = mrf.get("")  # permissions are more important than http method

    request.user = UserFactory()  # No permissions

    with raises(PermissionDenied):
        decide_position(request, "1323")


@mark.django_db
def test_pad_request_on_invalid_POST(mrf: MessagesRequestFactory):
    request = mrf.post("", data={"yooo": "lmao"})
    request.user = UserFactory(permissions=required_permissions)

    # note that in particular, this is prioritized over 404. We verify inputs first and fetch data
    # second.
    response = decide_position(request, "124")

    assert HTTPStatus(response.status_code) == HTTPStatus.BAD_REQUEST


@mark.django_db
def test_404_on_nonexisting_position(mrf: MessagesRequestFactory):
    request = mrf.post(
        "", data={"position": "magus", "decision": PositionDecision.Decision.ACCEPT}
    )
    request.user = UserFactory(permissions=required_permissions)

    application = ApplicationFactory(positions=[PositionFactory(key="witch")])

    response = decide_position(request, application.id)

    assert HTTPStatus(response.status_code) == HTTPStatus.NOT_FOUND
