import json
from typing import cast

from bs4 import BeautifulSoup
from bs4.element import Tag
from django.http.request import QueryDict
from django.template.response import TemplateResponse
from planner_common import features
from pytest import mark

from applications.forms import ApplicationForm
from applications.models import certificates_separator, nationalities_separator
from applications.tests.factories import ApplicationFactory
from applications.views import ApplicationUpdateView
from seawatch_planner.tests.messages import MessagesRequestFactory
from seawatch_registration.tests.factories import PositionFactory, UserFactory

if features.additional_positions:

    @mark.django_db
    def test_shows_additional_positions_separately(mrf: MessagesRequestFactory):
        external_position = PositionFactory(key="ext", open_for_applications=True)
        additional_position = PositionFactory(key="int", open_for_applications=False)
        a = ApplicationFactory(positions=[external_position, additional_position])

        request = mrf.get("")
        request.user = UserFactory(view_permissions=ApplicationUpdateView)

        response = ApplicationUpdateView.as_view()(request, pk=a.id)
        html = BeautifulSoup(
            cast(TemplateResponse, response).rendered_content, features="html.parser"
        )

        positions = html.find("select", id="id_positions")
        assert {
            o["value"] for o in cast(Tag, positions).find_all("option", selected=True)
        } == {str(external_position.id)}

        additional_positions = html.find("select", id="id_additional_positions")
        assert {
            o["value"]
            for o in cast(Tag, additional_positions).find_all("option", selected=True)
        } == {str(additional_position.id)}

    @mark.django_db
    def test_preserves_additional_positions():
        should_be_removed = PositionFactory(name="removed", open_for_applications=True)
        should_be_added = PositionFactory(name="added", open_for_applications=True)
        should_be_kept = PositionFactory(name="kept", open_for_applications=False)
        application = ApplicationFactory(positions=[should_be_removed, should_be_kept])

        # Use the form to get the data, this should decouple us somewhat from changes in the data layout
        data = QueryDict("", mutable=True)
        data.update(
            {
                k: v
                for k, v in ApplicationForm(instance=application).initial.items()
                if k != "additional_positions"
            }
        )
        data["positions"] = str(should_be_added.id)
        data["languages__data"] = json.dumps([{"language": "eng", "points": 2}])
        data.setlist(
            "nationalities", data["nationalities"].split(nationalities_separator)
        )
        data.setlist(
            "certificates_csv",
            data["certificates_csv"].split(certificates_separator)
            if data["certificates_csv"]
            else [],
        )

        form = ApplicationForm(
            instance=application,
            data=data,
        )
        assert form.is_valid()
        form.save()

        application.refresh_from_db()

        assert {p.name for p in application.positions.all()} == {"added", "kept"}
