import json
import re

from bs4 import BeautifulSoup
from bs4.element import Tag
from django.forms.forms import Form
from pytest import fixture, mark, raises

from applications.forms import LanguagesField


class ExampleForm(Form):
    languages = LanguagesField()


class TestRendering:
    data = [
        {"language": "deu", "points": 3},
        {"language": "ara-levantine", "points": 1},
    ]

    @fixture(scope="module")
    def form(self):
        return ExampleForm(initial={"languages": self.data})

    @fixture(scope="module")
    def html(self, form):
        # ignore the template and the javascript
        return BeautifulSoup(str(form), features="html.parser").find(
            class_="language-mapper"
        )

    def test_renders_languages_to_hidden_input(self, html):
        input = html.find(attrs={"name": "languages__data"})
        assert isinstance(input, Tag)
        assert json.loads(input.attrs["value"]) == self.data

    def test_renders_options(self, html):
        rendered_options = set(
            f"{e.attrs['value']}|{e.text}" for e in html.find_all("option")
        )
        assert "som|Somali" in rendered_options
        assert "arb|Modern Standard Arabic" in rendered_options

    def test_renders_selected_options(self, html):
        assert set(
            e.attrs["value"] for e in html.find_all("option", selected=True)
        ) == {"deu", "ara-levantine"}


def test_retrieves_value_from_data():
    data = [{"language": "som", "points": 0}]
    f = ExampleForm(data={"languages__data": json.dumps(data)})
    f.full_clean()
    assert "languages" in f.cleaned_data
    assert f.cleaned_data["languages"] == data


@mark.parametrize(
    "d, msg",
    [
        ("not a list", "Expected list"),
        (["not a dict"], "Expected dict"),
        ([{"wrong": "keys"}], "Expected dict with language and points"),
        ([{"language": "wrong", "points": 0}], 'Unknown language "wrong"'),
        ([{"language": "deu", "points": 42}], "Expected points to be between 0 and 3"),
    ],
)
def test_invalid_value_detection(d, msg):
    with raises(Exception, match=re.escape(msg)):
        ExampleForm(data={"languages__data": json.dumps(d)}).full_clean()
