from typing import cast

from bs4 import BeautifulSoup
from bs4.element import Tag
from django.template.response import TemplateResponse
from pytest import mark

from applications.tests.factories import ApplicationFactory
from applications.views import ApplicationAcceptView, ApplicationDeleteView
from seawatch_planner.tests.messages import MessagesRequestFactory
from seawatch_registration.tests.factories import UserFactory


@mark.django_db
def test_deleting_via_delete_page_works(mrf: MessagesRequestFactory):
    application = ApplicationFactory()

    request = mrf.get("")
    request.user = UserFactory(view_permissions=ApplicationDeleteView)

    response = ApplicationDeleteView.as_view()(request, pk=application.id)

    html = BeautifulSoup(
        cast(TemplateResponse, response).rendered_content, features="html.parser"
    )

    forms = html.find_all("form")
    for form in forms:
        assert isinstance(form, Tag)
        send_form_input = form.find("input", attrs={"name": "send_email"})
        assert isinstance(send_form_input, Tag)
        assert send_form_input["value"] in {"true", "false"}


@mark.django_db
def test_accept_application_view_doesnt_show_messages(mrf: MessagesRequestFactory):
    application = ApplicationFactory(positions__ready_to_accept=True)

    request = mrf.get("")
    request.user = UserFactory(view_permissions=ApplicationAcceptView)

    ApplicationAcceptView.as_view()(request, pk=application.id)

    assert request._messages.messages == []
