import random
from datetime import timezone
from typing import Callable, ClassVar, List, Optional
from uuid import uuid4

from factory import (
    Factory,
    Faker,
    LazyAttribute,
    LazyFunction,
    SubFactory,
    post_generation,
)
from factory.django import DjangoModelFactory
from faker.providers import BaseProvider
from phonenumber_field.phonenumber import PhoneNumber
from planner_common.models import Gender
from planner_common.test.factories import CertificatesListFactory, TypedFactoryMetaClass

from applications.models import certificates_separator
from seawatch_registration.models import Position
from seawatch_registration.tests.factories import UserFactory

from .. import models
from ..models import (
    Application,
    Comment,
    MalformedApplication,
    PositionDecision,
    nationalities_separator,
)
from ..service import ApplicationDTO, Language


class ApplicationFactory(
    DjangoModelFactory, metaclass=TypedFactoryMetaClass[Application]
):
    class Meta:
        model = Application
        rename = {"nationalities_csv": "nationalities"}

    create: ClassVar[Callable[..., Application]]
    build: ClassVar[Callable[..., Application]]

    class Params:
        # The seed is used so we only depend on the randomness from factory_boy
        positions__seed = Faker("random_int")
        positions__count = Faker("random_int", min=1, max=3)
        positions__ready_to_accept: Optional[bool] = None
        certificates = SubFactory(CertificatesListFactory)
        nationalities = sorted(["GB", "IT", "AU"])
        phone_number_tail = Faker("numerify", text="#######")

    first_name = Faker("first_name")
    last_name = Faker("last_name")
    phone_number = LazyAttribute(
        lambda s: PhoneNumber.from_string(f"+49 170 {s.phone_number_tail}")
    )
    email = Faker("ascii_email")
    date_of_birth = Faker("date_between", start_date="-78y", end_date="-18y")
    gender = Faker("random_element", elements=Gender.values)
    certificates_csv = LazyAttribute(
        lambda s: certificates_separator.join(s.certificates)
    )
    nationalities_csv = LazyAttribute(
        lambda obj: nationalities_separator.join(obj.nationalities)
    )
    created = Faker("past_datetime", tzinfo=timezone.utc)
    motivation = Faker("text", max_nb_chars=500)
    qualification = Faker("text", max_nb_chars=500)

    @post_generation
    def positions(obj, create, positions, **kwargs):
        if create:
            if positions is None:
                if "keys" in kwargs:
                    positions = [
                        Position.objects.get(key=key) for key in kwargs["keys"]
                    ]
                else:
                    all_positions = list(Position.objects.all())
                    positions = random.Random(kwargs["seed"]).sample(
                        all_positions, min(len(all_positions), kwargs["count"])
                    )
            obj.positions.set(positions)

            if kwargs["ready_to_accept"]:
                decisions = [
                    d
                    for d in PositionDecision.Decision
                    if d != PositionDecision.Decision.OPEN
                ]
                pds = obj.position_decisions.all()
                for pd in pds:
                    pd.decision = random.choice(decisions)

                # make sure we accept at least one of them
                if not any(
                    pd.decision == PositionDecision.Decision.ACCEPT for pd in pds
                ):
                    pds[0].decision = PositionDecision.Decision.ACCEPT

                for pd in pds:
                    pd.save()

    @post_generation
    def languages(obj, create, languages: Optional[dict], **kargs):
        if create:
            if languages is None:
                languages = {"eng": 2, "ita": 2, "deu": 1}
            obj.languages.set(
                [
                    models.Language(code=code, points=points)
                    for code, points in languages.items()
                ],
                bulk=False,
            )


class CommentFactory(DjangoModelFactory, metaclass=TypedFactoryMetaClass[Comment]):
    class Meta:
        model = Comment

    application = SubFactory(ApplicationFactory)
    author = SubFactory(UserFactory)
    date = Faker("past_datetime", tzinfo=timezone.utc)
    text = Faker("paragraph")


class LanguageProvider(BaseProvider):
    def language(self):
        return Language(code=self.language_code(), points=self.random_int(min=0, max=3))


Faker.add_provider(LanguageProvider)


class ApplicationDTOFactory(Factory, metaclass=TypedFactoryMetaClass[ApplicationDTO]):
    class Meta:
        model = ApplicationDTO

    create: ClassVar[Callable[..., ApplicationDTO]]
    build: ClassVar[Callable[..., ApplicationDTO]]

    uid = LazyFunction(lambda: str(uuid4()))
    created = Faker("past_datetime", tzinfo=timezone.utc)
    first_name = Faker("first_name")
    last_name = Faker("last_name")
    date_of_birth = Faker("date_between", start_date="-78y", end_date="-18y")
    phone_number = Faker("bothify", text="+############")
    email = Faker("ascii_email")
    spoken_languages = Faker("pylist", value_types=[Language], nb_elements=3)
    positions: List[str] = []
    email = Faker("email")
    motivation = Faker("sentence")
    qualification = Faker("sentence")
    nationalities = ["gb", "it", "au"]
    gender = Faker("random_element", elements=Gender.values)
    certificates = SubFactory(CertificatesListFactory)
    other_certificate = Faker("job")


class MalformedApplicationFactory(
    DjangoModelFactory, metaclass=TypedFactoryMetaClass[MalformedApplication]
):
    class Meta:
        model = MalformedApplication

    create: ClassVar[Callable[..., MalformedApplication]]
    build: ClassVar[Callable[..., MalformedApplication]]

    uid = LazyFunction(lambda: str(uuid4()))
    created = Faker("past_datetime", tzinfo=timezone.utc)
    submitted_data = {}
