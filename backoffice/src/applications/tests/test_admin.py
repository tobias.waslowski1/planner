from bs4 import BeautifulSoup
from django.template.response import TemplateResponse
from django.test import Client
from django.urls import reverse
from pytest import mark

from seawatch_registration.tests.factories import UserFactory

from .factories import ApplicationFactory, MalformedApplicationFactory


@mark.django_db
def test_applications_admin(client: Client):
    client.force_login(UserFactory.create(is_staff=True, permission="view_application"))
    assert (
        client.get(reverse("admin:applications_application_changelist")).status_code
        == 200
    )


@mark.django_db
def test_applicant_name_in_list(client: Client):
    ApplicationFactory(id=814, last_name="Foo", first_name="Bar")
    client.force_login(UserFactory.create(is_staff=True, permission="view_application"))

    response = client.get(reverse("admin:applications_application_changelist"))
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    detailsUrl = reverse(
        "admin:applications_application_change", kwargs={"object_id": 814}
    )
    links = html.find_all("a", href=detailsUrl)
    assert len(links) == 1
    assert links[0].text == "Foo, Bar"


@mark.django_db
def test_malformed_applications_admin(client: Client):
    client.force_login(
        UserFactory.create(is_staff=True, permission="view_malformedapplication")
    )
    assert (
        client.get(
            reverse("admin:applications_malformedapplication_changelist")
        ).status_code
        == 200
    )


@mark.django_db
def test_malformed_is_in_list(client: Client):
    MalformedApplicationFactory(
        id=1312, submitted_data={"name": "Mel", "last_name": "Ford"}
    )
    client.force_login(
        UserFactory.create(is_staff=True, permission="view_malformedapplication")
    )

    response = client.get(reverse("admin:applications_malformedapplication_changelist"))
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    detailsUrl = reverse(
        "admin:applications_malformedapplication_change", kwargs={"object_id": 1312}
    )
    links = html.find_all("a", href=detailsUrl)
    assert len(links) == 1
