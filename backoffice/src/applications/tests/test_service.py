import dataclasses
import logging
import uuid
from datetime import datetime

from jsonschema import ValidationError
from planner_common.test.factories import ApplicationDataFactory
from pytest_httpserver import HTTPServer

from applications.service import ApplicationDTO, import_applications


def test_import_all_applications(settings, httpserver: HTTPServer):
    # given
    settings.PLANNER_PUBLIC_BACKEND_URL = httpserver.url_for("")
    settings.PLANNER_PUBLIC_BACKEND_AUTH_TOKEN = "insecure-preshared-token-for-dev"

    # a default response from public-backend
    httpserver.expect_request("/applications/applications/").respond_with_json(
        {
            "uid": str(uuid.uuid1()),
            "created": str(datetime.now()),
            "data": {
                "first_name": "Harry P.",
                "phone_number": "+3914568179",
                "spoken_languages": [{"language": "deu", "points": 3}],
                "email": "potter@aintamuggel.wz",
                "positions": ["2nd-officer", "chief-eng"],
            },
            "applications": [
                {
                    "uid": str(uuid.uuid1()),
                    "created": str(datetime(2019, 1, 1)),
                    "data": dataclasses.asdict(
                        ApplicationDataFactory(first_name="Harry", certificates=[])
                    ),
                },
                {
                    "uid": str(uuid.uuid1()),
                    "created": str(datetime(2020, 3, 1)),
                    "data": dataclasses.asdict(
                        ApplicationDataFactory(
                            first_name="Ron", certificates=["sea-basic", "driving"]
                        )
                    ),
                },
            ],
        }
    )
    application_dtos = import_applications().dtos

    assert len(application_dtos) == 2
    assert application_dtos[0].first_name == "Harry"
    assert application_dtos[1].first_name == "Ron"
    assert application_dtos[0].certificates == []
    assert application_dtos[1].certificates == ["sea-basic", "driving"]
    assert application_dtos[0].created == str(datetime(2019, 1, 1))
    assert application_dtos[1].created == str(datetime(2020, 3, 1))


def test_import_should_contain_all_fields(settings, httpserver: HTTPServer):
    # given
    settings.PLANNER_PUBLIC_BACKEND_URL = httpserver.url_for("")
    settings.PLANNER_PUBLIC_BACKEND_AUTH_TOKEN = "insecure-preshared-token-for-dev"
    backend_application = ApplicationDataFactory()

    # a default response from public-backend
    httpserver.expect_request("/applications/applications/").respond_with_json(
        {
            "applications": [
                {
                    "uid": str(uuid.uuid1()),
                    "created": str(datetime(2019, 1, 1)),
                    "data": dataclasses.asdict(backend_application),
                },
            ]
        }
    )

    application_dtos = import_applications().dtos

    # then we assume the backend applications is fully imported into the backoffice
    assert len(application_dtos) == 1
    imported_application: ApplicationDTO = application_dtos[0]

    assert imported_application.first_name == backend_application.first_name
    assert imported_application.last_name == backend_application.last_name
    assert imported_application.email == backend_application.email
    assert imported_application.phone_number == backend_application.phone_number
    assert imported_application.positions == backend_application.positions
    assert imported_application.motivation == backend_application.motivation
    assert imported_application.qualification == backend_application.qualification
    assert (
        imported_application.date_of_birth.strftime("%Y-%m-%d")
        == backend_application.date_of_birth
    )
    assert imported_application.nationalities == backend_application.nationalities
    assert imported_application.gender == backend_application.gender
    # TODO: Check for data consistency and not only list len
    assert len(imported_application.spoken_languages) == len(
        backend_application.spoken_languages
    )


def test_import_application_logs_error_and_keeps_going_if_any_is_malformed(caplog):
    invalid_application_uid = str(uuid.uuid4())
    valid_application_uid = str(uuid.uuid4())

    malformed_data = dataclasses.asdict(ApplicationDataFactory(first_name="Harry"))
    del malformed_data["phone_number"]

    data = {
        "applications": [
            {
                "uid": invalid_application_uid,
                "created": str(datetime(2019, 1, 1)),
                "data": malformed_data,
            },
            {
                "uid": valid_application_uid,
                "created": str(datetime(2020, 3, 1)),
                "data": dataclasses.asdict(ApplicationDataFactory(first_name="Ron")),
            },
        ]
    }
    with caplog.at_level(logging.ERROR):
        dtos = import_applications(fetch_data=lambda: data).dtos

    assert len(dtos) == 1
    assert dtos[0].uid == valid_application_uid

    assert len(caplog.records) == 1
    assert caplog.records[0].levelname == "ERROR"
    assert invalid_application_uid in caplog.records[0].msg
    assert isinstance(caplog.records[0].exc_info[1], ValidationError)
    assert "Harry" not in caplog.records[0].msg


def test_import_application_logs_critical_and_keeps_going_if_malformed_and_no_uid(
    caplog,
):
    valid_application_uid = str(uuid.uuid4())
    malformed_data = {"jikes": 1312}

    data = {
        "applications": [
            {
                "data": malformed_data,
            },
            {
                "uid": valid_application_uid,
                "created": str(datetime(2020, 3, 1)),
                "data": dataclasses.asdict(ApplicationDataFactory(first_name="Ron")),
            },
        ]
    }
    with caplog.at_level(logging.ERROR):
        dtos = import_applications(fetch_data=lambda: data).dtos

    assert len(dtos) == 1
    assert dtos[0].uid == valid_application_uid

    assert len(caplog.records) == 1
    assert caplog.records[0].levelname == "ERROR"
    assert isinstance(caplog.records[0].exc_info[1], ValidationError)


def test_import_application_returns_malformed_applications(caplog):
    invalid_application_uid = str(uuid.uuid4())

    malformed_data = dataclasses.asdict(ApplicationDataFactory(first_name="Harry"))
    del malformed_data["phone_number"]

    data = {
        "applications": [
            {
                "uid": invalid_application_uid,
                "created": str(datetime(2019, 1, 1)),
                "data": malformed_data,
            }
        ]
    }
    malformed = import_applications(fetch_data=lambda: data).malformed
    assert malformed[0] == data["applications"][0]
