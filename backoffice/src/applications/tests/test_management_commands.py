import dataclasses
import logging
from datetime import date, datetime, timezone
from unittest.mock import Mock, call

from planner_common.models import Gender
from planner_common.test.factories import ApplicationDataFactory
from pytest import mark, raises

from applications.management.commands.import_applications import (
    Command as ImportApplicationsCommand,
)
from applications.management.commands.recover_applications import (
    Command as RecoverApplicationsCommand,
)
from applications.models import Application, MalformedApplication
from applications.service import ApplicationDTO, Language, RemoteApplications
from applications.tests.factories import ApplicationDTOFactory, ApplicationFactory


def no_op(_):
    pass


@mark.django_db
def test_import_applications_creates_single_application():
    import_applications_mock = Mock()
    certificates = ["lifeguard", "sea-basic"]
    dto = ApplicationDTO(
        uid="foo",
        created=datetime(1999, 12, 10, 1, 1, 1, tzinfo=timezone.utc),
        spoken_languages=[Language(code="de", points=2), Language("zh", points=4)],
        positions=["cook", "media-coord"],
        first_name="Jack",
        last_name="McSparrow",
        phone_number="555-789123",
        email="jack@example.com",
        motivation="Be the best example you never heard of",
        qualification="so much",
        date_of_birth=date(1993, 11, 12),
        nationalities=["DE", "ZN"],
        gender=Gender.PREFER_NOT_SAY,
        certificates=certificates,
        other_certificate="",
    )
    import_applications_mock.return_value = RemoteApplications([dto], [])

    ImportApplicationsCommand(
        import_applications=import_applications_mock, acknowledge_import=no_op
    ).handle()

    application = Application.objects.last()
    assert application

    assert import_applications_mock.call_count == 1
    expected_equal_fields = [
        "first_name",
        "last_name",
        "phone_number",
        "email",
        "motivation",
        "qualification",
        "date_of_birth",
        "created",
    ]

    assert {field: getattr(dto, field) for field in expected_equal_fields} == {
        field: getattr(application, field) for field in expected_equal_fields
    }
    assert [
        (language.code, language.points) for language in application.languages.all()
    ] == [("de", 2), ("zh", 4)]
    assert [position.key for position in application.positions.all()] == [
        "cook",
        "media-coord",
    ]
    assert application.nationalities == "DE;ZN"
    assert application.gender == Gender.PREFER_NOT_SAY
    assert application.certificates == certificates


@mark.django_db
def test_import_applications_creates_multiple_applications():
    import_applications_mock = Mock()
    import_applications_mock.return_value = RemoteApplications(
        [
            ApplicationDTOFactory(),
            ApplicationDTOFactory(),
        ],
        [],
    )

    ImportApplicationsCommand(
        import_applications=import_applications_mock, acknowledge_import=no_op
    ).handle()

    assert Application.objects.all().count() == 2


def test_import_applications_updates_import_tracker():
    update_import_tracker = Mock()
    ImportApplicationsCommand(
        import_applications=lambda: RemoteApplications([], []),
        acknowledge_import=no_op,
        update_import_tracker=update_import_tracker,
    ).handle()
    assert update_import_tracker.call_count == 1


def test_import_applications_does_not_update_import_tracker_on_failed_import():
    update_import_tracker = Mock()

    try:
        ImportApplicationsCommand(
            import_applications=Mock(side_effect=Exception()),
            acknowledge_import=no_op,
            update_import_tracker=update_import_tracker,
        ).handle()
    except Exception:
        # We don't care for this here, error reporting is covered by another test
        pass

    assert update_import_tracker.call_count == 0


@mark.django_db
def test_import_applications_acknowledges_applications():
    remoteApplications = RemoteApplications(
        [
            ApplicationDTOFactory(uid="1"),
            ApplicationDTOFactory(uid="2"),
        ],
        [],
    )

    acknowledge_mock = Mock()
    ImportApplicationsCommand(
        import_applications=lambda: remoteApplications,
        acknowledge_import=acknowledge_mock,
    ).handle()

    assert acknowledge_mock.call_count == 2
    assert acknowledge_mock.call_args_list == [call("1"), call("2")]


@mark.django_db
def test_import_applications_logs_warning_on_failed_acknowledge(caplog):
    uid = "12345"
    exception = Exception("WHEN this fails")
    remoteApplications = RemoteApplications([ApplicationDTOFactory(uid=uid)], [])
    with caplog.at_level(logging.WARNING):
        ImportApplicationsCommand(
            import_applications=lambda: remoteApplications,
            acknowledge_import=Mock(side_effect=exception),
        ).handle()

    assert len(caplog.records) == 1
    log = caplog.records[0]
    assert log.levelname == "WARNING"
    assert f'"{uid}"' in log.msg
    assert log.exc_info[1] == exception


@mark.django_db
def test_import_applications_raises_error_on_failed_import():
    exception = Exception()
    import_mock = Mock(side_effect=exception)
    with raises(Exception) as e:
        ImportApplicationsCommand(import_applications=import_mock).handle()
    assert e.value == exception


@mark.xfail(reason="We don't store the uid with the application yet")
@mark.django_db
def test_import_applications_doesnt_import_if_the_uid_already_exists_in_the_db():
    # We disambiguate by id only, so lets keep the other fields random
    ApplicationFactory(uid="42", name="blub")
    ImportApplicationsCommand(
        import_applications=lambda: RemoteApplications(
            [ApplicationDTOFactory(uid="42", name="foo")], []
        ),
    ).handle()

    assert Application.objects.count() == 1
    assert Application.objects.get().name == "blub"


@mark.django_db
def test_import_applications_creates_malformed_application():
    import_applications_mock = Mock()
    malformed_application_data = {"uid": 1, "something": "is not right here"}
    import_applications_mock.return_value = RemoteApplications(
        [], [malformed_application_data]
    )

    ImportApplicationsCommand(
        import_applications=import_applications_mock, acknowledge_import=no_op
    ).handle()

    malformed_application = MalformedApplication.objects.last()
    assert isinstance(malformed_application, MalformedApplication)

    assert import_applications_mock.call_count == 1
    assert malformed_application.submitted_data == malformed_application_data


@mark.django_db
def test_import_applications_creates_multiple_malformed_applications():
    import_applications_mock = Mock()
    malformed_applications_data = [
        {"uid": 1, "something": "is not right here"},
        {"uid": 123019283},
    ]
    import_applications_mock.return_value = RemoteApplications(
        [], malformed_applications_data
    )

    ImportApplicationsCommand(
        import_applications=import_applications_mock, acknowledge_import=no_op
    ).handle()

    malformed_application = MalformedApplication.objects.last()
    assert isinstance(malformed_application, MalformedApplication)

    assert malformed_application.submitted_data == malformed_applications_data[1]
    assert MalformedApplication.objects.all().count() == 2


@mark.django_db
def test_import_malformed_applications_acknowledges_applications():
    remoteApplications = RemoteApplications(
        [],
        [{"uid": "1312", "malformed": "application"}, {"uid": "123", "ignored": 3}],
    )

    acknowledge_mock = Mock()
    ImportApplicationsCommand(
        import_applications=lambda: remoteApplications,
        acknowledge_import=acknowledge_mock,
    ).handle()

    assert acknowledge_mock.call_count == 2
    assert acknowledge_mock.call_args_list == [call("1312"), call("123")]


@mark.django_db
def test_recover_command_recovers_repaired_applications():
    # given: there is one malformed application in the DB which has been repaired manually
    repaired_application: dict = {}
    repaired_application["uid"] = "1234"
    repaired_application["created"] = str(
        datetime(1999, 12, 10, 1, 1, 1, tzinfo=timezone.utc)
    )
    repaired_application["data"] = dataclasses.asdict(ApplicationDataFactory())

    MalformedApplication(uid="1234", submitted_data=repaired_application).save()

    # when: I use the recover function with the recover flag set
    RecoverApplicationsCommand().handle()

    # then: I expect that there is one application in the databse
    assert len(Application.objects.all()) == 1
    # and: I expect that no application is in a malformed state
    assert len(MalformedApplication.objects.all()) == 0


@mark.django_db
def test_exception_during_acknowledge_saves_application_anyways():
    # The appl
    def raise_exception():
        raise Exception("simulated network error")

    ImportApplicationsCommand(
        import_applications=lambda: RemoteApplications(
            dtos=[ApplicationDTOFactory(first_name="Aloy")], malformed=[]
        ),
        acknowledge_import=lambda _: raise_exception(),
    ).handle()

    assert Application.objects.filter(first_name="Aloy").exists()


@mark.django_db
def test_exception_during_acknowledge_saves_malformed_application_anyways():
    # The appl
    def raise_exception():
        raise Exception("simulated network error")

    data = {"uid": 42}

    ImportApplicationsCommand(
        import_applications=lambda: RemoteApplications(dtos=[], malformed=[data]),
        acknowledge_import=lambda _: raise_exception(),
    ).handle()

    assert MalformedApplication.objects.filter(uid=42).exists()


@mark.django_db
def test_import_applications_works_with_old_rhib_spelling():
    import_applications_mock = Mock()
    dto = ApplicationDTO(
        uid="foo",
        created=datetime(1999, 12, 10, 1, 1, 1, tzinfo=timezone.utc),
        spoken_languages=[Language(code="de", points=2), Language("zh", points=4)],
        positions=["rib-driver"],  # old spelling here
        first_name="Jack",
        last_name="McSparrow",
        phone_number="555-789123",
        email="jack@example.com",
        motivation="Be the best example you never heard of",
        qualification="so much",
        date_of_birth=date(1993, 11, 12),
        nationalities=["DE", "ZN"],
        gender=Gender.PREFER_NOT_SAY,
        certificates=[],
        other_certificate="",
    )
    import_applications_mock.return_value = RemoteApplications([dto], [])

    ImportApplicationsCommand(
        import_applications=import_applications_mock, acknowledge_import=no_op
    ).handle()

    application = Application.objects.last()
    assert application is not None
    assert [p.key for p in application.positions.all()] == [
        "rhib-driver"  # correct spelling here
    ]
