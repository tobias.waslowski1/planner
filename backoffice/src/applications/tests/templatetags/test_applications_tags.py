from pytest import mark

from applications.templatetags.applications_tags import join_comma_and


@mark.parametrize(
    "input,expected",
    [
        ([], ""),
        (["x"], "x"),
        (["x", "y"], "x and y"),
        (["x", "y", "z"], "x, y and z"),
    ],
)
def test_join_comma_and(input, expected):
    assert join_comma_and(input) == expected
