import json
from datetime import date, datetime

from django.utils import timezone
from freezegun import freeze_time
from phonenumber_field.phonenumber import PhoneNumber
from planner_common.models import Gender
from pytest import mark

from applications.models import (
    Application,
    Import,
    Language,
    PositionDecision,
    certificates_separator,
    nationalities_separator,
)
from seawatch_registration.tests.factories import PositionFactory

from .factories import ApplicationFactory


@mark.django_db
def test_get_absolute_url_includes_id():
    application: Application = ApplicationFactory.create(id=94)
    url = application.get_absolute_url()
    assert url == "/applications/94/"


@mark.django_db
@freeze_time("2021-02-02 01:02:03")
def test_Import_updating_allows_later_retrieval():
    Import.update_to_now()
    assert Import.get_last_imported() == timezone.now()


@mark.django_db
def test_Import_get_last_imported_returns_None_if_never_imported():
    assert Import.get_last_imported() is None


def test_certificates_splits_db_value():
    assert ApplicationFactory.build(
        certificates_csv=f"foo{certificates_separator}bar"
    ).certificates == ["bar", "foo"]


def test_certificates_is_empty_if_db_value_is_empty_string():
    assert ApplicationFactory.build(certificates_csv="").certificates == []


@mark.django_db
def test_to_history_dict_is_jsonifiable():
    data = {
        "first_name": "hackbert",
        "last_name": "divine",
        "certificates": ["bar", "foo"],
        "date_of_birth": date(1990, 5, 1),
        "phone_number": PhoneNumber.from_string("+49 152 123 456 89"),
        "email": "foo@example.com",
        "gender": Gender.NONE_OTHER,
        "motivation": "lorem impson",
        "qualification": "dolores pomodores",
        "nationalities": nationalities_separator.join(["UK", "DE"]),
        "accepted": datetime(1967, 3, 4, 13, tzinfo=timezone.utc),
    }
    application = Application(created=timezone.now(), **data)
    application.save()

    application.positions.set(
        [PositionFactory(name="PosA"), PositionFactory(name="PosB")]
    )

    Language.objects.create(code="arz", points=1, application=application)
    Language.objects.create(code="deu", points=2, application=application)

    application.refresh_from_db()
    r = json.loads(json.dumps(application.to_history_dict()))

    assert r == {
        **data,
        "id": application.id,
        "date_of_birth": data["date_of_birth"].isoformat(),
        "phone_number": str(data["phone_number"]),
        "gender": str(data["gender"]),
        "positions": ["PosA", "PosB"],
        "languages": [{"code": "arz", "points": 1}, {"code": "deu", "points": 2}],
        "accepted": data["accepted"].isoformat(),
    }


@mark.django_db
def test_to_history_dict_uses_all_fields():
    history_dict_keys = set(ApplicationFactory.create().to_history_dict().keys())
    fields = set(f.name for f in Application._meta.get_fields())

    assert fields - history_dict_keys == (
        {"created", "history", "comments", "volunteer"}  # We don't care about these
        | {"certificates_csv"}  # this is saved as "certificates"
        | {"position_decisions"}  # this is just a method to access the through model
    )

    assert history_dict_keys - fields == {"certificates"}


@mark.django_db
def test_does_not_include_accepted_applications_by_default():
    ApplicationFactory(accepted=timezone.now())
    assert not Application.objects.all().exists()


@mark.django_db
def test_accepted_applications_can_be_retrieved():
    a = ApplicationFactory(accepted=timezone.now())
    assert list(Application.objects.accepted()) == [a]


@mark.parametrize(
    "positions, expected",
    [
        ({"foo": PositionDecision.Decision.OPEN}, False),
        (
            {
                "foo": PositionDecision.Decision.OPEN,
                "bar": PositionDecision.Decision.ACCEPT,
            },
            False,
        ),
        ({"foo": PositionDecision.Decision.REJECT}, True),
    ],
)
@mark.django_db
def test_has_all_positions_decided(positions, expected):
    application = ApplicationFactory(
        positions=[PositionFactory(key=k) for k in positions.keys()]
    )

    for key, decision in positions.items():
        row_count = application.position_decisions.filter(position__key=key).update(
            decision=decision
        )
        assert row_count == 1

    assert application.has_all_positions_decided == expected
