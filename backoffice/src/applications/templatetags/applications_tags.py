from typing import Any, List

from countryinfo import CountryInfo
from django import template
from django.template.loader import render_to_string

from ..models import Application, nationalities_separator

register = template.Library()


@register.simple_tag
def display_nationalities(nationalities: str) -> str:
    def string_for_countrycode(countrycode: str) -> str:
        try:
            code = CountryInfo(countrycode).demonym()
            if not code:
                return countrycode.upper()
            return code
        except KeyError:
            return countrycode.upper()

    return ", ".join(
        string_for_countrycode(countrycode)
        for countrycode in nationalities.split(nationalities_separator)
    )


@register.filter
def attr(items: List[Any], name: str) -> List[Any]:
    return [getattr(i, name) for i in items]


@register.filter
def rejection_mail_subject(application: Application):
    return render_to_string(
        "applications/mail/reject_application.subject.txt", {"application": application}
    ).strip()


@register.filter
def rejection_mail_body(application: Application):
    # This filter is also used when actually sending the mail, ensuring we're rendering them
    # similarly.
    return render_to_string(
        "applications/mail/reject_application.message.txt",
        {
            "application": application,
            "positions": application.positions.filter(open_for_applications=True),
        },
    ).strip()


@register.filter
def method(items: List[Any], name: str) -> List[Any]:
    return [getattr(i, name)() for i in items]


@register.filter
def join_comma_and(items: List[str]):
    l = len(items)
    if l == 0:
        return ""
    if l == 1:
        return items[0]
    return " and ".join([", ".join(items[:-1]), items[-1]])
