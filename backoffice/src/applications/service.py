import datetime
import json
import logging
import os
from collections import namedtuple
from dataclasses import dataclass
from typing import List, Optional
from urllib.parse import quote

import jsonschema
import requests
from django.conf import settings
from planner_common import schema

from seawatch_planner.public_backend import auth_headers

logger = logging.getLogger(__name__)


RemoteApplications = namedtuple("RemoteApplications", ["dtos", "malformed"])


@dataclass
class Language:
    code: str
    points: int


@dataclass
class ApplicationDTO:
    uid: str
    created: datetime.datetime

    first_name: str
    last_name: str
    email: str
    phone_number: str
    date_of_birth: datetime.date
    spoken_languages: List[Language]
    positions: List[str]
    motivation: str
    qualification: str
    nationalities: List[str]
    gender: str
    certificates: List[str]
    other_certificate: Optional[str]


def fetch_applications_data_from_public_backend():
    response = requests.get(
        settings.PLANNER_PUBLIC_BACKEND_URL + "/applications/applications/",
        headers=auth_headers(),
    )
    # TODO: test malformed application data, e.g. missing "phone_number" field
    response.raise_for_status()
    if response.status_code != 204:
        return response.json()


def import_applications(
    fetch_data=fetch_applications_data_from_public_backend,
) -> RemoteApplications:
    dtos = []
    malformed = []
    data = fetch_data()
    if data:
        for application_data in data["applications"]:
            try:
                jsonschema.validate(
                    instance=application_data["data"], schema=schema.initial_application
                )
                data_fields = application_data["data"]

                dto = ApplicationDTO(
                    uid=application_data["uid"],
                    created=application_data["created"],
                    spoken_languages=to_language_list(
                        application_data["data"]["spoken_languages"]
                    ),
                    positions=application_data["data"]["positions"],
                    date_of_birth=datetime.datetime.strptime(
                        data_fields["date_of_birth"], "%Y-%m-%d"
                    ).date(),
                    other_certificate=application_data["data"].get("other_certificate"),
                    **{
                        field: application_data["data"][field]
                        for field in [
                            "first_name",
                            "last_name",
                            "phone_number",
                            "email",
                            "motivation",
                            "qualification",
                            "nationalities",
                            "gender",
                            "certificates",
                        ]
                    },
                )
            except Exception:
                if "uid" not in application_data:
                    logger.error(
                        "Failed to parse Application. No UID found",
                        exc_info=True,
                    )
                else:
                    uid = application_data["uid"]
                    logger.error(
                        f'Failed to parse Application "{uid}"',
                        exc_info=True,
                    )
                    malformed.append(application_data)
            else:
                dtos.append(dto)
    return RemoteApplications(dtos, malformed)


def acknowledge_import(application_id) -> None:
    response = requests.delete(
        f"{settings.PLANNER_PUBLIC_BACKEND_URL}/applications/{quote(application_id)}/",
        headers=auth_headers(),
    )
    response.raise_for_status()


def to_language_list(languages) -> List[Language]:
    return list(
        map(
            lambda e: Language(code=e["language"], points=e["points"]),
            languages,
        )
    )
