from django.http import HttpResponse
from django.urls import path

from . import views

urlpatterns = [
    path("", views.ApplicationListView.as_view(), name="application_list"),
    path("<int:pk>/", views.ApplicationUpdateView.as_view(), name="application_update"),
    path(
        "<int:pk>/delete/",
        views.ApplicationDeleteView.as_view(),
        name="application_delete",
    ),
    path(
        "<int:pk>/accept/",
        views.ApplicationAcceptView.as_view(),
        name="application_accept",
    ),
    path("<int:pk>/add_comment/", views.add_comment, name="application_add_comment"),
    path(
        "<int:pk>/decide_position/",
        views.decide_position,
        name="application_decide_position",
    ),
    path(
        "<int:pk>/update_additional_positions/",
        views.update_additional_positions,
        name="application_update_additional_positions",
    ),
]
