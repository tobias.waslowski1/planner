import logging
from datetime import datetime
from typing import Any, Dict, cast
from urllib.parse import urlencode

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.db import transaction
from django.db.models import Q
from django.http.request import HttpRequest
from django.http.response import (
    HttpResponse,
    HttpResponseBadRequest,
    HttpResponseNotFound,
    HttpResponseRedirect,
)
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from django.template.response import TemplateResponse
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.utils.translation import gettext as _
from django.views.decorators.http import require_POST
from django.views.generic import DeleteView
from django.views.generic.base import View
from django.views.generic.edit import UpdateView
from django_tables2 import SingleTableView

from applications.templatetags.applications_tags import (
    rejection_mail_body,
    rejection_mail_subject,
)
from history.models import HistoryItem
from missions.models import Availability
from seawatch_registration.models import Position
from volunteers.management.commands.import_inquiries import (
    Command as ImportInquiriesCommand,
)
from volunteers.models import Volunteer

from . import forms
from .management.commands.import_applications import (
    Command as ImportApplicationsCommand,
)
from .models import Application, Import, PositionDecision
from .tables import OpenApplicationsTable

logger = logging.getLogger(__name__)


def get_position_or_none(key):
    try:
        return Position.objects.get(key=key)
    except Position.DoesNotExist:
        return None


@require_POST
def add_comment(request, pk):
    application = get_object_or_404(Application, pk=pk)
    text = request.POST.get("comment")
    if not text:
        return HttpResponseBadRequest()

    comment = application.comments.create(
        author=request.user,
        text=text,
    )

    return TemplateResponse(
        request,
        "applications/application_form_add_comment.html",
        {"comment": comment, "application": application},
    )


class ApplicationListView(PermissionRequiredMixin, SingleTableView):
    model = Application
    table_class = OpenApplicationsTable
    permission_required = "applications.view_application"

    search = None

    def get_queryset(self):
        qs = super().get_queryset()

        position = self.request.GET.get("position")
        if position:
            qs = qs.filter(positions__key=position)

        self.search = self.request.GET.get("search")
        if self.search:
            search_without_spaces = self.search.split(" ")
            for word in search_without_spaces:
                qs = qs.filter(
                    Q(first_name__icontains=word) | Q(last_name__icontains=word)
                )
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["last_import"] = Import.get_last_imported()
        context["positions"] = [
            {"object": pos, "link": self._build_link(pos)}
            for pos in Position.objects.all().order_by("name")
        ]
        context["search"] = self.search or ""
        context["selected_position"] = get_position_or_none(
            self.request.GET.get("position")
        )
        context["all_positions_link"] = (
            f'?{urlencode({"search":self.search})}' if self.search else "?"
        )
        return context

    def _build_link(self, pos):
        query_search = {"position": pos.key}
        if self.search:
            query_search["search"] = self.search
        return f"?{urlencode(query_search)}"

    def post(self, request, *args, **kwargs):
        try:
            apps_before_import = Application.objects.count()
            unknown_avail_before_import = Availability.objects.filter(
                availability=Availability.UNKNOWN
            ).count()
            ImportApplicationsCommand().handle()
            ImportInquiriesCommand().handle()
            new_app_count = Application.objects.count() - apps_before_import
            new_avail_count = (
                unknown_avail_before_import
                - Availability.objects.filter(availability=Availability.UNKNOWN).count()
            )
            info_message = f"Import succeeded. Found {new_app_count} new applications"
            if new_avail_count > 0:
                info_message += f" and {new_avail_count} new status updates."
            messages.info(request, info_message)
        except:
            logger.exception(
                "Importing applications failed", extra={"request": request}
            )
            messages.error(
                request,
                "An error occured during import. Sorry about that. "
                "The administrator has alread been notified.",
            )
        return HttpResponseRedirect(reverse("application_list"))


class ApplicationUpdateView(PermissionRequiredMixin, UpdateView):
    model = Application
    permission_required = "applications.view_application"
    form_class = forms.ApplicationForm

    object: Application
    __original_history_dict: dict

    def post(self, *args, **kwargs):
        with transaction.atomic():
            return super().post(*args, **kwargs)

    def get_object(self):
        application = cast(Application, super().get_object())
        self.__original_history_dict = application.to_history_dict()
        return application

    def form_valid(self, form):
        response = super().form_valid(form)
        HistoryItem.objects.create_from_dicts(
            self.object,
            old=self.__original_history_dict,
            new=self.object.to_history_dict(),
        )
        messages.success(self.request, _("The application was successfully updated"))
        return response


class ApplicationDeleteView(PermissionRequiredMixin, DeleteView):
    model = Application
    success_url = reverse_lazy("application_list")
    permission_required = "applications.delete_application"

    object: Application

    # overridable in tests
    send_mail = staticmethod(lambda *a, **k: send_mail(*a, **k))

    def post(self, request, *args, **kwargs):
        should_send_mail_str = request.POST.get("send_email")
        if should_send_mail_str not in ["true", "false"]:
            return HttpResponseBadRequest("invalid or missing send_email parameter")

        self.should_send_mail = should_send_mail_str == "true"

        with transaction.atomic():
            return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        # DeleteView includes FormMixin which provides form_valid and object, but django-stubs isn't
        # up to date on this yet: https://github.com/typeddjango/django-stubs/pull/849
        application = self.object

        mail_data = None
        if self.should_send_mail:
            # need to do this before we delete
            mail_data = {
                "subject": rejection_mail_subject(application),
                "message": rejection_mail_body(application),
            }

        response = super().form_valid(form)  # type: ignore

        messages.info(
            self.request, f"{application.first_name}'s application has been deleted."
        )

        if mail_data:
            mail_context = {"application": application}
            self.send_mail(
                subject=mail_data["subject"],
                message=mail_data["message"],
                from_email=settings.EXTERNAL_EMAIL_FROM,
                recipient_list=[application.email],
            )
        return response


class ApplicationAcceptView(PermissionRequiredMixin, View):
    permission_required = [
        "volunteers.add_volunteer",
    ]

    # overridable in tests
    send_mail = staticmethod(lambda *a, **k: send_mail(*a, **k))

    def get(self, request: HttpRequest, pk):
        application = get_object_or_404(Application, pk=pk)
        if not application.has_all_positions_decided:
            return self._handle_application_with_undecided_positions(application)

        return TemplateResponse(
            request,
            "applications/application_confirm_accept.html",
            context={"application": application},
        )

    def post(self, request: HttpRequest, pk):
        with transaction.atomic():
            application = get_object_or_404(Application, pk=pk)

            if not application.has_all_positions_decided:
                return self._handle_application_with_undecided_positions(application)

            original_history_dict = application.to_history_dict()

            Volunteer.objects.create_from_application(application)
            application.accepted = timezone.now()
            application.save()

            HistoryItem.objects.create_from_dicts(
                application,
                old=original_history_dict,
                new=application.to_history_dict(),
            )

            mail_context = {"application": application}
            try:
                self.send_mail(
                    subject=render_to_string(
                        "applications/mail/accept_application.subject.txt",
                        mail_context,
                    ).strip(),
                    message=render_to_string(
                        "applications/mail/accept_application.message.txt",
                        mail_context,
                    ).strip(),
                    from_email=settings.EXTERNAL_EMAIL_FROM,
                    recipient_list=[application.email],
                )
            except:
                logger.exception(
                    "Error sending acceptance email",
                    extra={"request": self.request},
                )
                messages.error(
                    request,
                    (
                        "There was an error sending the mail. Please notify the applicant yourself.\n"
                        "We have already been notified about this and will look into the cause as soon "
                        "as possible. Sorry for the inconvenience."
                    ),
                )

        messages.info(
            request,
            f"Application accepted, {application.first_name} is now a Volunteer!",
        )

        return HttpResponseRedirect(reverse("application_list"))

    def _handle_application_with_undecided_positions(self, application):
        logging.warning(
            "Accept view has been reached for application that is not ready",
            extra={"request": self.request},
        )
        messages.warning(
            self.request,
            "Cannot accept applications before all positions have been decided",
        )
        return HttpResponseRedirect(application.get_absolute_url())


@permission_required("applications.change_application", raise_exception=True)
@require_POST
def decide_position(request: HttpRequest, pk):
    position_key = request.POST.get("position")
    decision = request.POST.get("decision")

    if not position_key or not decision:
        return HttpResponseBadRequest()

    application = get_object_or_404(Application, pk=pk)

    try:
        pd = application.position_decisions.get(position__key=position_key)
    except PositionDecision.DoesNotExist:
        return HttpResponseNotFound()
    pd.decision = PositionDecision.Decision(decision)
    pd.by = cast(User, request.user)
    pd.time = timezone.now()
    pd.save()

    return TemplateResponse(
        request,
        "applications/application_decide_position_response.html",
        context={
            "application": application,
            "pd": pd,
        },
    )


@permission_required("applications.change_application", raise_exception=True)
@require_POST
def update_additional_positions(request: HttpRequest, pk: int) -> HttpResponse:
    with transaction.atomic():
        application = get_object_or_404(Application, pk=pk)
        new_positions = {
            get_object_or_404(Position, pk=pos_id)
            for pos_id in request.POST.getlist("additional_positions")
        }
        current_positions = set(
            application.positions.filter(open_for_applications=False)
        )

        for p in current_positions - new_positions:
            application.positions.remove(p)

        for p in new_positions - current_positions:
            application.positions.add(p)

    return TemplateResponse(
        request,
        "applications/application_update_additional_positions_response.html",
        context={"application": application},
    )
