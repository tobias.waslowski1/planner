from django.core.management.base import BaseCommand
from django.db import transaction

from applications.models import Application, MalformedApplication
from applications.service import import_applications


class Command(BaseCommand):
    help = "Recover malformed applications"

    def __init__(
        self,
        import_applications=import_applications,
        *args,
        **kwargs,
    ):
        self._import_applications = import_applications
        super().__init__(*args, **kwargs)

    def handle(self, *args, **options):
        applications = MalformedApplication.objects.all()
        application_data = map(lambda x: x.submitted_data, applications)
        recoveredApplications = self._import_applications(
            fetch_data=lambda: {"applications": list(application_data)}
        )
        dtos = recoveredApplications.dtos

        for dto in dtos:
            with transaction.atomic():
                Application.create_from_dto(dto)
                MalformedApplication.objects.filter(uid=dto.uid).delete()
