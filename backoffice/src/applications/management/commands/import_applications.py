import logging

from django.core.management.base import BaseCommand
from django.db import transaction

from applications.models import Application, Import, MalformedApplication
from applications.service import acknowledge_import, import_applications
from history.models import actor_context

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Import new applications from public backend"

    def __init__(
        self,
        import_applications=import_applications,
        acknowledge_import=acknowledge_import,
        update_import_tracker=lambda: Import.update_to_now(),
        *args,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self._import_applications = import_applications
        self._acknowledge_import = acknowledge_import
        self._update_import_tracker = update_import_tracker

    def handle(self, *args, **options):
        remoteApplications = self._import_applications()
        dtos = remoteApplications.dtos
        malformed_applications = remoteApplications.malformed

        self._update_import_tracker()

        with actor_context("system"):
            for dto in dtos:
                # We want to save the application even if acknowledging fails.
                # It's better to have a duplicate (in case acknowledge fails before it is processed)
                # rather than lose one (in case acknowledge fails after processing and we rolled
                # back)
                # Proper tying for atomic is unreleased as of now:
                #   https://github.com/typeddjango/django-stubs/pull/586
                with transaction.atomic(durable=True):  # type: ignore
                    Application.create_from_dto(dto)
                try:
                    self._acknowledge_import(dto.uid)
                except Exception:
                    logger.warning(
                        f'Acknowledging application "{dto.uid}" failed',
                        exc_info=True,
                    )

        for malformed in malformed_applications:
            # Proper tying for atomic is unreleased as of now:
            #   https://github.com/typeddjango/django-stubs/pull/586
            with transaction.atomic(durable=True):  # type: ignore
                MalformedApplication(
                    uid=malformed["uid"], submitted_data=malformed
                ).save()
            try:
                ## We can assume that the malformed id has a uid attribute
                if "recover" not in options or options["recover"] == False:
                    # XXX: We haven't committed the data to the database yet, we
                    # can't acknowledge here.
                    self._acknowledge_import(malformed["uid"])
            except Exception:
                logger.warning(
                    f"Acknowledging malformed application failed", exc_info=True
                )
