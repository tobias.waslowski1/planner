import datetime
from collections.abc import Callable
from typing import TYPE_CHECKING, List, Optional

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.db.models.query import QuerySet
from django.forms.models import model_to_dict
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField
from planner_common import features
from planner_common.models import Gender

from history.models import HistoryItem
from seawatch_registration.models import Position, ShortTextField

from .service import ApplicationDTO

if TYPE_CHECKING:
    from django.db.models.manager import RelatedManager

certificates_separator = ";"
nationalities_separator = ";"


class ApplicationManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(accepted=None)

    def accepted(self) -> QuerySet["Application"]:
        return super().get_queryset().filter(accepted__isnull=False)


class Application(models.Model):
    objects = ApplicationManager()

    id = models.AutoField(primary_key=True)

    history = GenericRelation(HistoryItem)  # type: ignore

    accepted = models.DateTimeField(null=True, default=None)

    first_name = ShortTextField()
    last_name = ShortTextField()
    phone_number = PhoneNumberField()
    positions = models.ManyToManyField(Position, through="PositionDecision")
    email = models.EmailField()
    motivation = models.TextField()
    qualification = models.TextField()
    date_of_birth = models.DateField(default=datetime.date.min)
    gender = ShortTextField(choices=Gender.choices)
    created = models.DateTimeField(editable=False)

    # We store 2-char country codes with ;-separators
    nationalities = models.TextField()

    # Certificates separated by `certificates_separator`
    certificates_csv = models.TextField(blank=True)

    if TYPE_CHECKING:
        # See https://github.com/sbdchd/django-types
        languages = RelatedManager["Language"]()
        comments = RelatedManager["Comment"]()
        position_decisions = RelatedManager["PositionDecision"]()

    @property
    def name(self):
        return f"{self.first_name} {self.last_name}"

    @property
    def name_reversed(self):
        return f"{self.last_name}, {self.first_name}"

    @property
    def certificates(self) -> List[str]:
        if self.certificates_csv:
            return sorted(self.certificates_csv.split(certificates_separator))
        else:
            # We need to handle this specifically because "".split(...) is [""], not []
            return []

    @certificates.setter
    def certificates(self, value: List[str]) -> None:
        self.certificates_csv = certificates_separator.join(sorted(value))

    @staticmethod
    def compile_certificates(dto: ApplicationDTO):
        if "other" in dto.certificates:
            otherIndex = dto.certificates.index("other")
            if otherIndex > -1:
                dto.certificates[otherIndex] = f"other: {dto.other_certificate}"
        return certificates_separator.join(dto.certificates)

    @classmethod
    def create_from_dto(cls, dto: ApplicationDTO):
        application = cls.objects.create(
            **{
                field: getattr(dto, field)
                for field in [
                    "first_name",
                    "last_name",
                    "phone_number",
                    "email",
                    "motivation",
                    "qualification",
                    "date_of_birth",
                    "gender",
                    "created",
                ]
            },
            nationalities=nationalities_separator.join(dto.nationalities),
            certificates_csv=Application.compile_certificates(dto),
        )

        for spoken in dto.spoken_languages:
            application.languages.create(code=spoken.code, points=spoken.points)

        for position_key in dto.positions:
            if position_key == "rib-driver":
                position_key = "rhib-driver"
            position = Position.objects.get(key=position_key)
            application.positions.add(position)

    def __str__(self):
        return self.name_reversed

    def get_absolute_url(self):
        return reverse("application_update", kwargs={"pk": self.pk})

    def to_history_dict(self) -> dict:
        d = model_to_dict(self)

        d["certificates"] = self.certificates
        del d["certificates_csv"]

        d["phone_number"] = str(d["phone_number"])
        d["date_of_birth"] = d["date_of_birth"].isoformat()
        d["positions"] = [position.name for position in d["positions"]]
        d["languages"] = [
            {"code": l.code, "points": l.points} for l in self.languages.all()
        ]
        if d["accepted"]:
            d["accepted"] = d["accepted"].isoformat()

        return d

    @property
    def has_all_positions_decided(self) -> bool:
        return (
            self.position_decisions.filter(
                decision=PositionDecision.Decision.OPEN
            ).count()
            == 0
        )

    @property
    def accepted_positions(self) -> list[Position]:
        return [
            pd.position
            for pd in self.position_decisions.filter(
                decision=PositionDecision.Decision.ACCEPT
            )
        ]


class PositionDecision(models.Model):
    class Decision(models.TextChoices):
        OPEN = "open", _("Open")
        REJECT = "reject", _("Reject")
        ACCEPT = "accept", _("Accept")

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["position", "application"], name="position_decision__unique"
            )
        ]

    position = models.ForeignKey(Position, on_delete=models.PROTECT, related_name="+")
    application = models.ForeignKey(
        Application, on_delete=models.CASCADE, related_name="position_decisions"
    )
    decision = ShortTextField(choices=Decision.choices, default=Decision.OPEN)

    by = models.ForeignKey(User, on_delete=models.PROTECT, default=None, null=True)
    time = models.DateTimeField(default=None, null=True)

    get_decision_display: Callable[[], str]

    def __str__(self) -> str:
        return f"{self.application.name} as {self.position.name}: {self.get_decision_display()}"

    @property
    def is_accept(self):
        return self.decision == PositionDecision.Decision.ACCEPT

    @property
    def is_reject(self):
        return self.decision == PositionDecision.Decision.REJECT


class MalformedApplication(models.Model):
    id = models.AutoField(primary_key=True)
    submitted_data = models.JSONField()
    uid = ShortTextField()
    created = models.DateTimeField(editable=False)

    def save(self, *args, **kwargs):
        """on save, create timestamp"""
        if not self.id and self.created is None:
            self.created = datetime.datetime.now(datetime.timezone.utc)
        return super().save(*args, **kwargs)


class Language(models.Model):
    code = ShortTextField()
    points = models.IntegerField()

    application = models.ForeignKey(
        Application, on_delete=models.CASCADE, related_name="languages"
    )

    def __repr__(self):
        return f"<{type(self).__name__}: {self.code} ({self.points})>"


class Import(models.Model):
    time = models.DateTimeField(default=timezone.now)

    @classmethod
    def update_to_now(cls) -> None:
        cls.objects.update_or_create(defaults={"time": timezone.now()})

    @classmethod
    def get_last_imported(cls) -> Optional[datetime.datetime]:
        try:
            return cls.objects.get().time
        except cls.DoesNotExist:
            return None


class Comment(models.Model):

    text = models.TextField()
    date = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, related_name="+"
    )
    application = models.ForeignKey(
        Application, on_delete=models.CASCADE, related_name="comments"
    )
