from django.contrib.auth.models import AnonymousUser
from django.test.client import RequestFactory

from history.models import actor_context, unsafe_get_actor_context
from seawatch_registration.tests.factories import UserFactory

from ..middleware import history_actor_middleware


def test_middleware_sets_actor_from_user(rf: RequestFactory):
    request = rf.get("/")
    request.user = UserFactory.build(username="qwerty")

    expected_result = object()
    actual_actor = None

    def handler(_):
        nonlocal actual_actor
        actual_actor = unsafe_get_actor_context()
        return expected_result

    actual_result = history_actor_middleware(handler)(request)
    assert actual_result == expected_result
    assert actual_actor == request.user


def test_middleware_doesnt_set_anonymous_user(rf: RequestFactory):
    request = rf.get("/")
    request.user = AnonymousUser()

    actual_actor = "not set"

    def handler(_):
        nonlocal actual_actor
        actual_actor = unsafe_get_actor_context()

    with actor_context(None):  # type: ignore
        history_actor_middleware(handler)(request)
    assert actual_actor is None
