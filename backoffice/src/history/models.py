import threading
from contextlib import contextmanager
from datetime import datetime, timezone
from typing import Iterator, Literal, Optional, Union

from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models

from history.utils import diff

_threadlocal = threading.local()

Actor = Union[User, Literal["system"], Literal["testing_api"], Literal["pytest"]]


@contextmanager
def actor_context(actor: Actor) -> Iterator[None]:
    previous = getattr(_threadlocal, "actor", None)
    _threadlocal.actor = actor
    yield
    _threadlocal.actor = previous


def unsafe_set_actor_context(actor: Actor):
    _threadlocal.actor = actor


def unsafe_get_actor_context() -> Optional[Actor]:
    return getattr(_threadlocal, "actor", None)


def _nowutc():
    return datetime.now(timezone.utc)


def _current_actor():
    actor = getattr(_threadlocal, "actor", None)
    if not actor:
        raise Exception("HistoryItems can only be saved in an actor_context block")
    if isinstance(actor, User):
        return actor.username
    else:
        return actor


class HistoryItemManager(models.Manager):
    def create_from_dicts(self, instance, *, old, new):
        return self.create(content_object=instance, change_json=diff(old, new))


class HistoryItem(models.Model):
    class Meta:
        ordering = ["created"]

    objects = HistoryItemManager()

    # Model association fields as described
    # in https://docs.djangoproject.com/en/4.0/ref/contrib/contenttypes/#generic-relations
    content_type = models.ForeignKey(ContentType, on_delete=models.PROTECT)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey("content_type", "object_id")

    created = models.DateTimeField(default=_nowutc)
    actor = models.TextField(default=_current_actor)
    change_json = models.JSONField()
