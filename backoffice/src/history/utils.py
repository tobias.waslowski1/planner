from typing import Any, List, Optional, TypedDict


class Change(TypedDict):
    field: str
    old: Optional[Any]
    new: Optional[Any]


def diff(old: dict, new: dict) -> List[Change]:
    old_keys = set(old.keys())
    new_keys = set(new.keys())

    return [
        *[{"field": k, "old": old[k], "new": None} for k in old_keys - new_keys],
        *[
            {"field": k, "old": old[k], "new": new[k]}
            for k in old_keys & new_keys
            if old[k] != new[k]
        ],
        *[{"field": k, "old": None, "new": new[k]} for k in new_keys - old_keys],
    ]
