# Formatting for date objects.
DATE_FORMAT = "d.m.Y"
# Formatting for time objects.
TIME_FORMAT = "G:i"
# Formatting for datetime objects.
DATETIME_FORMAT = "d.m.Y, H:i"
# Formatting for date objects when only the year and month are relevant.
YEAR_MONTH_FORMAT = "F Y"
# Formatting for date objects when only the month and day are relevant.
MONTH_DAY_FORMAT = "F js"
# Short formatting for date objects.
SHORT_DATE_FORMAT = "d.m.Y"
# Short formatting for datetime objects.
SHORT_DATETIME_FORMAT = "d.m.Y, H:i"
# First day of week, to be used on calendars.
# 0 means Sunday, 1 means Monday...
FIRST_DAY_OF_WEEK = 1
