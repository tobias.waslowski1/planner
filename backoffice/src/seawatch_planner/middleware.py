import re

from django.conf import settings
from django.contrib.auth.decorators import login_required


class RequireLoginMiddleware(object):
    """
    Middleware component that wraps the login_required decorator around
    all views except matching url patterns. To use, add the class to MIDDLEWARE_CLASSES
    and define LOGIN_REQUIRED_URLS_EXCEPTIONS in your settings.py.
    For example:
    ------
    LOGIN_REQUIRED_URLS_EXCEPTIONS = [
        r'/topsecret/login(.*)$',
        r'/topsecret/logout(.*)$',
    ]
    ------
    LOGIN_REQUIRED_URLS_EXCEPTIONS is where you define any exceptions
    (like login and logout URLs).
    """

    def __init__(
        self,
        get_response,
        exceptions=lambda: settings.LOGIN_REQUIRED_URLS_EXCEPTIONS,
    ):
        self.get_response = get_response
        if callable(exceptions):
            exceptions = exceptions()

        self.exceptions = list(re.compile(url) for url in exceptions)

    def __call__(self, request):
        if any(exception.match(request.path) for exception in self.exceptions):
            return self.get_response(request)
        else:
            return login_required(self.get_response)(request)
