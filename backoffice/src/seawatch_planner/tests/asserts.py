from inspect import isclass
from typing import List, Sequence, Tuple, TypeVar

from django.core.exceptions import PermissionDenied
from django.test.client import RequestFactory
from django.views import View
from pytest import fail

from seawatch_registration.tests.factories import UserFactory


def assert_requires_permissions(
    rf: RequestFactory, view, permissions: List[str], method="get"
):
    if isclass(view) and issubclass(view, View):
        view = view.as_view()

    insufficient_permissions_possibilities = all_possibilities_with_one_element_removed(
        permissions
    )

    for (
        insufficient_perms,
        missing_perm,
    ) in insufficient_permissions_possibilities:
        if method == "post":
            request = rf.post("the_url")
        elif method == "get":
            request = rf.get("the_url")
        else:
            raise ValueError("Method must be either get or post")

        request.user = UserFactory(permissions=insufficient_perms)

        try:
            view(request)
        except PermissionDenied:
            pass  # This is what we expect
        else:
            fail(
                f"Did not deny with permissions {insufficient_perms} "
                f"(missing {missing_perm})"
            )


T = TypeVar("T")


def all_possibilities_with_one_element_removed(
    elements: List[T],
) -> Sequence[Tuple[Sequence[T], T]]:
    """Returns a list of Tuples (list_with_one_element_removed, the_removed_element)"""
    return [
        (elements[0:i] + elements[i + 1 :], elements[i])
        for i in range(0, len(elements))
    ]


def test_all_possibilities_with_one_element_removed():
    input = [1, 2, 3, 4, 5]
    expected = [
        ([2, 3, 4, 5], 1),
        ([1, 3, 4, 5], 2),
        ([1, 2, 4, 5], 3),
        ([1, 2, 3, 5], 4),
        ([1, 2, 3, 4], 5),
    ]
    assert all_possibilities_with_one_element_removed(input) == expected
