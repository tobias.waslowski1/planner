from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from django.views.generic.base import RedirectView, TemplateView

urlpatterns = [
    path("missions/", include("missions.urls")),
    path("admin/", admin.site.urls),
    path("accounts/", include("django.contrib.auth.urls")),
    path("applications/", include("applications.urls")),
    path("volunteers/", include("volunteers.urls")),
    path("", RedirectView.as_view(pattern_name="application_list"), name="index"),
    *(
        [path("_testing/", include("testing_api.urls"))]
        if settings.ENABLE_TESTING_API == "I 100% know what I'm doing by enabling this"
        else []
    ),
    path("styleguide", TemplateView.as_view(template_name="styleguide.html")),
]
