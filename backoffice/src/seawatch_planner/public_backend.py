from django.conf import settings


def auth_headers():
    # As per http spec, we need to specify an authentication scheme. There isn't one for
    # pre-shared tokens (Bearer is commonly used, but that implies OAuth2), so we just
    # use Token.
    return {
        "Authorization": f"Token {settings.PLANNER_PUBLIC_BACKEND_AUTH_TOKEN}",
        ## This is a workaround for #82
        ## In our prod environment the firewall banns the python/requests user-agent
        "User-Agent": "curl/7.74.0",
    }
