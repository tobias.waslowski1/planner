from .base import *

DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "insecure secret key"

ALLOWED_HOSTS = ["*"]

# Email Config
EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
EMAIL_FILE_PATH = PROJECT_DIR("sent_emails")
DEFAULT_FROM_EMAIL = "dev@planner.sea-watch.org"
EXTERNAL_EMAIL_FROM = "dev-backoffice-external@example.com"

MEDIA_ROOT = "media/"

PLANNER_PUBLIC_BACKEND_URL = env.str(
    "PLANNER_PUBLIC_BACKEND_URL", default="http://localhost:8001"
)
PLANNER_PUBLIC_BACKEND_AUTH_TOKEN = "insecure-preshared-token-for-dev"

DATABASES = {"default": env.db(default=f"sqlite:///{PROJECT_DIR}/db.sqlite")}

ENABLE_TESTING_API = env.str("ENABLE_TESTING_API", None)

FEEDBACK_URL = "https://feedback.test"

SHOW_ENV_WARNING = "dev"
