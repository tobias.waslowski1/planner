import email.utils
import logging
import os
from typing import Optional

import ldap
from django_auth_ldap.config import LDAPGroupQuery, LDAPSearch, PosixGroupType

from .base import *


def group_query_from_env(varname):
    groups = env(varname).split(":")
    query = LDAPGroupQuery(groups.pop(0))
    for group in groups:
        query |= LDAPGroupQuery(group)

    return query


DEBUG = False

if not MIDDLEWARE[0].endswith(".SecurityMiddleware"):
    raise Exception("MIDDLEWARE setup isn't as expected")

MIDDLEWARE = [
    MIDDLEWARE[0],
    "whitenoise.middleware.WhiteNoiseMiddleware",
    *MIDDLEWARE[1:],
]


# Must contain a list of all public hostnames for the service as a comma-separated list.
# See https://docs.djangoproject.com/en/3.0/ref/settings/#allowed-hosts
ALLOWED_HOSTS = env.list("ALLOWED_HOSTS")
CSRF_TRUSTED_ORIGINS = env.list("CSRF_TRUSTED_ORIGINS")

SECRET_KEY = env.str("SECRET_KEY")

# EMAIL_URL="smtp://user:password@hostname:25". See EMAIL_SCHEMES at
# https://github.com/joke2k/django-environ/blob/master/environ/environ.py for more
# details,
vars().update(env.email_url("EMAIL_URL"))

#  Add configuration for static files storage using whitenoise
STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"

MEDIA_ROOT = "media/"

PLANNER_PUBLIC_BACKEND_URL = env.str("PLANNER_PUBLIC_BACKEND_URL")
PLANNER_PUBLIC_BACKEND_AUTH_TOKEN = env.str("PLANNER_PUBLIC_BACKEND_AUTH_TOKEN")

# ADMINS=Full Name <email-with-name@example.com>,anotheremailwithoutname@example.com
ADMINS = email.utils.getaddresses([env("ADMINS")])

SERVER_EMAIL = env("ERROR_EMAIL_FROM")
# INTERNAL_EMAIL_FROM = env("PLANNER_INTERNAL_EMAIL_FROM")
EXTERNAL_EMAIL_FROM = env("PLANNER_EXTERNAL_EMAIL_FROM")

DATABASES = {"default": env.db()}

if "AUTH_LDAP_SERVER_URI" in os.environ and env("AUTH_LDAP_SERVER_URI") != "":
    AUTH_LDAP_SERVER_URI = env("AUTH_LDAP_SERVER_URI")
    AUTH_LDAP_BIND_DN = env("AUTH_LDAP_BIND_DN")
    AUTH_LDAP_BIND_PASSWORD = env("AUTH_LDAP_BIND_PASSWORD")
    AUTH_LDAP_USER_SEARCH = LDAPSearch(
        env("AUTH_LDAP_USER_SEARCH"), ldap.SCOPE_SUBTREE, "(uid=%(user)s)"
    )
    AUTH_LDAP_GROUP_SEARCH = LDAPSearch(
        env("AUTH_LDAP_GROUP_SEARCH"), ldap.SCOPE_SUBTREE, "(objectClass=posixGroup)"
    )
    AUTH_LDAP_GROUP_TYPE = PosixGroupType()
    AUTH_LDAP_REQUIRE_GROUP = group_query_from_env("AUTH_LDAP_REQUIRE_GROUP")
    AUTH_LDAP_CONNECTION_OPTIONS = {
        ldap.OPT_X_TLS_CACERTFILE: env("AUTH_LDAP_CA_CERT_PATH"),
        ldap.OPT_X_TLS_REQUIRE_CERT: ldap.OPT_X_TLS_DEMAND,
        ldap.OPT_X_TLS_NEWCTX: 0,
    }

    AUTHENTICATION_BACKENDS = ["django_auth_ldap.backend.LDAPBackend"]
    AUTH_LDAP_USER_ATTR_MAP = {"first_name": "givenName", "last_name": "sn"}
    AUTH_LDAP_FIND_GROUP_PERMS = True
    AUTH_LDAP_USER_FLAGS_BY_GROUP = {
        "is_staff": group_query_from_env("AUTH_LDAP_REQUIRE_GROUP")
    }


class SkipFavicon404(logging.Filter):
    def filter(self, record: logging.LogRecord):
        r = getattr(record, "request", None)
        if r and r.method.upper() == "GET" and r.path == "/favicon.ico":
            return False
        else:
            return True


class SkipWaitressQueueDepthWarning(logging.Filter):
    def filter(self, record: logging.LogRecord) -> bool:
        if record.name == "waitress.queue" and record.levelno == logging.WARNING:
            return False
        else:
            return True


LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "filters": {
        "SkipFavicon404": {"()": "seawatch_planner.settings.prod.SkipFavicon404"},
        "SkipWaitressQueueDepthWarning": {
            "()": "seawatch_planner.settings.prod.SkipWaitressQueueDepthWarning"
        },
    },
    "handlers": {
        "console": {
            "level": "INFO",
            "class": "logging.StreamHandler",
        },
        "mail_admins": {
            "level": "WARNING",
            "filters": ["SkipFavicon404", "SkipWaitressQueueDepthWarning"],
            "class": "django.utils.log.AdminEmailHandler",
        },
    },
    "root": {
        "handlers": ["console", "mail_admins"],
        "level": "WARNING",
    },
}

FEEDBACK_URL: Optional[str] = env("PLANNER_FEEDBACK_URL", default=None)

SHOW_ENV_WARNING = env(
    "SHOW_ENV_WARNING",
    default=None,  # type: ignore
)
