"""
Django settings for seawatch_planner project.

Generated by 'django-admin startproject' using Django 2.2.6.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""
from typing import Optional

import environ
from django.contrib.messages import constants as messages
from django.utils.translation import gettext_lazy as _

env = environ.Env()

PROJECT_DIR = environ.Path(__file__) - 4

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# Application definition

INSTALLED_APPS = [
    "missions",
    "seawatch_registration",
    "applications",
    "volunteers",
    "history",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.humanize",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "bootstrap4",
    "fontawesome_5",
    "django_tables2",
    "django_filters",
    "modeltranslation",
    "django_countries",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "seawatch_planner.middleware.RequireLoginMiddleware",
    "history.middleware.history_actor_middleware",
]

ROOT_URLCONF = "seawatch_planner.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [str(PROJECT_DIR + "templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "seawatch_planner.context_processors.env_warning",
                "seawatch_planner.context_processors.features",
            ],
        },
    },
]

WSGI_APPLICATION = "seawatch_planner.wsgi.application"

# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

# Default id field
DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

# Message system
# Bootstrap labels

MESSAGE_TAGS = {
    messages.DEBUG: "alert-info",
    messages.INFO: "alert-info",
    messages.SUCCESS: "alert-success",
    messages.WARNING: "alert-warning",
    messages.ERROR: "alert-danger",
}

# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = "en"

TIME_ZONE = "UTC"

USE_TZ = True

# Tell Django where the project's translation files should be.
LOCALE_PATHS = [PROJECT_DIR("locale")]


LANGUAGES = [("en", _("English"))]


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/4.0/howto/static-files/

STATIC_URL = "/static/"
STATIC_ROOT = PROJECT_DIR("staticfiles")

LOGIN_REQUIRED_URLS_EXCEPTIONS = [
    r"/accounts/",
    r"/static/",
    r"/favicon.ico",
    r"/_testing",
]
LOGIN_REDIRECT_URL = "/applications"

# Bootstrap Config
BOOTSTRAP4 = {"required_css_class": "required"}

ENABLE_TESTING_API = env.str("ENABLE_TESTING_API", None)

FEEDBACK_URL = None

SHOW_ENV_WARNING: Optional[str] = None

FORMAT_MODULE_PATH = ["seawatch_planner.formats"]
