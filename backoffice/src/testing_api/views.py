import json
from datetime import date, datetime

from django.core.management import call_command
from django.http import HttpResponse
from django.http.response import JsonResponse
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from applications.models import Application, PositionDecision, nationalities_separator
from history.models import actor_context
from missions.models import Mission
from seawatch_registration.models import Position
from volunteers.models import Volunteer


@require_POST
@csrf_exempt
def create_application(request):
    data = json.loads(request.body)
    positions = data.pop("positions", None)
    languages = data.pop("languages", None)

    if "nationalities" in data:
        data["nationalities"] = nationalities_separator.join(data["nationalities"])

    with actor_context("testing_api"):
        a = Application.objects.create(
            **{
                "created": timezone.now(),
                **data,
            }
        )
        if languages:
            for code, points in languages.items():
                a.languages.create(code=code, points=points)
        if positions:
            ps = {
                (p, PositionDecision.Decision.OPEN)
                if isinstance(p, str)
                else (p[0], PositionDecision.Decision(p[1]))
                for p in positions
            }
            a.position_decisions.set(
                [
                    PositionDecision(
                        position=Position.objects.get(name=name),
                        decision=decision,
                    )
                    for name, decision in ps
                ],
                bulk=False,
            )
            # a.positions.set([Position.objects.get(name=p) for p in positions])

    return JsonResponse({"id": a.id})


@require_POST
@csrf_exempt
def create_volunteer(request):
    with actor_context("testing_api"):
        data = json.loads(request.body)
        Volunteer.objects.create(
            **{
                "certificates": [],
                "languages": {},
                "date_of_birth": date(1000, 1, 1),
                "nationalities": [],
                "application": Application.objects.create(
                    **{
                        "created": datetime(2000, 1, 1, 12, 0, tzinfo=timezone.utc),
                        **data.pop("application", {}),
                    }
                ),
                **data,
            }
        )
    return HttpResponse(status=204)


@require_POST
@csrf_exempt
def delete_all_volunteers(request):
    Volunteer.objects.all().delete()
    return HttpResponse(status=204)


@require_POST
@csrf_exempt
def import_applications(request):
    call_command("import_applications")
    return HttpResponse(status=204)


@require_POST
@csrf_exempt
def delete_all_missions(request):
    Mission.objects.all().delete()
    return HttpResponse(status=204)


@require_POST
@csrf_exempt
def create_mission(request):
    Mission.objects.create(**json.loads(request.body))
    return HttpResponse(status=204)
