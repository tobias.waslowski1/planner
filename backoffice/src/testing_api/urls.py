from django.urls import path  # type: ignore

from . import views

urlpatterns = [
    path("import_applications/", views.import_applications),
    path("create_application/", views.create_application),
    path("create_volunteer/", views.create_volunteer),
    path("delete_all_volunteers/", views.delete_all_volunteers),
    path("create_mission/", views.create_mission),
    path("delete_all_missions/", views.delete_all_missions),
]
