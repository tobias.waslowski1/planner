import json
from datetime import date
from unittest.mock import call, patch

from django.http import HttpResponseNotAllowed
from pytest import mark

from applications.models import Application, PositionDecision, nationalities_separator
from testing_api import views
from volunteers.models import Volunteer
from volunteers.tests.factories import VolunteerFactory

#####################
## create_application


@mark.django_db
def test_create_application_from_partial_data(rf):
    response = views.create_application(
        rf.post("", content_type="application/json", data={"first_name": "yo"})
    )
    r = json.loads(response.content)
    assert Application.objects.filter(pk=r["id"]).exists()


@mark.django_db
def test_create_application(rf):
    basic_data = {
        "first_name": "foo",
        "last_name": "bar",
        "email": "foo@bar.de",
        "phone_number": "1234",
        "motivation": "motivated!",
        "qualification": "qualified!",
        "gender": "none_other",
        "certificates": sorted(["STCW-VI/3", "STCW-VI/1", "other: tapdancing"]),
    }
    complex_data = {
        "date_of_birth": "1984-02-14",
        "nationalities": ["FR", "UK"],
        "languages": {"deu": 1, "arz": 3},
    }
    response = views.create_application(
        rf.post(
            "",
            content_type="application/json",
            data={
                **basic_data,
                **complex_data,
                "positions": [["Bosun", "accept"], "Master"],
            },
        )
    )
    r = json.loads(response.content)

    application = Application.objects.get(pk=r["id"])
    for k, v in basic_data.items():
        assert getattr(application, k) == v

    assert application.date_of_birth == date.fromisoformat(
        complex_data.pop("date_of_birth")
    )
    assert application.nationalities == nationalities_separator.join(
        complex_data.pop("nationalities")
    )
    assert {l.code: l.points for l in application.languages.all()} == complex_data.pop(
        "languages"
    )
    assert {
        (pd.position.name, pd.decision) for pd in application.position_decisions.all()
    } == {
        ("Bosun", PositionDecision.Decision.ACCEPT),
        ("Master", PositionDecision.Decision.OPEN),
    }
    assert not complex_data  # just to make sure we didn't forget anything


###################
## create_volunteer


def test_create_volunteer_requires_post(rf):
    response = views.create_volunteer(rf.get("/_testing/create_volunteer/"))
    assert isinstance(response, HttpResponseNotAllowed)


@mark.django_db
def test_creates_volunteer_creates_volunteer(rf):
    response = views.create_volunteer(
        rf.post(
            "not used",
            content_type="application/json",
            data={"first_name": "liselotte", "last_name": "meier"},
        )
    )
    assert response.status_code == 204
    assert Volunteer.objects.all().count() == 1
    volunteer = Volunteer.objects.first()
    assert volunteer
    assert volunteer.name == "liselotte meier"


########################
## delete_all_volunteers


def test_delete_all_volunteers_is_reachable(client):
    # The endpoint requires post, so it will return a 405, but because it doesn't return
    # a 404 we know it's there, and this way we avoid actually doing a lot of work in
    # this test.
    assert client.get("/_testing/delete_all_volunteers/").status_code == 405


@mark.django_db
def test_delete_all_volunteers_deletes_volunteers(rf):
    VolunteerFactory()
    VolunteerFactory()
    response = views.delete_all_volunteers(rf.post("unused paramteter"))
    assert response.status_code == 204
    assert Volunteer.objects.count() == 0


######################
## import_applications


def test_import_applications_is_reachable(client):
    with patch("testing_api.views.call_command") as call_command_mock:
        client.post("/_testing/import_applications/")
    assert call_command_mock.call_args_list == [call("import_applications")]


#################
## create_mission


def test_create_mission_requires_post(rf):
    response = views.create_mission(rf.get("/_testing/create_mission/"))
    assert isinstance(response, HttpResponseNotAllowed)
