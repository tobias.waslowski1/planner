import os
from importlib import import_module
from typing import Dict, Optional
from urllib.parse import urlparse

import playwright
import pytest
from django.conf import settings
from django.contrib.auth import login
from django.http.request import HttpRequest
from playwright._impl._api_structures import SetCookieParam
from playwright.sync_api import Browser, BrowserContext
from pytest_django.lazy_django import skip_if_no_django
from pytest_django.live_server_helper import LiveServer

from history.models import unsafe_set_actor_context
from seawatch_registration.tests.factories import UserFactory


@pytest.fixture(scope="session", autouse=True)
def history_actor():
    unsafe_set_actor_context("pytest")


@pytest.fixture
def mrf():
    skip_if_no_django()
    from seawatch_planner.tests.messages import MessagesRequestFactory

    return MessagesRequestFactory()


@pytest.fixture(scope="session")
def browser_type_launch_args(
    browser_name: Optional[str], browser_type_launch_args: Dict
) -> Dict:
    # Django does this weird check where it errors out it an event loop is running because it
    # assumes it's called from an async context. That's not neccessarily correct though.
    # Specifically, the playwright fixtures create async contexts (and thus an event loop) while the
    # other fixtures are run in the usual context, so we disable the check and hope for the best.
    os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "True"

    if os.environ.get("CI") == "true":
        # Browsers are handled separately in CI
        return browser_type_launch_args

    assert browser_name
    assert (
        os.environ["PLANNER_PLAYWRIGHT_VERSION"] == playwright._repo_version.version  # type: ignore
    ), "Mismatched playwright browsers, did you update playwright without fixing the version in shell.nix?"

    env_var = f"PLANNER_PLAYWRIGHT_{browser_name.upper()}"
    browser_type_launch_args["executable_path"] = os.environ[env_var]
    return browser_type_launch_args


@pytest.fixture()
def logged_in_context(
    browser: Browser,
    live_server: LiveServer,
) -> BrowserContext:
    admin_user = UserFactory(is_superuser=True, is_staff=True)
    admin_user.set_password("password")
    admin_user.save()

    context = browser.new_context(base_url=live_server.url)

    ## Explicit (slow) way to login in
    # page = context.new_page()
    # page.goto(live_server.url + reverse("login"))
    # page.fill("[name=username]", admin_user.username)
    # page.fill("[name=password]", "password")
    # page.click('button:text("Login")')
    # page.close()

    ## Forced (fast) way to in. Mostly copied from django/test/client.py::Client.force_login()
    request = HttpRequest()
    request.session = import_module(settings.SESSION_ENGINE).SessionStore()
    login(request, user=admin_user, backend=None)
    request.session.save()

    cookie: SetCookieParam = {
        "name": settings.SESSION_COOKIE_NAME,
        "value": request.session.session_key,
        "domain": urlparse(live_server.url).netloc.rstrip(":1234567890"),
        "path": "/",
        # "expires": -1,
    }
    context.add_cookies([cookie])

    return context
