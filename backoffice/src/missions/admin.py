from django.contrib import admin

from . import models


@admin.register(models.Mission)
class MissionAdmin(admin.ModelAdmin):
    exclude = ("version",)
    list_display = ["id", "name", "start_date", "end_date"]
