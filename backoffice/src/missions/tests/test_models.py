from datetime import date
from uuid import UUID

from planner_common import dtos
from pytest import mark

from volunteers.tests.factories import VolunteerFactory

from ..models import Availability
from .factories import AvailabilityFactory, MissionFactory


@mark.django_db
def test_avilability_to_dto_returns_DTO():
    uid = UUID("135de131-093f-41dd-87f7-33dcf0faf359")
    # given I created an availabulity with a specific mission
    mission = MissionFactory()
    availability = AvailabilityFactory(uid=str(uid), mission=mission)

    # when i create a DTO from it
    avail_dto = availability.to_dto()

    # I expect mission DTO is created correctly
    assert isinstance(avail_dto, dtos.Availability)
    assert avail_dto.mission == uid
    assert avail_dto.start_date == mission.start_date
    assert avail_dto.end_date == mission.end_date


@mark.django_db
def test_mission_increments_on_save():
    # given I created a mission
    mission = MissionFactory()
    version = mission.version
    # when I change the date
    new_time = date.today()
    mission.start_date = new_time
    mission.save()
    # I expect the date to be changed and the version to be incremented
    assert mission.start_date == new_time
    assert mission.version == version + 1


@mark.django_db
def test_mission_name_is_changeable():
    # given I created a mission
    mission = MissionFactory()
    name = mission.name
    # when I change the name
    mission.name = "TEST MISSION - 231.0"
    mission.save()
    # I expect the name to be changed
    assert mission.name != name


@mark.django_db
def test_adding_available_volunteer_to_mission_stores_version_in_relation():
    # given I created a mission
    # and there is one volunteer available
    mission = MissionFactory()
    volunteer = VolunteerFactory()
    version = mission.version
    # When i add the volunteer to a mission by adding an Availability relation
    availability = Availability(mission=mission, volunteer=volunteer)
    availability.save()
    # I expect that the version of the mission I added the vol to is present
    assert availability.mission_version == version


@mark.django_db
def test_adding_available_volunteer_to_mission_defaults_to_UNKNOWN():
    # given I created a mission
    # and there is one volunteer available
    mission = MissionFactory()
    volunteer = VolunteerFactory()
    # When i add the volunteer to a mission by adding an Availability relation
    availability = Availability(mission=mission, volunteer=volunteer)
    availability.save()
    # I expect that the availability of the volunteer for this mission to be unknown
    assert availability.availability == Availability.UNKNOWN


@mark.django_db
def test_missions_contain_name():
    # given a new mission
    name = "test_mission"
    mission = MissionFactory(name=name)
    # when a missoin created in the DB
    # then a name should be part of the mission string
    assert mission.name == name
    assert name in str(mission)
