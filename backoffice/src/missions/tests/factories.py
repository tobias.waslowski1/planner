from datetime import timedelta
from random import randint
from typing import Callable, ClassVar
from uuid import uuid4

from factory import Faker, LazyFunction, SubFactory, post_generation
from factory.declarations import LazyAttribute
from factory.django import DjangoModelFactory
from planner_common.test.factories import TypedFactoryMetaClass

from volunteers.tests.factories import VolunteerFactory

from ..models import Availability, Mission


class MissionFactory(DjangoModelFactory, metaclass=TypedFactoryMetaClass[Mission]):
    class Meta:
        model = Mission

    start_date = Faker("date_between", start_date="+30d", end_date="+1y")
    end_date = Faker("date")
    name = Faker("word")

    create: ClassVar[Callable[..., Mission]]
    build: ClassVar[Callable[..., Mission]]

    @post_generation
    def end_date_after_start_date(obj, create, extracted, **kwargs):
        if create:
            # end_date is in between 1 and 60 days after start date
            obj.end_date = obj.start_date + timedelta(randint(1, 60))  # type: ignore


class AvailabilityFactory(
    DjangoModelFactory, metaclass=TypedFactoryMetaClass[Availability]
):
    class Meta:
        model = Availability

    create: ClassVar[Callable[..., Availability]]
    build: ClassVar[Callable[..., Availability]]

    mission = SubFactory(MissionFactory)
    volunteer = SubFactory(VolunteerFactory)
    uid = LazyFunction(lambda: str(uuid4()))
    availability = Faker("random_element", elements=["yes", "no", "maybe", "unknown"])
    mission_version = LazyAttribute(lambda self: self.mission.version)
