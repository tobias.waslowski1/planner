from bs4 import BeautifulSoup
from django.template.response import TemplateResponse
from django.test import Client
from django.urls import reverse
from pytest import mark

from missions.models import Mission
from seawatch_registration.tests.factories import UserFactory

from .factories import MissionFactory


@mark.django_db
def test_missions_admin(client: Client):
    client.force_login(UserFactory.create(is_staff=True, permission="view_mission"))
    assert client.get(reverse("admin:missions_mission_changelist")).status_code == 200


@mark.django_db
def test_mission_with_dates_in_list(client: Client):
    # given there is one mission in the databse
    # and I am logged in with a staff user that is allowed to view missions
    test_mission: Mission = MissionFactory.create()
    client.force_login(UserFactory.create(is_staff=True, permission="view_mission"))
    # when i navigate to the mission view
    response = client.get(reverse("admin:missions_mission_changelist"))
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")
    detailsUrl = reverse(
        "admin:missions_mission_change", kwargs={"object_id": test_mission.id}
    )
    # I expect that there is link on the page that leads me to the mission detail view
    # and  I expect that the table contains the start and end date
    links = html.find_all("a", href=detailsUrl)
    assert len(links) == 1
    assert (
        test_mission.start_date.strftime("%d.%m.%Y")
        in html.find_all("td", class_="field-start_date")[0]
    )
    assert (
        test_mission.end_date.strftime("%d.%m.%Y")
        in html.find_all("td", class_="field-end_date")[0]
    )
