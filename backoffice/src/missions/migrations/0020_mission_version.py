# Generated by Django 3.1.6 on 2021-02-20 18:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("missions", "0019_auto_20210218_1947"),
    ]

    operations = [
        migrations.AddField(
            model_name="mission",
            name="version",
            field=models.IntegerField(default=0),
        ),
    ]
