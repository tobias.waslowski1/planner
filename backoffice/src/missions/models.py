from uuid import UUID, uuid4

from django.db import models
from planner_common import dtos

from seawatch_registration.models import ShortTextField
from volunteers.models import Volunteer


def uuid4_str():
    return str(uuid4())


class Mission(models.Model):
    id = models.AutoField(primary_key=True)
    name = ShortTextField(unique=True)
    start_date = models.DateField()
    end_date = models.DateField()
    version = models.IntegerField(default=0)
    available = models.ManyToManyField(Volunteer, through="Availability")

    def __str__(self):
        return f"Mission {self.name} ({self.start_date} - {self.end_date})"

    def save(self, *args, **kwargs):
        """On save, increment version"""
        self.version += 1
        return super().save(*args, **kwargs)


class Availability(models.Model):
    class Meta:
        verbose_name_plural = "availabilities"

    YES = "YES"
    NO = "NO"
    MAYBE = "MAYBE"
    UNKNOWN = "UNKNOWN"
    "UNKNOWN means that the volunteer hasn't responded yet"

    # TODO align this with the stuff from planner_common
    AVAILABILITY_CHOICES = [
        (YES, "yes"),
        (NO, "no"),
        (MAYBE, "maybe"),
        (UNKNOWN, "unknown"),
    ]
    volunteer = models.ForeignKey(Volunteer, on_delete=models.CASCADE)
    mission = models.ForeignKey(Mission, on_delete=models.CASCADE)
    mission_version = models.IntegerField()
    uid = ShortTextField(default=uuid4_str)
    availability = ShortTextField(default=UNKNOWN, choices=AVAILABILITY_CHOICES)

    def save(self, *args, **kwargs):
        """On save, increment version"""
        if self.mission_version == None:
            self.mission_version = self.mission.version
        return super().save(*args, **kwargs)

    def to_dto(self) -> dtos.Availability:
        return dtos.Availability(
            mission=UUID(self.uid),
            start_date=self.mission.start_date,
            end_date=self.mission.end_date,
            availability=dtos.Availability.Answer(self.availability.lower()),
        )

    def __str__(self):
        return f"{self.volunteer} for {self.mission} -> {self.availability}"
