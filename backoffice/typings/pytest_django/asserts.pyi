from django.http.response import HttpResponse

def assertRedirects(
    response: HttpResponse,
    expected_url: str,
    status_code: int = 302,
    target_status_code: int = 200,
    msg_prefix: str = "",
    fetch_redirect_response: bool = True,
) -> None: ...
def assertContains(
    response: HttpResponse,
    text: str,
    count: int | None = None,
    status_code: int = 200,
    msg_prefix: str = "",
    html: bool = False,
) -> None: ...
def assertNotContains(
    response: HttpResponse,
    text: str,
    status_code: int = 200,
    msg_prefix: str = "",
    html: bool = False,
) -> None: ...
