from typing import Any, Callable, TypeVar, overload

class Task: ...

F = TypeVar("F")

@overload
def task(*args: Any, **kwargs: Any) -> Callable[[F], F]: ...
@overload
def task(_func: F) -> F: ...
