# seawatch_planner

⛴ Software for planning SeaWatch's rescue operations

--

NOTE: This README is a bit out of date. We now use
[invoke](https://www.pyinvoke.org/) as a convenient way to run common commands.
It is included in requirements-dev.txt, so to get started:

1. Install python, version 3.9 strongly preferred and setup a virtualenv.
2. `pip install --upgrade pip`
3. `pip install -r requirements.txt -r requirements-dev.txt`
4. `invoke setup-repository`
5. `invoke --list` or `inv -l` for short, or look into tasks.py to see what the
   commands do.

--

## Development

Install the dev dependencies using `pip install -r requirements-dev.txt` and
set-up pre-commit using `pre-commit install`. This will automatically run the
code formatter on changed files before you commit.

## Configure the app to use dev settings

- If you're using direnv, it will happen automatically
- If not, you can create an empty file ".use-dev-settings" in the repo-root (the
  one that contains the folders "backoffice", "public-backend" and
  "public-client")

### How to test the application

For testing the application, start the app as described before. Then run the following command:

`docker exec -it seawatch-planner_planner-backoffice_1 python manage.py test`

### Alternative

To just run the tests without building and running a lot of containers, execute

`pytest`

To continuously re-run tests while coding, run `./tdd.sh`. It will only re-run
the tests that are affected by the changes you made, so it should give you
a pretty good feedback loop.

### Coding conventions

We decided to use the the following coding conventions.

- Use black to format the code.
- If possible use Djangos generic class based views, for example ListView, CreateView, DetailView, UpdateView and DeleteView.
- ListViews are often represented with tables. For each dataset you have actions like delete this dataset of go to detail view. These actions are represented by links with fontawesome icon in the table and not by buttons.
- General actions like add a new dataset are represented by buttons with fontawesome icons outside of the table.
- Use the following table as an example for the url paths, the naming of the class of the views and the view name itself:

| url path              | class name of view | view name   |
| --------------------- | ------------------ | ----------- |
| ships/                | ShipListView       | ship_list   |
| ships/add             | ShipCreateView     | ship_create |
| ships/<int:pk>/       | ShipDetailView     | ship_detail |
| ships/<int:pk>/edit   | ShipUpdateView     | ship_update |
| ships/<int:pk>/delete | ShipDeleteView     | ship_delete |

## Maintainance

### Malformed Applications

A safety mechanism ensures that applications comply with a schema. The schema defines all attributes and their form. Every time an application is imported from the public-backend, it is compared with the schama. In case a deviation from the schema was detected, the application is stored as a `malformed_application`. It will not be visible in the views that are used to asses applications.

#### Recovering Malformed Applications

Whenever an application is stored as a `malformed_application` a notification will be send to the application maintainers. To inspect and repair the application, the maintainers can use the django admin interface. Once the application was fixed (e.g. by adding or changing attributes) the malformed application can be re-imported. The re-import can be triggered by executing the `recover_applications` command.

```
python manage.py recover_applications
```
