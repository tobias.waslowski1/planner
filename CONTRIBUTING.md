# Contribution/Dev documentation

This needs some content :(

Information on rules of engagement can be found on the
[GitLab wiki page](https://gitlab.com/sea-watch.org/planner/-/wikis/home).

# Setting up your development environment.

## Prerequisites

- [direnv](https://direnv.net/docs/installation.html)
- [nix](https://nixos.org/guides/install-nix.html)
- [lorri](https://github.com/nix-community/lorri) - adds on to nix
- Python

## Other tools that can be useful

- Sqlite - This is already included in MacOS

## Pre-Commit checks

We're using pre-commit to ensure we don't commit preventable errors. To set it up, you
need to follow a few steps:

1. Create a virtual environment using either `python -m venv env` or direnv
2. Install the dev requirements: `pip install -r requirements-dev.txt` NB: pre-commit is
   part of all requirements-dev.txt files, so people using direnv can commit from all
   directories.
3. Run `pre-commit install` to set up the git hooks. This needs to be done once per git
   clone.
