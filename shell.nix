{ sources ? import ./nix/sources.nix }:

let
  overlay = (self: super: {
    niv = self.haskell.lib.compose.overrideCabal
      (drv: { enableSeparateBinOutput = false; })
      super.haskellPackages.niv;
  });
  pkgs = import sources.nixpkgs { overlays = [overlay]; };
  python = pkgs.python39;
  playwright_version = "1.22.0";
  playwright-browsers = import ./nix/playwright-browsers.nix {
    inherit playwright_version;
    data_sha256 = "sha256:1jbq5xdklw2n8cqxjv912q124wmlqkwv6inlf6qw76x9ns16lv18";
  };
in
  pkgs.mkShell ({
    packages = [
      pkgs.niv

      (pkgs.poetry.override { inherit python; })
      pkgs.pre-commit
      pkgs.black
      (python.withPackages (py: [
        py.isort
        py.invoke
        py.requests
        py.bandit
      ]))

      (pkgs.writeShellScriptBin "pytest-watch" ''
        echo "pytest-watch is not supported upstream anymore, we are switching to watchfiles" >&2
        exit 1
      '')
      (pkgs.writeShellScriptBin "ptw" ''
        echo "pytest-watch is not supported upstream anymore, we are switching to watchfiles" >&2
        exit 1
      '')

      pkgs.nodejs-16_x
      pkgs.nodePackages.prettier
      pkgs.nodePackages.eslint
      pkgs.nodePackages.typescript  # Needed for editor integration

      # Build dependencies for some python libraries
      pkgs.openldap
      pkgs.cyrus_sasl.dev
      pkgs.openssl
      pkgs.postgresql

      # Deployment
      (pkgs.writeShellScriptBin "terraform" ''
        set -euo pipefail

        P="seawatch-crewingdb-personal"
        export TF_HTTP_USERNAME="$(set -e; gopass cat "$P/gitlab_username")"
        export TF_HTTP_PASSWORD="$(set -e; gopass cat "$P/gitlab_personal_access_token")"

        export TF_VAR_hcloud_token="$(set -e; gopass cat "seawatch-crewingdb/staging/hcloud_token")"
        export TF_VAR_deploy_ssh_key_pub="$(set -e; gopass cat "seawatch-crewingdb/staging/deploy_ssh_key.pub")"

        exec ${pkgs.terraform}/bin/terraform "$@"
      '')
      pkgs.ansible
    ];
  } // (
    if builtins.currentSystem == "x86_64-linux" then {
      "PLANNER_PLAYWRIGHT_VERSION" = playwright_version;
      "PLANNER_PLAYWRIGHT_CHROMIUM" = playwright-browsers.chromium;
      "PLANNER_PLAYWRIGHT_FIREFOX" = playwright-browsers.firefox;
    } else {})
    )
