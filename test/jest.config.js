const PUBLIC_FRONTEND_URL = process.env.PUBLIC_FRONTEND_URL || "http://localhost:9001";
const PUBLIC_BACKEND_URL = process.env.PUBLIC_BACKEND_URL || "http://localhost:9001";
const BACKOFFICE_URL = process.env.BACKOFFICE_URL || "http://localhost:9000";
const WEBMAIL_URL = process.env.WEBMAIL_URL || "http://localhost:9085";
const BACKOFFICE_ADMIN_USERNAME = process.env.BACKOFFICE_ADMIN_USERNAME || "admin";
const BACKOFFICE_ADMIN_PASSWORD = process.env.BACKOFFICE_ADMIN_PASSWORD || "admin";

module.exports = {
  preset: "jest-puppeteer",
  testTimeout: 2 * 60 * 1000,
  setupFilesAfterEnv: ["./jest.setup.after.env.js"],
  globals: {
    PUBLIC_FRONTEND_URL: PUBLIC_FRONTEND_URL,
    PUBLIC_BACKEND_URL: PUBLIC_BACKEND_URL,
    BACKOFFICE_URL: BACKOFFICE_URL,
    WEBMAIL_URL: WEBMAIL_URL,
    BACKOFFICE_ADMIN_USERNAME,
    BACKOFFICE_ADMIN_PASSWORD,
  },
};
