#Running the tests

- Run integration tests against local code:
  - `npm run compose` starts application on ports 9000 and 9001
    - This builds the images locally and couuld take a while. Wait till you get messages
      from the different services and things slow down a bit.
  - `npm run test` runs tests against it
  - TODO: mount the code (readonly) so we don't need to rebuild the containers all the
    time
- The following ones pull images from the repo, so you will need to log in using
  `npm run login`
- Run tests with CI-built images for the current branch
  - `npm run test_ci_current`
  - Use this to reproduce failed pipelines.
  - This uses the local test code but the production code images from CI.
  - This will keep the containers running so you can explore it in a browser or
    modify/re-run tests with `npm run test`. Use `test_ci_current_once` to exit after
    the tests
- `test_ci` and `test_ci_once` run the currently checked out tests against the CI images
  built for branch `$BRANCH`. Not sure if this is useful.

## How they run in CI
