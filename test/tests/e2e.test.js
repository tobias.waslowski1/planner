page = undefined;
context = undefined;

const jestscreenshot = require("@jeeyah/jestscreenshot");
const path = require("path");

const BackofficeContext = require("../util/backofficeContext");
const testingApi = require("../util/testingApi");
const mail = require("../util/mail");

async function $evalElement(node, selector, pageFunction) {
  const element = await node.$(selector);
  const result = await element.evaluateHandle(pageFunction);
  return result.asElement();
}

async function $shadowroot(node, selector) {
  return await $evalElement(node, selector, (n) => n.shadowRoot);
}

async function submitForm(name, { otherCertificates } = {}) {
  const context = await await browser.createIncognitoBrowserContext();
  const page = await context.newPage();
  await jestscreenshot.init({
    page: page,
    dirName: __dirname,
    scriptName: path.basename(__filename).replace(".js", ""),
  });

  await page.goto(PUBLIC_FRONTEND_URL);
  await page.waitForSelector("application-form");

  const form = await $shadowroot(page, "application-form");

  //////////////
  // Fill fields

  // First Name
  const firstNameInput = await form.$(`input[name='first_name']`);
  await firstNameInput.type(name);

  // Last Name
  const lastNameInput = await form.$('input[name="last_name"]');
  await lastNameInput.type("lastname_" + name);

  // Phone Number
  const phoneNumberInput = await form.$('input[name="phone_number"]');
  await phoneNumberInput.type("+491634884421");

  // Email
  const emailInput = await form.$('input[name="email"]');
  await emailInput.type(name + "@example.com");

  // Date of Birth
  const dateOfBirthElement = await $shadowroot(
    form,
    'date-picker[name="date_of_birth"]'
  );
  await (await dateOfBirthElement.$('[name="year"]')).select("2000");
  await (await dateOfBirthElement.$('[name="month"]')).select("01");
  await (await dateOfBirthElement.$('[name="day"]')).select("01");

  // Gender
  const genderInput = await form.$('[name="gender"]');
  await genderInput.select("none_other");

  // Nationalities
  const nationalitiesElement = await $shadowroot(form, '[name="nationalities"]');
  const nationalitiesInput = await nationalitiesElement.$("input");
  await nationalitiesInput.type("tajik");
  await nationalitiesInput.press("Enter");

  // Spoken Languages
  const languageElem = await $shadowroot(form, "[name=spoken_languages]");
  const languageInput = await languageElem.$(".choices");
  await languageInput.click();
  await (await languageInput.asElement().$("[data-value=deu]")).click();
  await (await languageInput.asElement().$("[data-value=ara-levantine]")).click();

  // Fill position
  const positionsElem = await $shadowroot(form, "[name=positions]");
  const positionsInput = await positionsElem.$(".choices");
  await positionsInput.click();
  await (await positionsInput.asElement().$("[data-value=guest-coord]")).click();

  // Certificates
  if (otherCertificates) {
    const certificatesElem = await $shadowroot(form, "[name=certificates]");
    const certificatesInput = await certificatesElem.$(".choices");
    await certificatesInput.click();
    await (await certificatesInput.asElement().$("[data-value=other]")).click();

    const otherCertificatesInput = await form.$("#input_other_certificate");
    await otherCertificatesInput.type(otherCertificates);
  }

  // Motivation
  const motivationElement = await $shadowroot(form, '[name="motivation"]');
  const motivationInput = await motivationElement.$("textarea");
  await motivationInput.type("motivated!!");

  // Qualification
  const qualificationElement = await $shadowroot(form, '[name="qualification"]');
  const qualificationInput = await qualificationElement.$("textarea");
  await qualificationInput.type("qualificated!!");

  /////////////////////////////////
  // Submit and verify

  await Promise.all([
    (await form.$('[type="submit"]')).click(),
    page.waitForNavigation(),
  ]);

  // ensure it's actually the success page
  const headlineElement = await page.$("h1");
  const headline = await headlineElement.evaluate((node) => node.innerText);
  expect(headline.toLowerCase()).toContain("thanks");

  context.close();
}

describe("Application process", () => {
  it("imports applications to backoffice after they have been submitted", async () => {
    const backoffice = new BackofficeContext();

    await Promise.all([
      backoffice.ensureLoggedIn(),
      // GIVEN two applications
      submitForm("Anna"),
      submitForm("Peter"),
    ]);

    // WHEN I import the into the backoffice
    await testingApi.import_applications();

    // THEN they should show up in the list
    const p = await backoffice.newPage(__dirname, __filename);
    await p.goto(BACKOFFICE_URL + "/applications/");
    html = await p.$eval("table", (e) => e.innerHTML);
    expect(html).toContain("Anna");
    expect(html).toContain("Peter");

    await backoffice.close();
  });

  it('processes applications with free-text "other certificate"', async () => {
    const backoffice = new BackofficeContext();

    const random_number = Math.floor(Math.random() * 10000);
    const name = `Charlotte${random_number}`;
    await Promise.all([
      backoffice.ensureLoggedIn(),
      // GIVEN an application with a free text certificate
      submitForm(name, {
        otherCertificates: "superspecial qualification cert",
      }),
    ]);

    // WHEN I import the into the backoffice
    await testingApi.import_applications();

    // THEN they should show up in the list
    const p = await backoffice.newPage(__dirname, __filename);
    await p.goto(BACKOFFICE_URL + "/applications/?sort=-created");
    const table = await p.$("table");
    const [link] = await table.$x(`//a[contains(., '${name}')]`);
    await Promise.all([p.waitForNavigation(), link.click()]);
    const pageContent = await p.evaluate(() => document.body.textContent);
    expect(pageContent).toContain("Personal Data");
    // We just want to make sure the application imports properly, display is tested in the
    // backoffice test

    await backoffice.close();
  });

  it("can redirect users to the seawatch position descriptions", async () => {
    const context = await browser.createIncognitoBrowserContext();
    const page = await context.newPage();
    await page.goto(PUBLIC_FRONTEND_URL, { waitUntil: "domcontentloaded" });

    const form = await $shadowroot(page, "application-form");

    const positionsElement = await $shadowroot(form, '[name="positions"]');
    const positionsInput = await positionsElement.$("input");
    await positionsInput.type("Cook");
    await positionsInput.press("Enter");
    await positionsInput.type("Chief Engineer");
    await positionsInput.press("Enter");

    const buttonsList = await (
      await $shadowroot(form, '[name="qualification"]')
    ).$(".buttonlist");
    const buttons = await buttonsList.$$("a");

    expect(buttons).toHaveLength(2);
    const [cookLabel, chiefEngineerLabel] = await Promise.all([
      buttons[0].evaluate((n) => n.textContent),
      buttons[1].evaluate((n) => n.textContent),
    ]);
    expect(cookLabel).toBe("Cook");
    expect(chiefEngineerLabel).toBe("Chief Engineer");

    // Actually load the seawatch homepage.
    // This is somewhat brittle and a little slow, but it already caught bad links multiple times.

    const [popup] = await Promise.all([
      new Promise((resolve) =>
        page.once("popup", async (p) => {
          await p.waitForXPath('//h2[text()="Cook"]');
          resolve(p);
        })
      ),
      buttons[0].click(),
    ]);

    const title = await popup.title();
    expect(title).toEqual("Join the Crew • Sea-Watch e.V.");
    const url = await popup.evaluate("location.href");
    expect(url).toMatch(/^https:\/\/sea-watch.org\//);
    await popup.waitForXPath("//h2[text()='Cook']", { visible: true });

    await context.close();
  });

  afterAll(() => {
    jestscreenshot.cleanup(() => {});
  });
});

describe("send email", () => {
  it("sends an email when I request availabilities", async () => {
    const backoffice = new BackofficeContext();
    await backoffice.ensureLoggedIn();
    const backofficePage = await backoffice.newPage(__dirname, __filename);

    const random_number = Math.floor(Math.random() * 10000);
    const first_name = "example" + random_number;
    const email = "example." + random_number + "@test";

    await testingApi.delete_all_volunteers();
    await testingApi.create_volunteer({
      first_name: first_name,
      last_name: "foo",
      email: email,
    });

    await testingApi.delete_all_missions();
    await testingApi.create_mission({
      name: "Jan-Mission",
      start_date: "2025-01-01",
      end_date: "2025-03-01",
    });
    await testingApi.create_mission({
      name: "Mar-Mission",
      start_date: "2025-03-01",
      end_date: "2025-06-01",
    });

    await backofficePage.goto(BACKOFFICE_URL + "/volunteers");
    await backofficePage.waitForXPath(`//tr[contains(., "${first_name}")]`);
    const requestAvailabilitiesButton = await backofficePage.$x(
      "//a[contains(., 'Request Availabilities')]"
    );
    await requestAvailabilitiesButton[0].click();
    await backofficePage.waitForXPath('/html[contains(., "Request Availabilities")]');

    const confirmButton = await backofficePage.$(".button-primary");
    await confirmButton.click();

    const requestMail = await mail.fetch_matching(
      (m) =>
        m.toAddresses.includes(email) && m.subject == "Mission availability request"
    );
    expect(requestMail.fromAddress).toBe("info@public-backend.test");
    expect(requestMail.body).toContain(`Dear ${first_name},`);
    expect(requestMail.body).toContain("to know if you are available");
    const urlRegex = /\bhttps?:\/\/\S+\b/;
    expect(requestMail.body).toMatch(urlRegex);
    const urlsInEmail = requestMail.body.match(urlRegex);
    expect(urlsInEmail[0]).toContain("public-backend");
    const availabilityFormPage = await (
      await browser.createIncognitoBrowserContext()
    ).newPage();
    await availabilityFormPage.goto(
      urlsInEmail[0].replace(/^https?:\/\/[^\/]+/, PUBLIC_BACKEND_URL)
    );
    await availabilityFormPage.waitForSelector("availability-form");
    const form = await $shadowroot(availabilityFormPage, "availability-form");

    // Make sure that one of our headers is "Availability"
    const availabilitiesHeader = await form.$$("h2");
    const headers = await Promise.all(
      availabilitiesHeader.map(
        async (header) =>
          await availabilityFormPage.evaluate((element) => element.textContent, header)
      )
    );
    expect(headers).toContain("Availability");

    // Now we know for sure that this is our availability form
    const labels = await form.$$eval("label", (ls) =>
      // The labels have linebreaks and non-breakable spaces, so we just replace all whitespace runs
      // by a single space
      ls.map((l) => l.innerText.replace(/[\s\u00A0]+/g, " "))
    );
    expect(labels).toEqual([
      "January 1st, 2025 to March 1st, 2025",
      "March 1st, 2025 to June 1st, 2025",
    ]);
    const selects = await form.$$("select");
    selects.forEach(async (select) => await select.select("yes"));
    await Promise.all([
      (await form.$('input[type="submit"]')).click(),
      availabilityFormPage.waitForNavigation(),
    ]);
    expect(await availabilityFormPage.$eval("h1", (h) => h.innerText)).toContain(
      "THANK YOU"
    );

    const confirmationMail = await mail.fetch_matching(
      (m) =>
        m.toAddresses.includes(email) && m.subject == "Mission availability confirmed"
    );
    expect(confirmationMail.fromAddress).toBe("info@public-backend.test");
    expect(confirmationMail.subject).toBe("Mission availability confirmed");
    expect(confirmationMail.body).toContain(
      "thank you for letting us know when you are available."
    );

    availabilityFormPage.close();
    await backoffice.close();
  });
  afterAll(() => {
    jestscreenshot.cleanup(() => {});
  });
});
