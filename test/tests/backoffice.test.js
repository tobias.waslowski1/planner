page = undefined;
context = undefined;

const BackofficeContext = require("../util/backofficeContext");
const testingApi = require("../util/testingApi");
const mail = require("../util/mail");

describe("Application editing", () => {
  let backoffice = new BackofficeContext();

  beforeAll(async () => {
    await backoffice.ensureLoggedIn();
  });

  afterAll(async () => {
    await backoffice.close();
  });

  it("allows updating data", async () => {
    const random_number = Math.floor(Math.random() * 10000);
    const data = {
      first_name: `asdf${random_number}`,
      last_name: "Sloanaxc",
      phone_number: "+12125552368",
      email: "a@b.de",
      date_of_birth: "2020-01-01",
      gender: "male",
      nationalities: ["AE"],
      languages: { ita: 0 },
      positions: ["Bosun", "Deck rating"],
      certificates: ["STCW-VI/2-1", "other: foo"],
    };

    const { id: application_id } = await testingApi.create_application(data);
    const page = await backoffice.applicationDetail(
      await backoffice.newPage(__dirname, __filename),
      application_id
    );
    const form = await page.dataForm();

    expect(await form.first_name).toBe(data.first_name);
    expect(await form.last_name).toBe(data.last_name);
    expect(await form.phone_number).toBe(data.phone_number);
    expect(await form.email).toBe(data.email);
    expect(await form.date_of_birth).toBe(data.date_of_birth);
    expect(await form.gender).toBe(data.gender);
    expect(await form.nationalities).toEqual([["AE", "United Arab Emirates"]]);
    expect(await form.languages).toEqual([["ita", "Italian"]]);
    expect(await form.language_levels).toEqual([["Italian", "0"]]);
    expect((await form.positions).sort()).toEqual(data.positions);
    expect(await form.certificates).toEqual([
      [
        "STCW-VI/2-1",
        "STCW VI/2 (1) Proficiency in Survival Craft and Rescue Boats other than Fast Rescue Boats",
      ],
      ["other", "Other (free text)"],
    ]);
    expect(await form.other_certificate).toBe("foo");

    new_data = {
      first_name: `new_first${random_number}`,
      last_name: "new_last",
      phone_number: "+49555123123",
      email: "a_new@b.de",
      date_of_birth: "1990-01-01",
      gender: "prefer_not_say",
      nationalities: ["GB"],
      languages: { eng: 2 },
      positions: ["Protection Officer"],
      certificates: ["STCW-VI/4-1", "other"],
      certificates_other: "new other",
    };

    await form.set_first_name(new_data.first_name);
    await form.set_last_name(new_data.last_name);
    await form.set_phone_number(new_data.phone_number);
    await form.set_email(new_data.email);
    await form.set_date_of_birth(new_data.date_of_birth);
    await form.set_gender(new_data.gender);
    await form.set_nationalities(new_data.nationalities);
    await form.set_languages(new_data.languages);
    await form.set_positions(new_data.positions);
    await form.set_certificates(new_data.certificates);
    await form.set_other_certificate(new_data.certificates_other);

    await form.submit();
    page.close();

    // Open a new tab to make sure we have actually saved the data and don't get some chached stuff
    const new_page = await backoffice.applicationDetail(
      await backoffice.newPage(__dirname, __filename),
      application_id
    );
    const new_form = await new_page.dataForm();
    expect(await new_form.first_name).toBe(new_data.first_name);
    await new_page.close();
  });

  it("allows accepting applications", async () => {
    // GIVEN an application
    const random_number = Math.floor(Math.random() * 10000);
    const first_name = `lemon${random_number}`;
    const application_data = {
      first_name,
      last_name: "grab",
      phone_number: "+12125552368",
      email: `a${random_number}@b.de`,
      date_of_birth: "2020-01-01",
      gender: "male",
      nationalities: ["AE"],
      languages: { ita: 0 },
      positions: ["Bosun", "Deck rating"],
      certificates: ["STCW-VI/2-1", "other: foo"],
    };
    const { id: application_id } = await testingApi.create_application(
      application_data
    );

    // WHEN I accept the application
    const page = await backoffice.newPage(__dirname, __filename);
    const applicationDetails = await backoffice.applicationDetail(page, application_id);

    await applicationDetails.decidePosition("Bosun", "accept");
    await applicationDetails.decidePosition("Deck rating", "reject");
    await applicationDetails.accept();

    // THEN it shows up in the volunteer list
    const volunteersList = await backoffice.volunteersList(page);
    await volunteersList.page.waitForXPath(`/html[contains(., "${first_name}")]`);

    //TODO pending feature flag enable: send_emails_on_decisions
    // AND an email is sent to the applicant
    const acceptanceMail = await mail.fetch_matching(
      (mail) =>
        mail.toAddresses.includes(application_data.email) &&
        mail.subject.startsWith("Accepted")
    );
    expect(acceptanceMail.toAddresses).toEqual([application_data.email]);
    expect(acceptanceMail.subject).toBe(
      "Accepted – Your Application for Sea-Watch Crew"
    );
    expect(acceptanceMail.body).toContain(
      "confirmation that you are now part of our crewing pool"
    );

    await page.close();
  });

  it("allows deleting applications", async () => {
    // GIVEN an application
    const random_number = Math.floor(Math.random() * 10000);
    const first_name = `jake${random_number}`;
    const application_data = {
      first_name,
      last_name: "theDog",
      phone_number: "+12125552368",
      email: `a${random_number}@b.de`,
      date_of_birth: "2020-01-01",
      gender: "male",
      nationalities: ["AE"],
      languages: { ita: 0 },
      positions: ["Bosun", "Deck rating"],
      certificates: ["STCW-VI/2-1", "other: foo"],
    };
    const { id: application_id } = await testingApi.create_application(
      application_data
    );

    // WHEN I delete the application
    const page = await backoffice.newPage(__dirname, __filename);
    const applicationDetails = await backoffice.applicationDetail(page, application_id);

    await applicationDetails.delete();

    // THEN The application is gone
    await page.goto(`${BACKOFFICE_URL}/applications/${application_id}/`);
    await page.waitForXPath(
      `/html[contains(., "The requested resource was not found")]`
    );

    //TODO pending feature flag enable: send_emails_on_decisions
    // AND an email is sent
    const deletionMail = await mail.fetch_matching((mail) =>
      mail.toAddresses.includes(application_data.email)
    );
    expect(deletionMail.toAddresses).toEqual([application_data.email]);
    expect(deletionMail.subject).toBe("Update – Your application for Sea-Watch Crew");
    expect(deletionMail.body).toContain(
      "after carefully reviewing your application we cannot accept"
    );

    await page.close();
  });
});
