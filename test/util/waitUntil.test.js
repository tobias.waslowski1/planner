const waitUntil = require("./waitUntil");

describe("waituntil", () => {
  it("returns", async () => {
    expect(await waitUntil(() => 3, 1, 0)).toBe(3);
  });
  it("is somewhat accurate", async () => {
    let x = 1;
    const fn = async () => {
      if (x++ == 3) {
        return "yes";
      } else {
        throw "fail";
      }
    };
    expect(await waitUntil(fn, 3, 0)).toBe("yes");
  });
  it("times out", async () => {
    let count = 0;
    const fn = async () => {
      count += 1;
      throw new Error("yip");
    };
    await expect(waitUntil(fn, 4, 0)).rejects.toThrow("yip");
    expect(count).toBe(4);
  });
  it("delays", async () => {
    let first = null,
      last = null;
    const fn = async () => {
      if (first == null) {
        first = Date.now();
      } else {
        last = Date.now();
      }
      throw null;
    };
    try {
      await waitUntil(fn, 5, 60);
    } catch (e) {}

    expect(last - first).toBeGreaterThan(239);
  });
});
