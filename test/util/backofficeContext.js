const jestscreenshot = require("@jeeyah/jestscreenshot");
const path = require("path");
const ApplicationDetailPage = require("./backoffice/applicationDetailPage");
const VolunteersList = require("./backoffice/volunteersList");

module.exports = class BackofficeContext {
  constructor() {
    this.context = null;
    this.logged_in = false;
  }

  async close() {
    if (this.context) {
      await this.context.close();
    }
  }

  async ensureContext() {
    if (!this.context) {
      this.context = await browser.createIncognitoBrowserContext();
    }
  }

  async ensureLoggedIn() {
    await this.ensureContext();
    if (!this.logged_in) {
      const p = await this.newPage(__dirname, __filename);
      await p.goto(BACKOFFICE_URL, { waitUntil: "domcontentloaded" });
      await p.type("#id_username", BACKOFFICE_ADMIN_USERNAME);
      await p.type("#id_password", BACKOFFICE_ADMIN_PASSWORD);
      await Promise.all([
        p.waitForNavigation(),
        p.click('input[type="submit"], button[type="submit"]'),
      ]);
      await expect(p).toMatch("Open Applications");
      p.close();
      this.logged_in = true;
    }
  }

  async newPage(dirname, filename) {
    if (!dirname || !filename) {
      throw "Please call newPage with (__dirname, __filename)";
    }
    const page = await this.context.newPage();
    await jestscreenshot.init({
      page: page,
      dirName: dirname,
      scriptName: path.basename(filename).replace(".js", ""),
    });
    return page;
  }

  async applicationDetail(page, id) {
    await page.goto(`${BACKOFFICE_URL}/applications/${id}/`);
    return new ApplicationDetailPage(page);
  }

  async volunteersList(page) {
    await page.goto(`${BACKOFFICE_URL}/volunteers/`);
    return new VolunteersList(page);
  }
};
