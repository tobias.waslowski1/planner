const fetch = require("node-fetch");

const { throw_on_bad_response } = require("./net");
const waitUntil = require("./waitUntil");

async function fetch_latest() {
  return await waitUntil(
    async () => {
      const webmailResponse = await fetch(WEBMAIL_URL + "/mail");
      const emails = await webmailResponse.json();
      expect(emails).toHaveProperty("mailItems");
      const mailItems = emails.mailItems;
      expect(mailItems).toBeInstanceOf(Array);
      const mail = mailItems.slice(-1)[0];
      expect(mail).toBeDefined();
      return mail;
    },
    15,
    500
  );
}

async function fetch_matching(predicate) {
  return await waitUntil(
    async () => {
      const webmailResponse = await fetch(WEBMAIL_URL + "/mail");
      const emails = await webmailResponse.json();
      expect(emails).toHaveProperty("mailItems");
      const mailItems = emails.mailItems;
      expect(mailItems).toBeInstanceOf(Array);
      const match = mailItems.find(predicate);
      if (match == undefined) {
        throw `no mail matches predicate, got the following: \n${mailItems
          .map((m) => `${m.subject} (${m.fromAddress} -> ${m.toAddresses.join(", ")})`)
          .join("\n")}`;
      }
      return match;
    },
    15,
    500
  );
}

async function flush() {
  const response = await fetch(WEBMAIL_URL + "/mail", {
    method: "DELETE",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ pruneCode: "all" }),
  });
  await throw_on_bad_response(response, "flush mailslurper");
}

module.exports = {
  fetch_latest,
  flush,
  fetch_matching,
};
