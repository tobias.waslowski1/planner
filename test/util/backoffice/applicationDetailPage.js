const puppeteer = require("puppeteer");

async function get_shown_choices(select) {
  const inner = await select.evaluateHandle((s) => s._choices.containerInner.element);
  if (!inner) {
    throw `failed to get _choices.containerInner on ${select}`;
  }
  return await inner.$$eval(".choices__item", (items) =>
    items.map((i) => [i.dataset.value, i.childNodes[0].textContent])
  );
}

async function replace_text_in_input(elementHandle, value) {
  await elementHandle.click({ clickCount: 3 });
  await elementHandle.type(value);
}

async function set_choices(select, values) {
  const possible_values = new Set(
    await select.evaluate((s) => s._choices.config.choices.map((c) => c.value))
  );
  for (const v of values) {
    if (!possible_values.has(v)) {
      throw `Unknown value "${v}"`;
    }
  }
  await select.evaluate((select, values) => {
    select._choices.setChoiceByValue(values);
  }, values);
}

class ApplicationDataForm {
  constructor(page, form) {
    this.page = page;
    this.form = form;
  }

  async submit() {
    try {
      await Promise.all([
        this.page.waitForResponse((r) => r.status() == 302, { timeout: 2000 }),
        (await this.form.$('input[type=submit][value="Save changes"]')).click(),
      ]);
    } catch (e) {
      if (e.name == "TimeoutError") {
        console.error(
          "submitting application data form timed out, maybe the data was invalid?"
        );
      }
      throw e;
    }
  }

  get first_name() {
    return this.form.$eval("input[name=first_name]", (e) => e.value);
  }
  async set_first_name(value) {
    await replace_text_in_input(await this.form.$("input[name=first_name]"), value);
  }

  get last_name() {
    return this.form.$eval("input[name=last_name]", (e) => e.value);
  }
  async set_last_name(value) {
    await replace_text_in_input(await this.form.$("input[name=last_name]"), value);
  }

  get phone_number() {
    return this.form.$eval("input[name=phone_number]", (e) => e.value);
  }
  async set_phone_number(value) {
    await replace_text_in_input(await this.form.$("input[name=phone_number]"), value);
  }

  get email() {
    return this.form.$eval("input[name=email]", (e) => e.value);
  }
  async set_email(value) {
    await replace_text_in_input(await this.form.$("input[name=email]"), value);
  }

  get date_of_birth() {
    return this.form.$eval("input[name=date_of_birth]", (e) => e.value);
  }
  async set_date_of_birth(value) {
    await replace_text_in_input(await this.form.$("input[name=date_of_birth]"), value);
  }

  get gender() {
    return this.form.$eval("select[name=gender]", (e) => e.value);
  }
  async set_gender(value) {
    const select = await this.form.$("select[name=gender]");
    if (!(await select.$(`option[value=${value}]`))) {
      throw `Unknown gender "${value}"`;
    }
    return await select.select(value);
  }

  get nationalities() {
    return this.form.$("select[name=nationalities]").then(get_shown_choices);
  }
  async set_nationalities(values) {
    await set_choices(await this.form.$("select[name=nationalities"), values);
  }

  get languages() {
    return this.form.$("select[name=languages]").then(get_shown_choices);
  }
  get language_levels() {
    return this.form.$$eval(".language-mapper-level", (levels) =>
      levels.map((level) => [
        level.querySelector("label").innerText,
        level.querySelector("select").value,
      ])
    );
  }
  async set_languages(values) {
    await set_choices(await this.form.$("select[name=languages]"), Object.keys(values));
    for (const [code, points] of Object.entries(values)) {
      const select = await this.form.$(
        `.language-mapper-level[data-language="${code}"] select`
      );
      if (!(await select.$(`option[value="${points}"]`))) {
        throw `Language level select doesn't have "${points}"`;
      }
      await select.select(`${points}`);
    }
  }

  get positions() {
    return (
      this.form
        .$("select[name=positions]")
        .then(get_shown_choices)
        // We don't care about the position ids here
        .then((ps) => ps.map(([_, pos]) => pos))
    );
  }
  async set_positions(values) {
    const select = await this.form.$("select[name=positions]");
    const ids_by_name = Object.fromEntries(
      await select.evaluate((s) =>
        s._choices.config.choices.map((c) => [c.label, c.value])
      )
    );
    await set_choices(
      select,
      values.map((name) => ids_by_name[name])
    );
  }

  get certificates() {
    return this.form.$("select[name=certificates_csv]").then(get_shown_choices);
  }
  async set_certificates(values) {
    await set_choices(await this.form.$("select[name=certificates_csv]"), values);
  }
  get other_certificate() {
    return (async () => {
      const input = await this.form.$("input[name=certificates_csv__other]");
      if (await input.evaluate((i) => i.hasAttribute("aria-hidden"))) {
        return null;
      } else {
        return await input.evaluate((i) => i.value);
      }
    })();
  }
  async set_other_certificate(value) {
    const input = await this.form.$("input[name=certificates_csv__other]");
    if (await input.evaluate((i) => i.hasAttribute("aria-hidden"))) {
      throw "cannot set other certificate, input is hidden";
    } else {
      await replace_text_in_input(input, value);
    }
  }
}

class ApplicationDetailPage {
  constructor(page) {
    this.page = page;
  }

  async close() {
    return this.page.close(...arguments);
  }

  async dataForm() {
    return new ApplicationDataForm(
      this.page,
      await this.page.$(".application-data form")
    );
  }

  async decidePosition(name, decision) {
    const positionRecs = await this.page.$$("#position-recommendations form");
    const formsWithText = await Promise.all(
      positionRecs.map((e) =>
        e.evaluate((handle) => handle.textContent).then((text) => [e, text])
      )
    );
    const form = formsWithText.find(([_, t]) => t.includes(name))[0];
    if (!form) {
      throw `Couldn't find label for position: "${name}"`;
    }

    const select = await form.$(`select`);
    if (!select) {
      throw `Couldn't find select for position "${name}"`;
    }

    return await Promise.all([select.select(decision), this.page.waitForNetworkIdle()]);
  }

  async accept() {
    const acceptButton = await this.page.$("button#acceptApplication");
    if (!acceptButton) {
      throw "Couldn't find application accept button";
    }
    if ((await acceptButton.evaluate((h) => h["ariaDisabled"])) == "true") {
      throw "accepting not possible, button is disabled";
    }

    const dialogButtonSelector = "#acceptApplicationDialog button[value=accept]";
    await Promise.all([
      acceptButton.click("button#acceptApplication"),
      this.page.waitForSelector(dialogButtonSelector, {
        visible: true,
      }),
    ]);

    await Promise.all([
      this.page.click(dialogButtonSelector),
      this.page.waitForNavigation(),
    ]);

    await this.page.waitForXPath('/html[contains(., "Application accepted")]');
  }

  async delete() {
    const deleteButtonSelector = "form#deleteApplicationForm button";
    await Promise.all([
      this.page.click("button#deleteApplication"),
      this.page.waitForSelector(deleteButtonSelector, { visible: true }),
    ]);

    await Promise.all([
      this.page.click(deleteButtonSelector),
      this.page.waitForNavigation(),
    ]);

    const messageDialog = await this.page.$("dialog#messageDialog");
    if (!messageDialog) {
      throw "Didn't find message dialog after deleting application";
    }
    if (
      !(await messageDialog.evaluate((e) => e.textContent)).includes(
        "'s application has been deleted."
      )
    ) {
      throw "Didn't find deletion confirmation message in message dialog";
    }
  }
}

module.exports = ApplicationDetailPage;
