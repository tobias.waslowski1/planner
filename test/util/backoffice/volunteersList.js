const puppeteer = require("puppeteer");

class VolunteersList {
  constructor(page) {
    this.page = page;
  }

  async close() {
    return this.page.close(...arguments);
  }
}

module.exports = VolunteersList;
