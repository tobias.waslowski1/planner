async function waitUntil(asyncFn, tries, delay) {
  let error = null;
  while (tries > 0) {
    tries = tries - 1;
    try {
      return await asyncFn();
    } catch (e) {
      error = e;
      await new Promise((r) => setTimeout(r, delay));
    }
  }
  throw error;
}

module.exports = waitUntil;
