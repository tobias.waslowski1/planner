const fetch = require("node-fetch");
const { throw_on_bad_response } = require("./net");

async function call_with_data(endpoint, data) {
  const response = await fetch(BACKOFFICE_URL + "/_testing/" + endpoint + "/", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify(data),
  });
  await throw_on_bad_response(response, endpoint);
  const body = await response.text();
  if (body != "") {
    return JSON.parse(body);
  }
}

function call(endpoint) {
  return call_with_data(endpoint, {});
}

module.exports = {
  import_applications: () => call("import_applications"),

  create_application: (data) => call_with_data("create_application", data),

  create_volunteer: (data) => call_with_data("create_volunteer", data),
  delete_all_volunteers: () => call("delete_all_volunteers"),

  create_mission: (data) => call_with_data("create_mission", data),
  delete_all_missions: () => call("delete_all_missions"),
};
