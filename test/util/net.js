module.exports = {
  throw_on_bad_response: async (response, identifier) => {
    if (!response.ok) {
      throw {
        error: "" + identifier + " isn't ok",
        status: response.status,
        text: await response.text(),
      };
    } else if (response.redirected) {
      throw "" + identifier + " redirected";
    }
  },
};
