process.on("unhandledRejection", console.warn);

module.exports = {
  launch: {
    headless: process.env.HEADLESS
      ? process.env.HEADLESS.toLowerCase() === "true"
      : true,
    slowMo: process.env.SLOWMO || 0,
    args: process.env.PUPPETEER_NO_SANDBOX
      ? ["--no-sandbox", "--disable-setuid-sandbox"]
      : [],
  },
};
