# Domain Concepts

This file wants to be a list of important domain concepts with very brief descriptions.

- _Mission_ In the narrowest sense, a ship and it's crew going out to sea to rescue
  people. In a broader sense, the neccessary work to support that specific mission.
- _Application_ When someone demonstrates interest to become a volunteer, they submit an
  application.
- _Volunteer_ Person wowrking for seawatch in their free time. Someone whose application
  was accepted. Might take part in missions, ship maintenance or not. Might not take
  part in any seawatch activities for some time.
- Availability Connects Volunteers with planned Missions. Non-binding statement whether
  a volunteer has the possibility of participating in a specific planned mission.
