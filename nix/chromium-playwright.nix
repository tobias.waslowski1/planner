{ pkgs ? import <nixpkgs> { }, rev }:

let
  inherit (pkgs) fetchurl lib;
  inherit (pkgs.lib) optional makeLibraryPath makeSearchPathOutput makeBinPath escapeShellArg;
  inherit (pkgs.stdenv) mkDerivation;

  inherit (pkgs.stdenv.hostPlatform) system;
  selectSystem = attrs:
    attrs.${system} or (throw "Unsupported system: ${system}");

  suffix = selectSystem {
    x86_64-linux = "linux";
    aarch64-linux = "linux-arm64";
    # lets hope the linux stuff works on mac too
    x86_64-darwin = "linux";
    aarch64-darwin = "linux-arm64";
    # x86_64-darwin = "mac";
    # aarch64-darwin = "mac-arm64";
  };
  sha256 = {
    "1000" = selectSystem {
      x86_64-linux = "";
      aarch64-linux = "";
      x86_64-darwin = "";
      aarch64-darwin = "";
    };
    "1005" = selectSystem {
      x86_64-linux = "sha256-xLstVHIBzQLWVP5aGPTOFHrbfOU3in4jIxxnLfG/WtI=";
      aarch64-linux = "";
      x86_64-darwin = "";
      aarch64-darwin = "";
    };
  }.${rev};

  upstream_chromium = fetchurl {
    url =
      "https://playwright.azureedge.net/builds/chromium/${rev}/chromium-${suffix}.zip";
    inherit sha256;
  };

  opusWithCustomModes = pkgs.libopus.override {
    withCustomModes = true;
  };

  deps =
    let
      pulseSupport = true;
      libvaSupport = true;
    in with pkgs; with xorg; [
    glib fontconfig freetype pango cairo libX11 libXi atk nss nspr
    libXcursor libXext libXfixes libXrender libXScrnSaver libXcomposite libxcb
    alsa-lib libXdamage libXtst libXrandr libxshmfence expat cups
    dbus gdk-pixbuf gcc-unwrapped.lib
    systemd
    libexif pciutils
    liberation_ttf curl util-linux xdg-utils wget
    flac harfbuzz icu libpng opusWithCustomModes snappy speechd
    bzip2 libcap at-spi2-atk at-spi2-core
    libkrb5 libdrm libglvnd mesa coreutils
    libxkbcommon pipewire wayland
  ] ++ optional pulseSupport libpulseaudio
    ++ optional libvaSupport libva
    ++ [ gtk3 ];

  commandLineArgs = [ "--disable-gpu" ];

  gtk_modules = [ pkgs.libcanberra-gtk3 ];

in mkDerivation {
  name = "chromium-playwright-${rev}";
  version = rev;
  src = upstream_chromium;

  nativeBuildInputs = with pkgs; [ unzip patchelf makeWrapper ];

  buildInputs = with pkgs; [
    # needed for GSETTINGS_SCHEMAS_PATH
    gsettings-desktop-schemas glib gtk3

    # needed for XDG_ICON_DIRS
    gnome.adwaita-icon-theme
  ];

  rpath = makeLibraryPath deps + ":" + makeSearchPathOutput "lib" "lib64" deps;
  binpath = makeBinPath deps;

  dontBuild = true;

  installPhase = ''
    runHook preInstall

    mkdir $out
    cp -r ./* $out/

    mv $out/chrome $out/chrome.bin
    makeWrapper "$out/chrome.bin" "$out/bin/chrome" \
      --prefix LD_LIBRARY_PATH : "$rpath" \
      --prefix PATH            : "$binpath" \
      --prefix XDG_DATA_DIRS   : "$XDG_ICON_DIRS:$GSETTINGS_SCHEMAS_PATH:${pkgs.addOpenGLRunpath.driverLink}/share" \
      --suffix-each GTK_PATH ':' "$gtk_modules" \
      --set CHROME_WRAPPER  "google-chrome-$dist" \
      --add-flags ${escapeShellArg commandLineArgs} \
      --add-flags "\''${NIXOS_OZONE_WL:+\''${WAYLAND_DISPLAY:+--enable-features=UseOzonePlatform --ozone-platform=wayland}}"

    for elf in $out/{chrome.bin,chrome_sandbox,chrome_crashpad_handler}; do
      patchelf --set-rpath $rpath $elf
      patchelf --set-interpreter "$(cat $NIX_CC/nix-support/dynamic-linker)" $elf
    done

    runHook postInstall
  '';

  gtk_modules = map (x: x + x.gtkModule) gtk_modules;
}
