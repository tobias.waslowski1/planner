{ pkgs ? import <nixpkgs> { }, rev }:

let
  inherit (pkgs) fetchurl;
  inherit (pkgs.lib) optionalString;
  inherit (pkgs.stdenv) mkDerivation;

  inherit (pkgs.stdenv.hostPlatform) system;
  selectSystem = attrs:
    attrs.${system} or (throw "Unsupported system: ${system}");

  suffix = selectSystem {
    x86_64-linux = "ubuntu-20.04";
    # aarch64-linux = "ubuntu-20.04-arm64";
    # lets hope the linux stuff works on mac too
    x86_64-darwin = "ubuntu-20.04";
    aarch64-darwin = "ubuntu-20.04-arm64";
    # x86_64-darwin = "mac";
    # aarch64-darwin = "mac-arm64";
  };
  sha256 = {
    "1322" = selectSystem {
      x86_64-linux = "";
      aarch64-linux = "";
      x86_64-darwin = "";
      aarch64-darwin = "";
    };
    "1323" = selectSystem {
      x86_64-linux = "sha256-YPD+gKM/OKDb86AkXcSS0ryvF89L3VEITtrORgvG5es=";
      aarch64-linux = "";
      x86_64-darwin = "";
      aarch64-darwin = "";
    };
  }.${rev};

  upstream_firefox = fetchurl {
    url =
      "https://playwright.azureedge.net/builds/firefox/${rev}/firefox-${suffix}.zip";
    inherit sha256;
  };
  gtk_modules = [ pkgs.libcanberra-gtk3 ];

in mkDerivation {
  name = "firefox-playwright-${rev}";
  version = rev;
  src = upstream_firefox;

  nativeBuildInputs = with pkgs; [ unzip patchelf makeWrapper ];

  buildInputs = with pkgs; [
    gdk-pixbuf
  ];

  dontBuild = true;

  installPhase = ''
    runHook preInstall

    mkdir $out
    cp -r ./* $out/

    mv $out/firefox $out/firefox.bin

    makeWrapper "$out/firefox.bin" "$out/bin/firefox" \
        --prefix LD_LIBRARY_PATH ':' "$libs" \
        --suffix-each GTK_PATH ':' "$gtk_modules" \
        --prefix PATH ':' "${pkgs.xdg-utils}/bin" \
        --suffix PATH ':' "$out/bin" \
        --set MOZ_APP_LAUNCHER "firefox" \
        --set MOZ_SYSTEM_DIR "$out/lib/mozilla" \
        --set MOZ_LEGACY_PROFILES 1 \
        --set MOZ_ALLOW_DOWNGRADE 1 \
        --prefix XDG_DATA_DIRS : "$GSETTINGS_SCHEMAS_PATH" \
        --suffix XDG_DATA_DIRS : '${pkgs.gnome.adwaita-icon-theme}/share' \
        "''${oldWrapperArgs[@]}"
  '';
  gtk_modules = map (x: x + x.gtkModule) gtk_modules;
}
